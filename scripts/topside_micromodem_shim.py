#! /usr/bin/env python2
import rospy
import select
import socket

import ds_acomms_msgs.msg
import ds_core_msgs.msg


class MicromodemTopsideShim(object):
    '''
    Shim to allow the ROS micromodem driver to drop right in to the existing
    topside sentrysitter pipeline.

    This requires two interfaces:
    1) For sending JT and passthrough commands from the sentrysitter GUI, the
       sentrysiter server acts like a modem driver and sends $CCCYC/$CCTXD
       commands to dpa:13023 and expects them to be passed straight through to
       moxa:13023, bypassing the driver.
    2) Various pieces of the infrastructure require access to the raw data
       coming from the modem. The driver only needs to forward it to
       - 192.168.100.255:13013 (sentrysitter server)
       - 127.0.0.1:60301 (nav? ...regardless, it's "unreachable" in the
         current setup)
       This can be done by subscribing to the driver's DsConnection's
       /modem/raw output, and forwarding the content to the server.
    '''

    def __init__(self):
        self.rx_port = rospy.get_param("~rx_passthrough_port")

        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.socket.bind(('', self.rx_port))

        self.tx_raw_addr = rospy.get_param("~tx_raw_addr")
        self.tx_raw_port = rospy.get_param("~tx_raw_port")
        self.tx_moxa_addr = rospy.get_param("~tx_moxa_addr")
        self.tx_moxa_port = rospy.get_param("~tx_moxa_port")

        self.raw_sub = rospy.Subscriber("raw",
                                        ds_core_msgs.msg.RawData,
                                        self.raw_callback)

        # For the modems...
        self.local_address = rospy.get_param("~local_address")
        self.remote_address = rospy.get_param("~remote_address")

    def raw_callback(self, msg):
        '''
        Listen for raw data coming from the modem and rebroadcast it via
        UDP to the various topside GUIs.
        '''
        if msg.data_direction == msg.DATA_OUT:
            return
        data = ''.join([chr(ord(ii)) for ii in msg.data])
        # This is a hack trying to get around the weird \n .... \r
        # formatting I'm seeing in ds_raw_console
        data = data.strip() + "\r\n"

        rospy.loginfo("(driver -> sentrysitter) {}".format(data.strip()))
        self.socket.sendto(data, (self.tx_raw_addr, self.tx_raw_port))

    def run(self):
        '''
        Monitor the socket for incoming messages that need to be
        sent directly to the modem, bypassing the driver.
        '''
        timeout = 0.05
        read_list, write_list, x_list = [self.socket], [], []

        while not rospy.is_shutdown():
            read_ready, _, _ = select.select(read_list, write_list, x_list, timeout)
            if len(read_ready) > 0:
                data = self.socket.recv(4096)
                rospy.loginfo("(sentrysitter -> modem) {}".format(data.strip()))
                self.socket.sendto(data.strip()+"\r\n", (self.tx_moxa_addr, self.tx_moxa_port))


if __name__ == "__main__":
    rospy.init_node('topside_micromodem_shim')
    shim = MicromodemTopsideShim()
    shim.run()
