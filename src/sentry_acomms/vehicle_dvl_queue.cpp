/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

//
// Created by ivandor on 09/02/23.
//

#include "sentry_acomms/vehicle_dvl_queue.h"
#include "ds_sensor_msgs/DvlStatus.h"

namespace sentry_acomms
{
VehicleDvlStatusQueue::VehicleDvlStatusQueue() : SimpleSingleQueue<SentryDvlStatus>()
{
}

VehicleDvlStatusQueue::~VehicleDvlStatusQueue() = default;

void VehicleDvlStatusQueue::setup(ros::NodeHandle& nh)
{

  auto topic_name = std::string{};
  topic_name = ros::names::resolve(ros::this_node::getName(),
                                   std::string{"dvlstatus_topic"});
  auto dvl_topic = std::string{};
  if (!ros::param::get(topic_name, dvl_topic))
  {
    ROS_FATAL_STREAM("Parameter: '" << topic_name << "' is not defined");
    ROS_BREAK();
  }
  dvl_sub_ = nh.subscribe(dvl_topic, 1,
                                     &VehicleDvlStatusQueue::dvlstatusCallback, this);
}

void VehicleDvlStatusQueue::dvlstatusCallback(const ds_sensor_msgs::DvlStatus& msg)
{
  // DVL Status
  item_.altitude = msg.altitude;
  item_.course_gnd = msg.course_gnd;
  item_.speed_gnd = msg.speed_gnd;
  item_.num_good_beams = msg.num_good_beams;
  
  // New fields. 2024-06-24 -tj
  item_.time_last_raw_dvl = msg.time_last_raw_dvl;
  item_.time_good_parse = msg.time_good_parse;
  item_.window_size = msg.window_size;
  item_.parses_tried_in_window = msg.parses_tried_in_window;
  item_.parses_ok_in_window = msg.parses_ok_in_window;
}

} // namespace sentry_acomms
