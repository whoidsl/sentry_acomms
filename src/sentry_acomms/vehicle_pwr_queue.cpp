/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_acomms/vehicle_pwr_queue.h"
#include "sentry_acomms/serialization.h"

#include "ds_hotel_msgs/PWR.h"

namespace sentry_acomms
{
VehiclePwrQueue::VehiclePwrQueue() : SimpleSingleQueue<SentryPowerSwitches>()
{
}

VehiclePwrQueue::~VehiclePwrQueue() = default;

void VehiclePwrQueue::setup(ros::NodeHandle& nh)
{
  auto node_name = std::string{};
  auto str = std::string{};

  str = ros::names::resolve(ros::this_node::getName(), std::string{ "pwr_node" });

  if (!ros::param::get(str, node_name))
  {
    ROS_FATAL_STREAM("Parameter: '" << str << "' is not defined");
    ROS_BREAK();
  }

  str = ros::names::resolve(node_name, std::string{ "state" });
  pwr_sub_ = nh.subscribe(str, 1, &VehiclePwrQueue::pwrCallback, this);
}

void VehiclePwrQueue::pwrCallback(const ds_hotel_msgs::PWR& msg)
{
  for (auto i = 0; i < item_.power.size(); ++i)
  {
    item_.power[i] = static_cast<uint8_t>(msg.pwr_cmd.at(i));
  }
}
}
