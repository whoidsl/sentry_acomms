/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_acomms/serialization.h"

// Sentry Queue Commands
#include "sentry_acomms/SentryQueueManagerCommandEnum.h"
#include "sentry_acomms/MissionControllerCommand.h"
#include "sentry_acomms/BlueviewCommand.h"
#include "sentry_acomms/PhinsCommand.h"
#include "sentry_acomms/NavaggCommand.h"
#include "sentry_acomms/BottomFollowerParameterCommand.h"
#include "sentry_acomms/DepthFollowerParameterCommand.h"
#include "sentry_acomms/SetActiveDepthGoalCommand.h"
#include "sentry_acomms/ResonCommand.h"
#include "sentry_acomms/AcousticJoystickCommand.h"
#include "sentry_acomms/ThrustersEnabledCommand.h"
#include "sentry_acomms/UdpQueueCommand.h"
#include "sentry_acomms/AcousticScheduleCommand.h"
#include "ds_control_msgs/ExternalBottomFollowTimedOverride.h"
#include "sentry_acomms/SentryMxEvent.h"
#include "sentry_acomms/SentryMxSharedParam.h"
#include "sentry_acomms/SentryNavShift.h"
#include "sentry_acomms/KongsbergParamCommand.h"
#include "sentry_acomms/KongsbergPingCommand.h"
#include "sentry_acomms/KongsbergKctrlCommand.h"
#include "sentry_acomms/SypridCommand.h"
#include "sentry_acomms/SuprCommand.h"

// Sentry Queues
#include "sentry_acomms/SentryVehicleState.h"
#include "sentry_acomms/SentryHtp.h"
#include "sentry_acomms/SentryProsilicaCameraStatus.h"
#include "sentry_acomms/SentryPowerSwitches.h"
#include "sentry_acomms/SentryCurrentShift.h"
#include "sentry_acomms/SentryBottomFollowerParameters.h"
#include "sentry_acomms/SentryDepthFollowerParameters.h"
#include "sentry_acomms/SentryGoalLegState.h"
#include "sentry_acomms/SentryBlueviewStatus.h"
#include "sentry_acomms/SentryControlAndActuatorState.h"
#include "sentry_acomms/SentryResonStatus.h"
#include "sentry_acomms/SentryScalarScience.h"
#include "sentry_acomms/SentryKongsbergStatus.h"
#include "sentry_acomms/SentryKongsbergStatus2.h"
#include "sentry_acomms/SentrySypridStatus.h"
#include "sentry_acomms/SentryIns.h"
#include "sentry_acomms/SentryInsNav.h"
#include "sentry_acomms/SentryDvlStatus.h"

#include <boost/date_time.hpp>
#include <regex>
#include <sentry_acomms/SentryMxStatus.h>

namespace sentry_acomms
{
// uint8_t queue_message_type(const std::vector<uint8_t>& buf)
// {
//   ROS_ERROR_STREAM("Queue commands from byte streams are not implemented");
//   return ds_acomms_queue_manager::QueueManagerMessageEnum::INVALID_MESSAGE_TYPE;
// }

uint8_t queue_message_type(const std::string& buf)
{
  // Skip white space
  const auto start = buf.find_first_not_of(' ');

#define ADD_TEST_CASE(STRING, VALUE)                                                                                   \
  if (buf.compare(start, SentryQueueManagerCommandEnum::STRING.size(), SentryQueueManagerCommandEnum::STRING) == 0)    \
  {                                                                                                                    \
    return SentryQueueManagerCommandEnum::VALUE;                                                                       \
  }

  // Mission controller (MCA) passthrough
  ADD_TEST_CASE(MISSION_CONTROLLER_CMD_STR, MISSION_CONTROLLER_COMMAND)

  // Reson daemon (RESONMSG) passthrough
  ADD_TEST_CASE(RESON_CMD_STR, RESON_COMMAND)

  // Blueview daemon (BLV) passthrough
  ADD_TEST_CASE(BLUEVIEW_CMD_STR, BLUEVIEW_COMMAND)

  // Phins repeater command
  ADD_TEST_CASE(PHINS_CMD_STR, PHINS_COMMAND)

  // Nav aggregator switchyard command
  ADD_TEST_CASE(NAVAGG_CMD_STR, NAVAGG_COMMAND)

  // Bottom Follower Parameter (WBFP)
  ADD_TEST_CASE(BOTTOM_FOLLOWER_PARAM_CMD_STR, BOTTOM_FOLLOWER_PARAM_COMMAND)

  // Depth Follower Parameter (WDFP)
  ADD_TEST_CASE(DEPTH_FOLLOWER_PARAM_CMD_STR, DEPTH_FOLLOWER_PARAM_COMMAND)

  // Reson command (RESONMSG)
  ADD_TEST_CASE(RESON_CMD_STR, RESON_COMMAND)

  // Bottom Follower Override (BF_OVERRIDE)
  ADD_TEST_CASE(BOTTOM_FOLLOWER_OVERRIDE_CMD_STR, BOTTOM_FOLLOWER_OVERRIDE_COMMAND)

  // Acoustic Joystick Command (AJOY)
  ADD_TEST_CASE(ACOUSTIC_JOYSTICK_CMD_STR, ACOUSTIC_JOYSTICK_COMMAND)

  // Thrusters Enable Command (THRUSTERS_ENABLED)
  ADD_TEST_CASE(THRUSTERS_ENABLED_CMD_STR, THRUSTERS_ENABLED_COMMAND)

  // Acoustic Schedule Command
  ADD_TEST_CASE(ACOUSTIC_SCHEDULE_CMD_STR, ACOUSTIC_SCHEDULE_COMMAND)

  // Nav Shift in (SHFTABS)
  ADD_TEST_CASE(NAV_SHIFT_CMD_STR , NAV_SHIFT_COMMAND)

  // MX Event In (MXEV)
  ADD_TEST_CASE(MX_EVENT_CMD_STR , MX_EVENT_COMMAND)

  // MX Shared Parameter In (MXSP)
  ADD_TEST_CASE(MX_SHARED_PARAM_CMD_STR , MX_SHARED_PARAM_COMMAND)

  // Kongsberg EM2040 Param Cmd
  ADD_TEST_CASE(KONGSBERG_EM2040_PARAM_CMD_STR, KONGSBERG_EM2040_PARAM_COMMAND)

  // Kongsberg EM2040 Ping Cmd
  ADD_TEST_CASE(KONGSBERG_EM2040_PING_CMD_STR, KONGSBERG_EM2040_PING_COMMAND)

  // Kongsberg EM2040 watercolumn cmd
  ADD_TEST_CASE(KONGSBERG_EM2040_WATERCOLUMN_CMD_STR, KONGSBERG_EM2040_WATERCOLUMN_COMMAND)

  // Kongsberg EM2040 KLOGGER
  ADD_TEST_CASE(KONGSBERG_EM2040_KLOGGER_CMD_STR, KONGSBERG_EM2040_KLOGGER_COMMAND)

  // Syprid Cmd
  ADD_TEST_CASE(SYPRID_CMD_STR, SYPRID_COMMAND)

  // Supr Cmd
  ADD_TEST_CASE(SUPR_CMD_STR, SUPR_COMMAND)

  // Set Active Depth Goal
  ADD_TEST_CASE(SET_ACTIVE_DEPTH_GOAL_CMD_STR, SET_ACTIVE_DEPTH_GOAL_COMMAND)

  // UDP Passthroughs
  ADD_TEST_CASE(UDP_PASSTHROUGH_00_CMD_STR, UDP_PASSTHROUGH_00_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_01_CMD_STR, UDP_PASSTHROUGH_01_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_02_CMD_STR, UDP_PASSTHROUGH_02_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_03_CMD_STR, UDP_PASSTHROUGH_03_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_04_CMD_STR, UDP_PASSTHROUGH_04_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_05_CMD_STR, UDP_PASSTHROUGH_05_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_06_CMD_STR, UDP_PASSTHROUGH_06_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_07_CMD_STR, UDP_PASSTHROUGH_07_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_08_CMD_STR, UDP_PASSTHROUGH_08_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_09_CMD_STR, UDP_PASSTHROUGH_09_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_10_CMD_STR, UDP_PASSTHROUGH_10_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_11_CMD_STR, UDP_PASSTHROUGH_11_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_12_CMD_STR, UDP_PASSTHROUGH_12_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_13_CMD_STR, UDP_PASSTHROUGH_13_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_14_CMD_STR, UDP_PASSTHROUGH_14_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_15_CMD_STR, UDP_PASSTHROUGH_15_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_16_CMD_STR, UDP_PASSTHROUGH_16_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_17_CMD_STR, UDP_PASSTHROUGH_17_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_18_CMD_STR, UDP_PASSTHROUGH_18_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_19_CMD_STR, UDP_PASSTHROUGH_19_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_20_CMD_STR, UDP_PASSTHROUGH_20_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_21_CMD_STR, UDP_PASSTHROUGH_21_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_22_CMD_STR, UDP_PASSTHROUGH_22_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_23_CMD_STR, UDP_PASSTHROUGH_23_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_24_CMD_STR, UDP_PASSTHROUGH_24_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_25_CMD_STR, UDP_PASSTHROUGH_25_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_26_CMD_STR, UDP_PASSTHROUGH_26_COMMAND)
  ADD_TEST_CASE(UDP_PASSTHROUGH_27_CMD_STR, UDP_PASSTHROUGH_27_COMMAND)

#undef ADD_TEST_CASE

  return ds_acomms_queue_manager::QueueManagerMessageEnum::INVALID_MESSAGE_TYPE;
}
}  // namespace sentry_acomms

namespace ds_acomms_serialization
{
template <>
void serialize(const sentry_acomms::SentryVehicleState& msg, std::string& buf)
{
  auto ss = std::ostringstream{};
  // First up, position w/ no decimal places
  ss << std::fixed << std::setprecision(0);
  ss << msg.pos_ned.x << "," << msg.pos_ned.y << "," << msg.pos_ned.z << ",";
  // Next, altitude and heading w/ 1 decimal point.
  ss << std::setprecision(1);
  ss << msg.altitude << "," << msg.heading << ",";
  // Velocities get 2 decimals
  ss << std::setprecision(2);
  ss << msg.horz_velocity << "," << msg.vert_velocity << ",";
  // A single digit for the abort flag
  ss << "A" << static_cast<int>(msg.abort_status) << ",";
  // We don't have the 'cfg' parameter any more, this is only here for legacy purposes
  ss << "0,";
  // Battery percent
  ss << std::setprecision(1) << msg.battery_pct << ",";
  // Goals, no decimal places
  ss << std::setprecision(0);
  ss << msg.goal_ned.x << "," << msg.goal_ned.y << "," << msg.goal_ned.z << ",";
  // More unused fields now: battery_time, tx_count, rx_count, dvl_status
  ss << "0,0,0,0,";
  // Still more unused fields:  flourometer_volts, optode_volts, eh_volts
  ss << "0,0,0,";
  // Finally, trackline number
  ss << msg.trackline;

  buf.append(ss.str());
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryVehicleState& msg)
{
  auto fields = std::vector<std::string>{};

  const auto num_parsed =
      sscanf(buf.data(), "%f,%f,%f,%f,%f,%f,%f,A%hhu,%*f,%f,%f,%f,%f,%*f,%*f,%*f,%*f,%*f,%*f,%*f,%hu", &msg.pos_ned.x,
             &msg.pos_ned.y, &msg.pos_ned.z, &msg.altitude, &msg.heading, &msg.horz_velocity, &msg.vert_velocity,
             &msg.abort_status, &msg.battery_pct, &msg.goal_ned.x, &msg.goal_ned.y, &msg.goal_ned.z, &msg.trackline);

  return num_parsed == 13;
}

template <>
void serialize(const sentry_acomms::SentryHtp& msg, std::string& buf)
{
  auto ss = std::ostringstream{};
  ss << std::fixed;
  ss << "MAIN:";
  ss << std::setprecision(0) << "H" << msg.main_humidity << ",";
  ss << std::setprecision(1) << "T" << msg.main_temperature << ",P" << msg.main_pressure;

  ss << " BAT:";
  ss << std::setprecision(0) << "H" << msg.battery_humidity << ",";
  ss << std::setprecision(1) << "T" << msg.battery_temperature << ",P" << msg.battery_pressure;

  buf = ss.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryHtp& msg)
{
  auto fields = std::vector<std::string>{};

  const auto num_parsed =
      sscanf(buf.data(), "MAIN:H%f,T%f,P%f BAT:H%f,T%f,P%f", &msg.main_humidity, &msg.main_temperature,
             &msg.main_pressure, &msg.battery_humidity, &msg.battery_temperature, &msg.battery_pressure);

  return num_parsed == 6;
}

template <>
void serialize(const sentry_acomms::MissionControllerCommand& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  const auto start = msg.command.find("MCA");
  if (start != msg.command.npos)
  {
    os << msg.command.substr(start);
  }
  else
  {
    os << "MCA " << msg.command;
  }

  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::MissionControllerCommand& msg)
{
  // add that extra space at the end of MCA -- needs to be there as the actual command follows.
  const auto start = buf.find("MCA ");
  if (start == buf.npos)
  {
    return false;
  }

  msg.command = buf.substr(start);
  return true;
}

template <>
void serialize(const sentry_acomms::BlueviewCommand& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  const auto start = msg.command.find("BLV");
  if (start == !msg.command.npos)
  {
    os << msg.command.substr(start);
  }
  else
  {
    os << "BLV " << msg.command;
  }

  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::BlueviewCommand& msg)
{
  // add that extra space at the end of BLV -- needs to be there as the actual command follows.
  const auto start = buf.find("BLV ");
  if (start == buf.npos)
  {
    return false;
  }

  msg.command = buf.substr(start);
  return true;
}

template <>
void serialize(const sentry_acomms::PhinsCommand& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  const auto start = msg.command.find("PHI");
  if (start == !msg.command.npos)
  {
    os << msg.command.substr(start);
  }
  else
  {
    os << "PHI " << msg.command;
  }

  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::PhinsCommand& msg)
{
  // add that extra space at the end of PHI -- needs to be there as the actual command follows.
  const auto start = buf.find("PHI ");
  if (start == buf.npos)
  {
    return false;
  }

  msg.command = buf.substr(start);
  return true;
}

template <>
void serialize(const sentry_acomms::NavaggCommand& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  os << "NAG " << static_cast<int>(msg.enum_xy) << " " << static_cast<int>(msg.enum_heading) << " "
     << static_cast<int>(msg.enum_pitchroll) << " " << static_cast<int>(msg.enum_depth) << " " << 
     static_cast<int>(msg.enum_sync);
  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::NavaggCommand& msg)
{
  // add that extra space at the end of NAG -- needs to be there as the actual command follows.
  const auto start = buf.find("NAG ");
  if (start == buf.npos)
  {
    return false;
  }

  auto is = std::istringstream{ buf.substr(start + 4) };

  int enum_xy;
  int enum_heading;
  int enum_pitchroll;
  int enum_depth;
  int enum_sync;

  is >> enum_xy >> enum_heading >> enum_pitchroll >> enum_depth >> enum_sync;

  msg.enum_xy = static_cast<uint8_t>(enum_xy);
  msg.enum_heading = static_cast<uint8_t>(enum_heading);
  msg.enum_pitchroll = static_cast<uint8_t>(enum_pitchroll);
  msg.enum_depth = static_cast<uint8_t>(enum_depth);
  msg.enum_sync = static_cast<uint8_t>(enum_sync);

  if (!is)
  {
    return false;
  }

  return true;
}

template <>
void serialize(const sentry_acomms::ResonCommand& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  const auto start = msg.command.find("RESONMSG");
  if (start == !msg.command.npos)
  {
    os << msg.command.substr(start);
  }
  else
  {
    os << "RESONMSG " << msg.command;
  }

  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::ResonCommand& msg)
{
  // add that extra space at the end of BLV -- needs to be there as the actual command follows.
  const auto start = buf.find("RESONMSG ");
  if (start == buf.npos)
  {
    return false;
  }

  msg.command = buf.substr(start);
  return true;
}

template <>
void serialize(const sentry_acomms::AcousticJoystickCommand& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  os << "AJOY " << msg.timeout.toBoost() << " " << std::fixed << std::setprecision(1)
     << static_cast<int>(msg.allocation_enum) << " " << msg.surge_u << " " << msg.heave_w << " " << msg.torque_w;
  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::AcousticJoystickCommand& msg)
{
  // add that extra space at the end of AJOY -- needs to be there as the actual command follows.
  const auto start = buf.find("AJOY ");
  if (start == buf.npos)
  {
    return false;
  }

  auto is = std::istringstream{ buf.substr(start + 4) };

  boost::posix_time::time_duration duration;
  uint16_t allocation;
  int hold_heading;

  is >> duration >> allocation >> msg.surge_u >> msg.heave_w >> msg.torque_w;

  msg.timeout.fromSec(duration.total_seconds());
  msg.allocation_enum = static_cast<uint8_t>(allocation);

  if (!is)
  {
    return false;
  }

  return true;
}

template <>
void serialize(const sentry_acomms::SentryProsilicaCameraStatus& msg, std::string& buf)
{
  auto os = std::ostringstream{};

  os << std::fixed;
  os << "P" << static_cast<int>(msg.power) << ",A" << static_cast<int>(msg.active) << ",NF" << static_cast<int>(msg.nf)
     << ",FC" << msg.frames << ",E" << msg.exposure << ",G" << static_cast<int>(msg.gain) << ",FR"
     << std::setprecision(2) << msg.frame_rate;
  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryProsilicaCameraStatus& msg)
{
  const auto start = buf.find('P');
  if (start == buf.npos)
  {
    return false;
  }

  const auto num_parsed = std::sscanf(buf.substr(start).data(), "P%hhu,A%hhu,NF%hhu,FC%u,E%u,G%hhu,FR%f", &msg.power,
                                      &msg.active, &msg.nf, &msg.frames, &msg.exposure, &msg.gain, &msg.frame_rate);

  return num_parsed == 7;
}

template <>
void serialize(const sentry_acomms::SentryPowerSwitches& msg, std::string& buf)
{
  auto os = std::ostringstream{};

  os << std::fixed << std::hex << std::uppercase << std::setfill('0');
  os << std::setw(2) << static_cast<int>(msg.power[0]) << "," << std::setw(2) << static_cast<int>(msg.power[1]) << ","
     << std::setw(2) << static_cast<int>(msg.power[2]) << "," << std::setw(2) << static_cast<int>(msg.power[3]) << ","
     << std::setw(2) << static_cast<int>(msg.power[4]) << "," << std::setw(2) << static_cast<int>(msg.power[5]) << ","
     << std::setw(2) << static_cast<int>(msg.power[6]) << "," << std::setw(2) << static_cast<int>(msg.power[7]);
  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryPowerSwitches& msg)
{
  const auto num_parsed =
      std::sscanf(buf.data(), "%hhx,%hhx,%hhx,%hhx,%hhx,%hhx,%hhx,%hhx", &msg.power[0], &msg.power[1], &msg.power[2],
                  &msg.power[3], &msg.power[4], &msg.power[5], &msg.power[6], &msg.power[7]);

  return num_parsed == 8;
}

template <>
void serialize(const sentry_acomms::SentryCurrentShift& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  auto facet = new boost::posix_time::time_facet("%H:%M");
  os.imbue(std::locale(os.getloc(), facet));

  os << msg.stamp.toBoost() << " " << std::fixed << std::setprecision(0) << msg.x << " " << msg.y;

  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryCurrentShift& msg)
{
  auto is = std::istringstream{ buf };

  // Attempt to parse a time -- should be present on all responses.
  auto facet = new boost::posix_time::time_input_facet("%H:%M");
  is.imbue(std::locale(is.getloc(), facet));

  auto time = boost::posix_time::ptime{};
  is >> time;

  if (!is)
  {
    return false;
  }

  // Date parsing.. is complicated..  We only send HH:MM, so need to be mindful of
  // sending things with timestamps in the previous day.

  // This is the current time according to ROS
  auto boost_stamp = ros::Time::now().toBoost();
  // And the current date
  auto boost_date = boost_stamp.date();

  // If the parsed hours are GREATER than the current hours, then we have a situation where
  // we are sending a timestamp from the previous day.  Subtract one day from the current date
  if (time.time_of_day().hours() > boost_stamp.time_of_day().hours())
  {
    boost_date -= boost::gregorian::days(1);
  }

  // Create our timestamp.
  boost_stamp = boost::posix_time::ptime{ boost_date, time.time_of_day() };
  msg.stamp = msg.stamp.fromBoost(boost_stamp);

  is >> msg.x;
  is >> msg.y;

  if (!is)
  {
    return false;
  }

  return true;
}

template <>
void serialize(const ds_control_msgs::ExternalBottomFollowTimedOverride& msg, std::string& buf)
{
  auto os = std::ostringstream{};

  os << std::fixed << std::setprecision(2);
  os << "BF_OVERRIDE " << static_cast<int>(msg.timeout.toSec()) << " "
     << static_cast<int>(msg.override_depth_direction);
  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, ds_control_msgs::ExternalBottomFollowTimedOverride& msg)
{
  auto start_index = buf.find("BF_OVERRIDE ");
  if (start_index == buf.npos)
  {
    return false;
  }

  unsigned int timeout;

  const auto num_scanned =
      sscanf(buf.substr(start_index).data(), "BF_OVERRIDE %u %hhd", &timeout, &msg.override_depth_direction);

  if (num_scanned != 2)
  {
    return false;
  }

  switch (msg.override_depth_direction)
  {
    case ds_control_msgs::ExternalBottomFollowTimedOverride::DRIVE_UP:
    case ds_control_msgs::ExternalBottomFollowTimedOverride::DRIVE_DOWN:
      break;
    default:
      ROS_ERROR_STREAM(
          "Invalid bottom follower override direction: " << static_cast<int>(msg.override_depth_direction));
      return false;
  }

  msg.timeout.fromSec(timeout);
  msg.header.stamp = ros::Time::now();
  return true;
}

template <>
void serialize(const sentry_acomms::BottomFollowerParameterCommand& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  os << std::fixed << std::setprecision(2);
  os << "WBFP " << static_cast<int>(msg.index) << " " << msg.value;
  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::BottomFollowerParameterCommand& msg)
{
  auto start_index = buf.find("WBFP ");
  if (start_index == buf.npos)
  {
    msg.index = msg.INDEX_INVALID;
    return false;
  }

  const auto num_scanned = sscanf(buf.substr(start_index).data(), "WBFP %hhu %f", &msg.index, &msg.value);

  if (msg.index < sentry_acomms::BottomFollowerParameterCommand::INDEX_MIN ||
      msg.index > sentry_acomms::BottomFollowerParameterCommand::INDEX_MAX)
  {
    msg.index = msg.INDEX_INVALID;
    return false;
  }

  return num_scanned == 2;
}

template <>
void serialize(const sentry_acomms::DepthFollowerParameterCommand& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  os << std::fixed << std::setprecision(2);
  os << "WDFP " << static_cast<int>(msg.index) << " " << msg.value;
  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::DepthFollowerParameterCommand& msg)
{
  auto start_index = buf.find("WDFP ");
  if (start_index == buf.npos)
  {
    msg.index = msg.INDEX_INVALID;
    return false;
  }

  const auto num_scanned = sscanf(buf.substr(start_index).data(), "WDFP %hhu %f", &msg.index, &msg.value);

  if (msg.index < sentry_acomms::BottomFollowerParameterCommand::INDEX_MIN ||
      msg.index > sentry_acomms::BottomFollowerParameterCommand::INDEX_MAX)
  {
    msg.index = msg.INDEX_INVALID;
    return false;
  }

  return num_scanned == 2;
}

template <>
void serialize(const sentry_acomms::SentryBottomFollowerParameters& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  os << std::fixed << std::setprecision(2);
  os << msg.altitude << " " << msg.envelope << " " << msg.speed << " " << msg.speed_gain << " " << msg.depth_rate << " "
     << msg.depth_acceleration << " " << msg.min_speed << " " << msg.alarm_timeout << " " << msg.depth_floor << " "
     << static_cast<int>(msg.user_override_active) << " " << static_cast<int>(msg.user_override_direction);

  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryBottomFollowerParameters& msg)
{
  const auto num_scanned =
      sscanf(buf.data(), "%f %f %f %f %f %f %f %f %f %hhu %hhd", &msg.altitude, &msg.envelope, &msg.speed,
             &msg.speed_gain, &msg.depth_rate, &msg.depth_acceleration, &msg.min_speed, &msg.alarm_timeout,
             &msg.depth_floor, &msg.user_override_active, &msg.user_override_direction);

  return num_scanned == 11;
}

template <>
void serialize(const sentry_acomms::SentryDepthFollowerParameters& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  os << std::fixed << std::setprecision(2);
  os << msg.depth << " " << msg.envelope << " " << msg.speed << " " << msg.speed_gain << " " << msg.depth_rate << " "
     << msg.depth_acceleration << " " << msg.minimum_speed << " " << msg.minimum_altitude << " " << msg.depth_floor << " "
     << msg.next_depth_goal << " " << msg.next_speed_goal << " " << msg.current_altitude << " " << static_cast<int>(msg.enabled);

  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryDepthFollowerParameters& msg)
{
  auto enabled = 0;
  const auto num_scanned =
      sscanf(buf.data(), "%f %f %f %f %f %f %f %f %f %f %f %f %d", &msg.depth, &msg.envelope, &msg.speed,
             &msg.speed_gain, &msg.depth_rate, &msg.depth_acceleration, &msg.minimum_speed, &msg.minimum_altitude,
             &msg.depth_floor, &msg.next_depth_goal, &msg.next_speed_goal, &msg.current_altitude, &enabled);
  msg.enabled = static_cast<bool>(enabled);

  return num_scanned == 13;
}

template <>
void serialize(const sentry_acomms::SentryGoalLegState& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  os << std::fixed << std::setprecision(2);

  os << msg.line_start.x << " " << msg.line_start.y << " " << msg.line_end.x << " " << msg.line_end.y << " "
     << msg.angle_line_segment << " " << msg.off_line_vect << " " << msg.sign_of_vect << " " << msg.kappa << " "
     << msg.old_goal << " " << msg.new_goal << " " << msg.leg_number;

  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryGoalLegState& msg)
{
  const auto num_scanned = sscanf(buf.data(), "%f %f %f %f %f %f %f %f %f %f %lu", &msg.line_start.x, &msg.line_start.y,
                                  &msg.line_end.x, &msg.line_end.y, &msg.angle_line_segment, &msg.off_line_vect,
                                  &msg.sign_of_vect, &msg.kappa, &msg.old_goal, &msg.new_goal, &msg.leg_number);

  return num_scanned == 11;
}

template <>
void serialize(const sentry_acomms::SentryBlueviewStatus& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  os << std::fixed << std::setprecision(0);

  os << msg.time << " SONAR_";

  if (!msg.ok)
  {
    os << "BAD " << msg.ret;
    buf = os.str();
    return;
  }

  os << "OK " << msg.ret << " " << static_cast<int>(msg.enabled) << " " << static_cast<int>(msg.active);
  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryBlueviewStatus& msg)
{
  msg.ok = buf.find("SONAR_OK") != buf.npos;
  if (msg.ok)
  {
    const auto num_scanned =
        sscanf(buf.data(), "%f SONAR_OK %u %hhu %hhu", &msg.time, &msg.ret, &msg.enabled, &msg.active);
    return num_scanned == 4;
  }

  const auto num_scanned = sscanf(buf.data(), "%f SONAR_BAD %u", &msg.time, &msg.ret);
  return num_scanned == 2;
}

template <>
void serialize(const sentry_acomms::SentryControlAndActuatorState& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  os << std::fixed << std::setprecision(0);

  os << static_cast<int>(msg.active_controller) << " " << static_cast<int>(msg.active_allocation) << " " << msg.force_u
     << " " << msg.force_w << " " << msg.torque_q << " "

     << std::setprecision(1) << msg.fwd_servo_deg << " " << msg.aft_servo_deg << " "

     << std::hex << std::uppercase << static_cast<int>(msg.fwd_servo_switch) << " "
     << static_cast<int>(msg.aft_servo_switch) << " "

     << static_cast<int>(msg.fwd_servo_speed) << " " << static_cast<int>(msg.aft_servo_speed) << " "

     << std::dec << msg.fwd_port_thruster_desired_current << " " << msg.fwd_port_thruster_measured_current << " "

     << msg.fwd_stbd_thruster_desired_current << " " << msg.fwd_stbd_thruster_measured_current << " "

     << msg.aft_port_thruster_desired_current << " " << msg.aft_port_thruster_measured_current << " "

     << msg.aft_stbd_thruster_desired_current << " " << msg.aft_stbd_thruster_measured_current;

  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryControlAndActuatorState& msg)
{
  const auto num_scanned =
      sscanf(buf.data(), "%hu %hu %f %f %f %f %f %hhX %hhX %hhX %hhX %f %f %f %f %f %f %f %f", &msg.active_controller,
             &msg.active_allocation, &msg.force_u, &msg.force_w, &msg.torque_q, &msg.fwd_servo_deg, &msg.aft_servo_deg,
             &msg.fwd_servo_switch, &msg.aft_servo_switch, &msg.fwd_servo_speed, &msg.aft_servo_speed,
             &msg.fwd_port_thruster_desired_current, &msg.fwd_port_thruster_measured_current,
             &msg.fwd_stbd_thruster_desired_current, &msg.fwd_stbd_thruster_measured_current,
             &msg.aft_port_thruster_desired_current, &msg.aft_port_thruster_measured_current,
             &msg.aft_stbd_thruster_desired_current, &msg.aft_stbd_thruster_measured_current);

  return num_scanned == 19;
}

template <>
void serialize(const sentry_acomms::SentryScalarScience& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  // Given the expected magnitudes of the data fields, uniform two decimal
  //  precision will yield a 35-character message.
  os << std::fixed;
  os << std::setprecision(2) << msg.oxygen_concentration << " "
     << std::setprecision(4) << msg.obs_raw << " " << msg.orp_raw << " " 
     << std::setprecision(2) << msg.ctd_temperature << " "
     << std::setprecision(4) << msg.ctd_salinity << " "
     << std::setprecision(2) << msg.paro_depth;
  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryScalarScience& msg)
{
  const auto num_scanned = sscanf(buf.data(), "%f %f %f %f %f %f",
				  &msg.oxygen_concentration, &msg.obs_raw,
				  &msg.orp_raw, &msg.ctd_temperature,
				  &msg.ctd_salinity, &msg.paro_depth);

  return num_scanned == 6;
}

template <>
void serialize(const sentry_acomms::SentryKongsbergStatus& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  os << std::fixed << std::setprecision(0) << std::dec;
  // Status
  os << "S";
  os << static_cast<int>(msg.pu_good);
  os << static_cast<int>(msg.sensors_good);
  os << static_cast<int>(msg.pinging) << " ";
  // RT Depth Settings
  os << "D";
  os << msg.rt_force_depth << " ";
  os << msg.rt_min_depth << " ";
  os << msg.rt_max_depth << " ";
  os << msg.rt_ping_freq << " ";
  os << static_cast<int>(msg.rt_fm_disable);
  os << static_cast<int>(msg.rt_log_watercol);
  os << static_cast<int>(msg.rt_extra_detect) << " ";
  os << msg.rt_depth_mode << " ";
  // RT TXMIT Settings
  os << "T";
  os << msg.rt_tx_angle_along << " ";
  os << msg.rt_max_ping_rate << " ";
  os << msg.rt_min_swath_distance << " ";
  os << static_cast<int>(msg.rt_pitch_stab);
  os << static_cast<int>(msg.rt_trigger) << " ";
  // Latest ping
  os << "#"  << msg.ping_num << " ";
  os << msg.num_soundings << " ";
  os << msg.percent_good << " ";
  os << msg.max_depth << " ";
  os << msg.min_depth << " ";
  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryKongsbergStatus& msg)
{
  const auto start_index = buf.find("S");
  if (start_index == buf.npos)
  {
    return false;
  }

  auto status = std::array<char, 64>{};
  status.fill('\0');

  char depth_mode[64] = {0};

  const auto num_scanned =
      sscanf(buf.c_str() + start_index, "S%1hhd%1hhd%1hhd D%f %hd %hd %hd %1hhd%1hhd%1hhd %s T%f %f %hd %1hhd%1hhd #%hd %hd %hd %hd %hd",
             &msg.pu_good,
             &msg.sensors_good,
             &msg.pinging,
             // Depth settings
             &msg.rt_force_depth,
             &msg.rt_min_depth,
             &msg.rt_max_depth,
             &msg.rt_ping_freq,
             &msg.rt_fm_disable,
             &msg.rt_log_watercol,
             &msg.rt_extra_detect,
             &depth_mode[0],
             // Txmit settings
             &msg.rt_tx_angle_along,
             &msg.rt_max_ping_rate,
             &msg.rt_min_swath_distance,
             &msg.rt_pitch_stab,
             &msg.rt_trigger,
             // Latest ping
             &msg.ping_num,
             &msg.num_soundings,
             &msg.percent_good,
             &msg.max_depth,
             &msg.min_depth);

  msg.rt_depth_mode.assign(status.data());
  if (num_scanned != 20)
  {
    return false;
  }
  msg.rt_depth_mode.assign(depth_mode);
  return true;
}

template <>
void serialize(const sentry_acomms::SentryKongsbergStatus2& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  os << std::fixed << std::setprecision(1) << std::dec;
  // Status
  os << "S";
  os << static_cast<int>(msg.status_position);
  os << static_cast<int>(msg.status_attitude);
  os << static_cast<int>(msg.status_sound_velocity);
  os << static_cast<int>(msg.status_time_sync);
  os << static_cast<int>(msg.status_pps);
  os << static_cast<int>(msg.status_depth);
  // RT Depth Settings
  os << "|P";
  os << static_cast<int>(msg.status_pinging);
  os << static_cast<int>(msg.status_water_column) << " ";
  os << msg.ping_frequency << " ";
  os << msg.approx_ping_rate;
  os << "|R";
  os << static_cast<int>(msg.status_recording) << " ";
  os << msg.ping_num << " ";
  os << msg.num_soundings << " ";
  os << msg.percent_good << " ";
  os << msg.min_depth << " ";
  os << msg.max_depth << " ";
  os << msg.center_depth << " ";
  os << msg.swath_width;

  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryKongsbergStatus2& msg)
{
  const auto start_index = buf.find("S");
  if (start_index == buf.npos)
  {
    return false;
  }

  const auto num_expected = 18; 
  const auto num_scanned = 
      sscanf(buf.substr(start_index).c_str(), 
             "S%1hhd%1hhd%1hhd%1hhd%1hhd%1hhd|P%1h%1hhd%1hhd %hd %f|R%1hhd %hd %hd %hd %hd %hd %hd %hd",
             &msg.status_position,
             &msg.status_attitude,
             &msg.status_sound_velocity,
             &msg.status_time_sync,
             &msg.status_pps,
             &msg.status_depth,
             // Ping group
             &msg.status_pinging,
             &msg.status_water_column,
             &msg.ping_frequency,
             &msg.approx_ping_rate,
             // Recording group
             &msg.status_recording,
             &msg.ping_num,
             &msg.num_soundings,
             &msg.percent_good,
             &msg.min_depth,
             &msg.max_depth,
             &msg.center_depth,
             &msg.swath_width
             );

  return num_scanned == num_expected;
}

template <>
void serialize(const sentry_acomms::SentryResonStatus& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  os << std::fixed << std::setprecision(1) << std::noboolalpha;

  os << "RSTAT "
     << "REC" << static_cast<int>(msg.recording) << ","
     << "S=" << msg.status << ","
     << "CMD=" << msg.mode << ","
     << "0"
     << ","
     << "#" << msg.ping_number << ","
     << "AP" << msg.auto_pilot << ","
     << "GAIN" << msg.gain << ","
     << "RNG" << msg.range << ","
     << "PWR" << msg.power << ","
     << "CA" << msg.coverage_angle_deg;
  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryResonStatus& msg)
{
  const auto start_index = buf.find("RSTAT");
  if (start_index == buf.npos)
  {
    return false;
  }

  auto status = std::array<char, 64>{};
  status.fill('\0');

  const auto num_scanned =
      sscanf(buf.substr(start_index).data(), "RSTAT REC%hhu,S=%63[^,],CMD=%hd,%*d,#%u,AP%hd,GAIN%f,RNG%f,PWR%f,CA%f",
             &msg.recording, status.data(), &msg.mode, &msg.ping_number, &msg.auto_pilot, &msg.gain, &msg.range,
             &msg.power, &msg.coverage_angle_deg);

  msg.status = std::string{ status.data() };

  if (num_scanned != 9)
  {
    return false;
  }

  return true;
}

template<>
void serialize(const sentry_acomms::SentryDvlStatus& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  os << std::fixed;
  os << std::setprecision(2) << msg.altitude << " "
     << std::setprecision(2) << msg.course_gnd << " "
     << std::setprecision(2) << msg.speed_gnd << " "
     << std::setprecision(2) << msg.num_good_beams << " "
     << std::setprecision(2) << msg.time_last_raw_dvl << " "
     << std::setprecision(2) << msg.time_good_parse << " "
     << std::setprecision(0) << msg.window_size << " "
     << std::setprecision(0) << msg.parses_tried_in_window << " "
     << std::setprecision(0) << msg.parses_ok_in_window;

  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryDvlStatus& msg)
{
  const auto num_scanned = sscanf(buf.data(), "%lf %lf %lf %hd %hd %hd %hd %d %d",
                                  &msg.altitude, &msg.course_gnd,
                                  &msg.speed_gnd, &msg.num_good_beams, &msg.time_last_raw_dvl, 
                                  &msg.time_good_parse, &msg.window_size, &msg.parses_tried_in_window, &msg.parses_ok_in_window);

  return num_scanned == 9;
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::ThrustersEnabledCommand& msg)
{
  auto start_index = buf.find("THRUSTERS_ENABLED ");
  if (start_index == buf.npos)
  {
    return false;
  }

  int value;

  const auto num_scanned = sscanf(buf.substr(start_index).data(), "THRUSTERS_ENABLED %d", &value);
  msg.enabled = value > 0;
  return num_scanned == 1;
}

template <>
void serialize(const sentry_acomms::ThrustersEnabledCommand& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  os << "THRUSTERS_ENABLED " << static_cast<int>(msg.enabled);
  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::UdpQueueCommand& msg)
{
  auto start_idx = buf.find("UDP");
  if (start_idx == buf.npos)
  {
    std::cout << "Couldn't find UDP tag" << std::endl;
    return false;
  }

  auto split_idx = buf.find(" ", start_idx);
  if (split_idx == buf.npos)
  {
    std::cout << "Couldn't find space after UDP tag" << std::endl;
    return false;
  }

  // actually split the string into its bits
  msg.tag = buf.substr(0, split_idx);
  msg.command = buf.substr(split_idx + 1);

  std::cout << "Got TAG:\"" << msg.tag << "\", CMD:\"" << msg.command << "\"" << std::endl;

  return true;
}

template <>
void serialize(const sentry_acomms::UdpQueueCommand& msg, std::string& buf)
{
  buf = msg.tag + ' ' + msg.command;
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::AcousticScheduleCommand& msg)
{
  const auto regex = std::regex(R"(ASCHED (\d+) (\d+)(.*))");
  auto smatch = std::smatch{};

  if (!std::regex_match(buf, smatch, regex))
  {
    return false;
  }

  auto int_value = std::stoi(smatch[1].str());
  if (int_value > std::numeric_limits<std::uint8_t>::max())
  {
    return false;
  }
  msg.modem = static_cast<uint8_t>(int_value);

  int_value = std::stoi(smatch[2].str());
  if (int_value > std::numeric_limits<std::uint8_t>::max())
  {
    return false;
  }

  msg.command = static_cast<uint8_t>(int_value);

  return true;
}

template <>
void serialize(const sentry_acomms::AcousticScheduleCommand& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  os << "ASCHED " << static_cast<int>(msg.modem) << " " << static_cast<int>(msg.command);
  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryMxEvent& msg) {
  std::string key = sentry_acomms::SentryQueueManagerCommandEnum::MX_EVENT_CMD_STR;
  auto split_idx = buf.find(key);
  if (split_idx == buf.npos) {
    std::cout << "Couldn't find " << key << " tag" << std::endl;
    return false;
  }

  // actually split the string into its bits
  size_t evt_start = split_idx + key.length() + 1;
  if (evt_start >= buf.length()) {
    // we got an empty MEVT string
    return false;
  }
  msg.event = buf.substr(evt_start);

  return true;
}

template <>
void serialize(const sentry_acomms::SentryMxEvent& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  os <<sentry_acomms::SentryQueueManagerCommandEnum::MX_EVENT_CMD_STR <<" " <<msg.event;
  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryMxSharedParam& msg)
{
  std::string key = sentry_acomms::SentryQueueManagerCommandEnum::MX_SHARED_PARAM_CMD_STR;
  auto idx = buf.find(key);
  if (idx == buf.npos) {
    std::cout << "Couldn't find " << key << " tag" << std::endl;
    return false;
  }
  idx += key.length() + 1; // extra for the space
  if (idx >= buf.length()) {
    // got a MXSP but no payload
    return false;
  }

  while (idx != buf.npos && idx < buf.length()) {
    ds_core_msgs::KeyString value;

    auto colon_idx = buf.find(':', idx);
    auto comma_idx = buf.find(',', idx);

    if (colon_idx == buf.npos) {
      ROS_ERROR_STREAM("Couldn't find colon in key/value pair, skipping");
      return false;
    }

    value.key = buf.substr(idx, colon_idx-idx);

    if (comma_idx != buf.npos) {
      if (colon_idx+1 + comma_idx - colon_idx - 1 > buf.size()) {
        ROS_ERROR_STREAM("Error parsing param values: bad length");
        return false;
      }
      // there's a comma
      value.value = buf.substr(colon_idx+1, comma_idx - colon_idx - 1);
      idx = comma_idx+1;
    } else {
      if (colon_idx+1 >= buf.size()) {
        ROS_ERROR_STREAM("Error parsing param values: bad length");
        return false;
      }
      value.value = buf.substr(colon_idx + 1);
      idx = comma_idx;
    }
    msg.values.push_back(value);
  }
  return true;
}

template <>
void serialize(const sentry_acomms::SentryMxSharedParam& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  os <<sentry_acomms::SentryQueueManagerCommandEnum::MX_SHARED_PARAM_CMD_STR <<" ";

  for (size_t i=0; i<msg.values.size(); i++) {
    os <<msg.values[i].key <<":" <<msg.values[i].value <<",";
  }
  buf = os.str();
  buf.pop_back(); // remove last character, as its unnecessary
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryMxStatus& msg) {
  // copied from shift
  auto is = std::istringstream{ buf };

  // Attempt to parse a time -- should be present on all responses.
  auto facet = new boost::posix_time::time_input_facet("%H:%M:%S");
  is.imbue(std::locale(is.getloc(), facet));

  auto time = boost::posix_time::ptime{};
  is >> time;

  if (!is)
  {
    return false;
  }

  // Date parsing.. is complicated..  We only send HH:MM, so need to be mindful of
  // sending things with timestamps in the previous day.

  // This is the current time according to ROS
  auto boost_stamp = ros::Time::now().toBoost();
  // And the current date
  auto boost_date = boost_stamp.date();

  // If the parsed hours are LESS than the current hours, then we have a situation where
  // we are sending a timestamp from the next day-- or it already expired.
  // We'll have to trust the timeout time, and add one day from the current date
  if (time.time_of_day().hours() < boost_stamp.time_of_day().hours())
  {
    boost_date += boost::gregorian::days(1);
  }

  // Create our timestamp.
  boost_stamp = boost::posix_time::ptime{ boost_date, time.time_of_day() };
  msg.next_timeout = msg.next_timeout.fromBoost(boost_stamp);

  // load our UUID
  std::string uuidstr;
  is >> uuidstr;

  if (uuidstr.size() < 32) {
    ROS_WARN_STREAM("MX status UUID string too short.  Need 32 bytes, have " << uuidstr.size());
    return false;
  }

  for (size_t i=0; i<16; i++) {
    msg.primitive_uuid[i] = std::stoul(uuidstr.substr(i*2, 2), nullptr, 16);
  }
  return true;
}

template <>
void serialize(const sentry_acomms::SentryMxStatus& msg, std::string& buf) {
  auto os = std::ostringstream{};
  auto facet = new boost::posix_time::time_facet("%H:%M:%S");
  os.imbue(std::locale(os.getloc(), facet));

  os << msg.next_timeout.toBoost() << " " <<std::setfill('0') <<std::hex;

  for (const auto& byte : msg.primitive_uuid) {
    // setw isn't presistent
    os <<std::setw(2) <<static_cast<int>(byte);
  }

  buf = os.str();
}

template <>
void serialize(const sentry_acomms::SentryNavShift& msg, std::string& buf) {
 std::ostringstream os;
 os <<sentry_acomms::SentryQueueManagerCommandEnum::NAV_SHIFT_CMD_STR;
 os <<" " <<msg.easting <<" " <<msg.northing;
 buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryNavShift& msg) {
  std::string key = sentry_acomms::SentryQueueManagerCommandEnum::NAV_SHIFT_CMD_STR;
  auto idx = buf.find(key);
  if (idx == buf.npos) {
    std::cout << "Couldn't find " << key << " tag" << std::endl;
    return false;
  }
  idx += key.length() + 1; // extra for the space
  if (idx >= buf.length()) {
    // got a Key but no payload
    return false;
  }

  std::istringstream is(buf.substr(idx));

  if (is.eof()) { return false; }
  is >> msg.easting;

  if (is.eof()) { return false; }
  is >> msg.northing;

  return true;
}

template<>
bool deserialize(const std::string& buf, sentry_acomms::KongsbergParamCommand& msg)
{
  const auto start_index = buf.find("KM_PARAM");
  if (start_index == buf.npos)
  {
    return false;
  }
  auto msg_substr = buf.substr(start_index);
  char param_name[30];
  char param_value[30];

  const auto parenth_index = buf.find("\"");
  if (parenth_index == buf.npos){
    auto num_scanned = sscanf(msg_substr.data(), "KM_PARAM %s %s", param_name, param_value);
    if (num_scanned != 2)
      return false;
    msg.param_name = std::string{param_name};
    msg.param_value = std::string{param_value};
  } else { //Multiword! Look for the string contained within parentheses
    auto num_scanned = sscanf(msg_substr.data(), "KM_PARAM %s \"%[^\"]\"", param_name, param_value);
    if (num_scanned != 2)
      return false;
    msg.param_name = std::string{param_name};
    msg.param_value = "\"" + std::string{param_value} + "\"";
  }
  return true;
}

template <>
void serialize(const sentry_acomms::KongsbergParamCommand& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  os << "KM_PARAM " << msg.param_name << " " << msg.param_value;
  buf = os.str();
}
template<>
bool deserialize(const std::string& buf, sentry_acomms::KongsbergPingCommand& msg)
{
  const auto start_index = buf.find("KM_PING");
  if (start_index == buf.npos)
  {
    return false;
  }
  auto msg_substr = buf.substr(start_index);
  uint32_t ping = 0;
  auto num_scanned = sscanf(msg_substr.data(), "KM_PING %u", &ping);
  if (num_scanned != 1)
    return false;
  msg.ping = ping;
  return true;
}
template <>
void serialize(const sentry_acomms::KongsbergPingCommand& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  os << "KM_PING " << msg.ping ;
  buf = os.str();
}

template<>
bool deserialize(const std::string& buf, sentry_acomms::KongsbergKctrlCommand& msg)
{
  const auto start_index = buf.find("KM_KCTRL");
  if (start_index == buf.npos)
  {
    return false;
  }
  auto msg_substr = buf.substr(start_index);
  uint32_t kctrl = 0;
  auto num_scanned = sscanf(msg_substr.data(), "KM_KCTRL %u", &kctrl);
  if (num_scanned != 1)
    return false;
  msg.kctrl = kctrl;
  return true;
}
template <>
void serialize(const sentry_acomms::KongsbergKctrlCommand& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  os << "KM_KCTRL " << msg.kctrl ;
  buf = os.str();
}
template <>
bool deserialize(const std::string& buf, sentry_acomms::SentrySypridStatus& msg)
{
/*   int items = sscanf(buf.data(),"PKZ %d %lf %lf %lf %lf %lf %d %lf %lf %lf %lf %lf",
  */
    int items = sscanf(buf.data(),"PKZ %d %f %f %f %f %d %f %f %f %f %d %d %d %d",
            
		     &msg.dcam_port_state,
                     &msg.elmo_port_torque_desired,
                     &msg.elmo_port_volts_latest,
                     &msg.elmo_port_amps_active_latest,
                     &msg.elmo_port_rpm_latest,
                     &msg.dcam_stbd_state,
                     &msg.elmo_stbd_torque_desired,
                     &msg.elmo_stbd_volts_latest,
                     &msg.elmo_stbd_amps_active_latest,
                     &msg.elmo_stbd_rpm_latest,
                     &msg.dcam_persistence_state,
		     &msg.flow_sensor_state,
		     &msg.flow_running_avg,
		     &msg.flow_total_counts);

  if (items == 14){
    return true;
  }
  return false;
}

template <>
void serialize(const sentry_acomms::SentrySypridStatus& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  os << "PKZ ";
  os << msg.dcam_port_state << " ";
  os << msg.elmo_port_torque_desired << " ";
  os << msg.elmo_port_volts_latest << " ";
  os << msg.elmo_port_amps_active_latest << " ";
  os << msg.elmo_port_rpm_latest << " ";
  os << msg.dcam_stbd_state << " ";
  os << msg.elmo_stbd_torque_desired << " ";
  os << msg.elmo_stbd_volts_latest << " ";
  os << msg.elmo_stbd_amps_active_latest << " ";
  os << msg.elmo_stbd_rpm_latest << " ";
  os << msg.dcam_persistence_state << " ";
  os << msg.flow_sensor_state << " ";
  os << msg.flow_running_avg << " ";
  os << msg.flow_total_counts;
  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::SypridCommand& msg)
{
  if (buf.find("PKZ REQUEST FLOW SENSOR DATA") != std::string::npos){
   msg.pkz_cmd = sentry_acomms::SypridCommand::PKZ_REQUEST_FLOW_SENSOR_DATA;
  }
  else if (buf.find("PKZ ENABLE DCAM PERSISTENCE") != std::string::npos){
    msg.pkz_cmd = sentry_acomms::SypridCommand::PKZ_ENABLE_DCAM_PERSISTENCE;
  }
  else if (buf.find("PKZ DISABLE DCAM PERSISTENCE") != std::string::npos){
    msg.pkz_cmd = sentry_acomms::SypridCommand::PKZ_DISABLE_DCAM_PERSISTENCE;
  }
  else if (buf.find("PKZ START ") != std::string::npos){
    double t1 = 0;
    double t2 = 0;
    auto num_scanned = sscanf(buf.data(), "PKZ START ALL %lf %lf", &t1, &t2);
    if (num_scanned == 2){
      msg.pkz_cmd = sentry_acomms::SypridCommand::PKZ_START_ALL;
      msg.torque_1 = t1;
      msg.torque_2 = t2;
    } else {
      num_scanned = sscanf(buf.data(), "PKZ START PORT %lf", &t1);
      if (num_scanned == 1){
        msg.pkz_cmd = sentry_acomms::SypridCommand::PKZ_START_PORT;
        msg.torque_1 = t1;
      } else {
        num_scanned = sscanf(buf.data(), "PKZ START STBD %lf", &t1);
        if (num_scanned == 1){
          msg.pkz_cmd = sentry_acomms::SypridCommand::PKZ_START_STBD;
          msg.torque_1 = t1;
        } else {
          return false;
        }
      }
    }
  } else if (buf.find("PKZ STOP ALL") != std::string::npos){
    msg.pkz_cmd = sentry_acomms::SypridCommand::PKZ_STOP_ALL;
  } else if (buf.find("PKZ STOP PORT") != std::string::npos){
    msg.pkz_cmd = sentry_acomms::SypridCommand::PKZ_STOP_PORT;
  } else if (buf.find("PKZ STOP STBD") != std::string::npos){
    msg.pkz_cmd = sentry_acomms::SypridCommand::PKZ_STOP_STBD;
  } else if (buf.find("PKZ OPEN PORT") != std::string::npos){
    msg.pkz_cmd = sentry_acomms::SypridCommand::PKZ_OPEN_DCAM_PORT;
  } else if (buf.find("PKZ OPEN STBD") != std::string::npos){
    msg.pkz_cmd = sentry_acomms::SypridCommand::PKZ_OPEN_DCAM_STBD;
  } else if (buf.find("PKZ CLOSE PORT") != std::string::npos){
    msg.pkz_cmd = sentry_acomms::SypridCommand::PKZ_CLOSE_DCAM_PORT;

  } else if (buf.find("PKZ CLOSE STBD") != std::string::npos){
    msg.pkz_cmd = sentry_acomms::SypridCommand::PKZ_CLOSE_DCAM_STBD;
  } else if (buf.find("PKZ TORQUE ") != std::string::npos){
    double t = 0;
    auto num_scanned = sscanf(buf.data(), "PKZ TORQUE PORT %lf", &t);
    if (num_scanned == 1){
      msg.pkz_cmd = sentry_acomms::SypridCommand::PKZ_TORQUE_PORT;
      msg.torque_1 = t;
    } else {
      num_scanned = sscanf(buf.data(), "PKZ TORQUE STBD %lf", &t);
      if (num_scanned == 1){
        msg.pkz_cmd = sentry_acomms::SypridCommand::PKZ_TORQUE_STBD;
        msg.torque_1 = t;
      } else {
        return false;
      }
    }
  } else {
    return false;
  }
  
  return true;
}

template <>
void serialize(const sentry_acomms::SypridCommand& msg, std::string& buf)
{
  switch (msg.pkz_cmd){
    case sentry_acomms::SypridCommand::PKZ_START_ALL:
      buf = "PKZ START ALL "+std::to_string(msg.torque_1) + " " + std::to_string(msg.torque_2);
      break;
    case sentry_acomms::SypridCommand::PKZ_STOP_ALL:
      buf = "PKZ STOP ALL";
      break;
    case sentry_acomms::SypridCommand::PKZ_START_PORT:
      buf = "PKZ START PORT " + std::to_string(msg.torque_1);
      break;
    case sentry_acomms::SypridCommand::PKZ_START_STBD:
      buf = "PKZ START STBD " + std::to_string(msg.torque_1);
      break;
    case sentry_acomms::SypridCommand::PKZ_STOP_PORT:
      buf = "PKZ STOP PORT";
      break;
    case sentry_acomms::SypridCommand::PKZ_STOP_STBD:
      buf = "PKZ STOP STBD";
      break;
    case sentry_acomms::SypridCommand::PKZ_OPEN_DCAM_PORT:
      buf = "PKZ OPEN PORT";
      break;
    case sentry_acomms::SypridCommand::PKZ_OPEN_DCAM_STBD:
      buf = "PKZ OPEN STBD";
      break;
    case sentry_acomms::SypridCommand::PKZ_CLOSE_DCAM_PORT:
      buf = "PKZ CLOSE PORT";
      break;
    case sentry_acomms::SypridCommand::PKZ_CLOSE_DCAM_STBD:
      buf = "PKZ CLOSE STBD";
      break;
    case sentry_acomms::SypridCommand::PKZ_ENABLE_DCAM_PERSISTENCE:
      buf = "PKZ ENABLE DCAM PERSISTENCE";
      break;
    case sentry_acomms::SypridCommand::PKZ_DISABLE_DCAM_PERSISTENCE:
      buf = "PKZ DISABLE DCAM PERSISTENCE";
      break;
    case sentry_acomms::SypridCommand::PKZ_REQUEST_FLOW_SENSOR_DATA:
      buf = "PKZ REQUEST FLOW SENSOR DATA";
      break;
    case sentry_acomms::SypridCommand::PKZ_TORQUE_PORT:
      buf = "PKZ TORQUE PORT " + std::to_string(msg.torque_1);
      break;
    case sentry_acomms::SypridCommand::PKZ_TORQUE_STBD:
      buf = "PKZ TORQUE STBD " + std::to_string(msg.torque_1);
      break;
    default:
      break;
  }
}

template <>
void serialize(const sentry_acomms::SentryIns& msg, std::string& buf)
{
  auto ss = std::ostringstream{};
  
  ss << std::fixed << std::setprecision(0);
  
  // - user w1 (phinsstdv3.h), high level interpretation of system - 8 chars
  ss << std::uppercase << std::setfill('0') << std::setw(8) << std::hex << msg.phins.ins_user_status << ",";
  // - system w2 (phinsstdv3.h) - 8 chars uint32
  ss << std::uppercase << std::setfill('0') << std::setw(8) << std::hex << msg.phins.ins_system_status[1] << ",";
  // - algorithm w1 (phinsstdv3.h) - 8 chars
  ss << std::uppercase << std::setfill('0') << std::setw(8) << std::hex << msg.phins.ins_algo_status[0] << ",";
  // - algorithm w2 (phinsstdv3.h) - 8 chars
  ss << std::uppercase << std::setfill('0') << std::setw(8) << std::hex << msg.phins.ins_algo_status[1] << ",";
  // - algorithm w3 (phinsstdv3.h) - 8 chars
  ss << std::uppercase << std::setfill('0') << std::setw(8) << std::hex << msg.phins.ins_algo_status[2] << ",";
  // - algorithm w4 (phinsstdv3.h) - 8 chars
  ss << std::uppercase << std::setfill('0') << std::setw(8) << std::hex << msg.phins.ins_algo_status[3];


  buf.append(ss.str());
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryIns& msg)
{
  return true;
}

template <>
void serialize(const sentry_acomms::SentryInsNav& msg, std::string& buf)
{
  auto ss = std::ostringstream{};
  
  ss << std::fixed << std::setprecision(0);
  
  // lon, lat, altitue
  ss << std::setprecision(6) << msg.phins.longitude << "," << msg.phins.latitude << "," << std::setprecision(1) << msg.phins.altitude << ",";
  // - hdg stddev
  ss << std::setprecision(3) << msg.phins.heading_stddev << ",";
  // - vehicle frame vx,vy
  ss << std::setprecision(2) << msg.phins.body_velocity_XVn[0] << "," << msg.phins.body_velocity_XVn[1];


  buf.append(ss.str());
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryInsNav& msg)
{
  return true;
}

template <>
void serialize(const ds_core_msgs::DsStringStamped& msg, std::string& buf){
  buf.append(msg.payload);
}

template <>
bool deserialize(const std::string& buf, ds_core_msgs::DsStringStamped& msg){
  //Can add any string msg handling here if you'd like. Defaulted the bahvior to if the case is unhandled
  // to return false. -tj
  if (buf.find("SUPR") != std::string::npos){
   msg.payload = buf;
  }

  else {
    return false;
  }
  return true;
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::SuprCommand& msg)
{
  if (buf.find("SUPR SAMPLE") != std::string::npos){
   msg.supr_cmd = sentry_acomms::SuprCommand::START_SAMPLE;
  }
   else {
    return false;
  }
  
  return true;
}

template <>
void serialize(const sentry_acomms::SuprCommand& msg, std::string& buf)
{
  switch (msg.supr_cmd){
    case sentry_acomms::SuprCommand::START_SAMPLE:
      buf = "SUPR SAMPLE";
      break;
    default:
      break;
  }
}

template <>
void serialize(const sentry_acomms::SetActiveDepthGoalCommand& msg, std::string& buf)
{
  auto os = std::ostringstream{};
  os << std::fixed << std::setprecision(2);
  os << "SADG " << static_cast<int>(msg.goal);
  buf = os.str();
}

template <>
bool deserialize(const std::string& buf, sentry_acomms::SetActiveDepthGoalCommand& msg)
{
  auto start_index = buf.find("SADG ");
  if (start_index == buf.npos)
  {
    msg.goal = msg.INDEX_INVALID;
    return false;
  }

  const auto num_scanned = sscanf(buf.substr(start_index).data(), "SADG %hhu", &msg.goal);

  if (msg.goal < sentry_acomms::SetActiveDepthGoalCommand::INDEX_MIN ||
      msg.goal > sentry_acomms::SetActiveDepthGoalCommand::INDEX_MAX)
  {
    msg.goal = msg.INDEX_INVALID;
    return false;
  }

  return num_scanned == 1;
}

}//namespace
