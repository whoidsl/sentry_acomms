/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_acomms/vehicle_leg_goal_queue.h"
#include "sentry_acomms/serialization.h"
#include "ds_control_msgs/GoalLegState.h"

namespace sentry_acomms
{
VehicleLegGoalQueue::VehicleLegGoalQueue() : SimpleSingleQueue<SentryGoalLegState>()
{
}

VehicleLegGoalQueue::~VehicleLegGoalQueue() = default;

void VehicleLegGoalQueue::setup(ros::NodeHandle& nh)
{
  const auto goals_ns = nh.param<std::string>("goals_ns", "");

  auto node_name = std::string{};
  auto str = std::string{};

  str = ros::names::resolve(ros::this_node::getName(), std::string{ "leg_goal_node" });
  if (!ros::param::get(str, node_name))
  {
    ROS_FATAL_STREAM("Parameter: '" << str << "' is not defined");
    ROS_BREAK();
  }

  if (!goals_ns.empty())
  {
    node_name = ros::names::resolve(goals_ns, node_name);
  }

  str = ros::names::resolve(node_name, std::string{ "internal_state" });
  leg_sub_ = nh.subscribe(str, 1, &VehicleLegGoalQueue::goalLegCallback, this);
}

void VehicleLegGoalQueue::goalLegCallback(const ds_control_msgs::GoalLegState& msg)
{
  item_.line_start.x = static_cast<float>(msg.line_start.x);
  item_.line_start.y = static_cast<float>(msg.line_start.y);
  item_.line_end.x = static_cast<float>(msg.line_end.x);
  item_.line_end.y = static_cast<float>(msg.line_end.y);

  item_.kappa = static_cast<float>(msg.kappa);
  item_.angle_line_segment = static_cast<float>(msg.angle_line_segment);
  item_.off_line_vect = static_cast<float>(msg.off_line_vect);
  item_.sign_of_vect = static_cast<float>(msg.sign_of_vect);

  item_.old_goal = static_cast<float>(msg.old_goal);
  item_.new_goal = static_cast<float>(msg.new_goal);

  item_.leg_number = msg.leg_number;
}
}
