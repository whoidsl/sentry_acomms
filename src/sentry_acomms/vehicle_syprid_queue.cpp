/**
* Copyright 2020 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

//
// Created by jvaccaro on 3/5/20.
//

#include "sentry_acomms/vehicle_syprid_queue.h"
#include "sentry_msgs/SypridState.h"

namespace sentry_acomms
{
VehicleSypridStatusQueue::VehicleSypridStatusQueue() : SimpleSingleQueue<SentrySypridStatus>()
{
}

VehicleSypridStatusQueue::~VehicleSypridStatusQueue() = default;

void VehicleSypridStatusQueue::setup(ros::NodeHandle& nh)
{
  auto topic_name = std::string{};
  topic_name = ros::names::resolve(ros::this_node::getName(),
                                   std::string{"sypridstatus_topic"});
  auto syprid_topic = std::string{};
  if (!ros::param::get(topic_name, syprid_topic))
  {
    ROS_FATAL_STREAM("Parameter: '" << topic_name << "' is not defined");
    ROS_BREAK();
  }
  syprid_sub_ = nh.subscribe(syprid_topic, 1,
                             &VehicleSypridStatusQueue::pkzStatusCallback, this);
}

void VehicleSypridStatusQueue::pkzStatusCallback(const sentry_msgs::SypridState& msg)
{
//  ## Syprid SDE messages not used on beinart-2022.
//  ## The formatting and comments below may be outdated
//  ## update comments as needed. Luckily order doesnt matter here.
//  ## "%d %.1f %.1f %.1f %.1f %.1f %d %.1f %.1f %.1f %.1f %.1f"
//  ## SDE 2015/07/27 06:59:59.515 Update message:
//  ## 1 0.0 -0.0 55.9 21.0 0.0 0 6.0 0.0 56.3 25.0 338.0
//  ##-1-2---3----4----5----6---7-8---9---10---11---12---
//  ##  1: port dcam reported state     dcam_port_state
//  ##  2: port active current (avg)    elmo_port_amps_active_avg
//  ##  3: port reactive current (avg)  elmo_port_amps_reactive_avg
//  ##  4: port line voltage (avg)      elmo_port_volts_avg
//  ##  5: port motor ctrlr temp (avg)  elmo_port_temp_avg
//  ##  6: port rpm (avg)               elmo_port_rpm_avg
//  ##  7: stbd dcam reported state     dcam_stbd_state
//  ##  8: stbd active current (avg)    elmo_stbd_amps_active_avg
//  ##  9: stbd reactive current (avg)  elmo_stbd_amps_reactive_avg
//  ## 10: stbd line voltage (avg)      elmo_stbd_volts_avg
//  ## 11: stbd motor ctrlr temp (avg)  elmo_stbd_temp_avg
//  ## 12: stbd rpm (avg)               elmo_stbd_rpm_avg
  item_.dcam_persistence_state = msg.dcam_persistence_state;
  item_.dcam_port_state = msg.dcam_port_state;
  item_.elmo_port_torque_desired = msg.elmo_port_torque_desired;
  item_.elmo_port_volts_latest = msg.elmo_port_volts_latest;
  item_.elmo_port_amps_active_latest = msg.elmo_port_amps_active_latest;
  item_.elmo_port_rpm_latest = msg.elmo_port_rpm_latest;
  item_.dcam_stbd_state = msg.dcam_stbd_state;
  item_.elmo_stbd_torque_desired = msg.elmo_stbd_torque_desired;
  item_.elmo_stbd_volts_latest = msg.elmo_stbd_volts_latest;
  item_.elmo_stbd_amps_active_latest = msg.elmo_stbd_amps_active_latest;
  item_.elmo_stbd_rpm_latest = msg.elmo_stbd_rpm_latest;
  item_.flow_sensor_state = msg.flow_sensor_state;
  item_.flow_running_avg = msg.flow_running_avg;
  item_.flow_total_counts = msg.flow_total_counts;
}

} // namespace sentry_acomms
