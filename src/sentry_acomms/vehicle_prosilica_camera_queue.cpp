/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_acomms/vehicle_prosilica_camera_queue.h"
#include "sentry_acomms/serialization.h"

#include "ds_base/ds_process.h"
#include "ds_asio/ds_connection.h"
#include "ds_core_msgs/RawData.h"

namespace sentry_acomms
{
VehicleProsilicaCameraQueue::VehicleProsilicaCameraQueue() : SimpleSingleQueue<SentryProsilicaCameraStatus>()
{
}

VehicleProsilicaCameraQueue::~VehicleProsilicaCameraQueue() = default;

void VehicleProsilicaCameraQueue::cameraCallback(const ds_core_msgs::RawData& msg)
{
  auto camera_status = sentry_acomms::SentryProsilicaCameraStatus{};
  if (!ds_acomms_serialization::deserialize(msg.data, camera_status, true))
  {
    ROS_WARN_STREAM("Failed to parse camera status string: '" << std::string(std::begin(msg.data), std::end(msg.data))
                                                              << "'");
    return;
  }

  item_ = camera_status;
}
}
