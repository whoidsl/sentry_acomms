/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_acomms/vehicle_state_queue.h"
#include "sentry_acomms/serialization.h"
#include "ds_core_msgs/Abort.h"
#include "ds_control_msgs/BottomFollow1D.h"
#include "ds_control_msgs/GoalLegState.h"
#include "ds_nav_msgs/AggregatedState.h"
#include "ds_hotel_msgs/BatMan.h"

namespace sentry_acomms
{
VehicleStateQueue::VehicleStateQueue() : SimpleSingleQueue<SentryVehicleState>()
{
}

VehicleStateQueue::~VehicleStateQueue() = default;

void VehicleStateQueue::setup(ros::NodeHandle& nh)
{
  // const auto controllers_ns = nh.param<std::string>("controllers_ns", "");
  const auto goals_ns = nh.param<std::string>("goals_ns", "");
  const auto nav_ns = nh.param<std::string>("nav_ns", "");
  const auto vehicle_ns = nh.param<std::string>("vehicle_ns", "");

  auto node_name = std::string{};
  auto str = std::string{};

  str = ros::names::resolve(ros::this_node::getName(), std::string{ "bottom_follower_node" });
  // Bottom follower
  if (!ros::param::get(str, node_name))
  {
    ROS_FATAL_STREAM("Parameter: '" << str << "' is not defined");
    ROS_BREAK();
  }

  if (!goals_ns.empty())
  {
    node_name = ros::names::resolve(goals_ns, node_name);
  }

  str = ros::names::resolve(node_name, std::string{ "bf_topic" });
  bf_goal_sub_ = nh.subscribe(str, 1, &VehicleStateQueue::bottomFollowerCallback, this);

  // Leg generator
  str = ros::names::resolve(ros::this_node::getName(), std::string{ "leg_goal_node" });
  if (!ros::param::get(str, node_name))
  {
    ROS_FATAL_STREAM("Parameter: '" << str << "' is not defined");
    ROS_BREAK();
  }

  if (!goals_ns.empty())
  {
    node_name = ros::names::resolve(goals_ns, node_name);
  }

  str = ros::names::resolve(node_name, std::string{ "internal_state" });
  leg_goal_sub_ = nh.subscribe(str, 1, &VehicleStateQueue::legGoalCallback, this);

  // Nav
  str = ros::names::resolve(ros::this_node::getName(), std::string{ "nav_aggregator_node" });
  if (!ros::param::get(str, node_name))
  {
    ROS_FATAL_STREAM("Parameter: '" << str << "' is not defined");
    ROS_BREAK();
  }

  if (!nav_ns.empty())
  {
    node_name = ros::names::resolve(nav_ns, node_name);
  }

  str = ros::names::resolve(node_name, std::string{ "state" });
  nav_sub_ = nh.subscribe(str, 1, &VehicleStateQueue::navStateCallback, this);

  // Battery
  str = ros::names::resolve(ros::this_node::getName(), std::string{ "batman_node" });
  if (!ros::param::get(str, node_name))
  {
    ROS_FATAL_STREAM("Parameter: '" << str << "' is not defined");
    ROS_BREAK();
  }

  if (!vehicle_ns.empty())
  {
    node_name = ros::names::resolve(vehicle_ns, node_name);
  }

  str = ros::names::resolve(node_name, std::string{ "state" });
  bat_sub_ = nh.subscribe(str, 1, &VehicleStateQueue::batmanCallback, this);

  // Abort
  node_name = ros::names::resolve(ros::this_node::getName(), std::string{ "abort_topic" });
  if (!ros::param::get(node_name, str))
  {
    ROS_FATAL_STREAM("Parameter: '" << node_name << "' is not defined");
    ROS_BREAK();
  }

  abort_sub_ = nh.subscribe(str, 1, &VehicleStateQueue::abortCallback, this);
}

void VehicleStateQueue::navStateCallback(const ds_nav_msgs::AggregatedState& msg)
{
#define FILL(FIELD) msg.FIELD.valid ? static_cast<float>(msg.FIELD.value) : SentryVehicleState::INVALID
  item_.heading = FILL(heading);
  item_.heading *= 180 / M_PI;
  item_.pos_ned.x = FILL(easting);
  item_.pos_ned.y = FILL(northing);
  item_.pos_ned.z = FILL(down);
  item_.vert_velocity = FILL(heave_w);
#undef FILL

  if (msg.surge_u.valid && msg.sway_v.valid)
  {
    item_.horz_velocity = static_cast<float>(std::sqrt(std::pow(msg.surge_u.value, 2) + std::pow(msg.sway_v.value, 2)));
  }
  else
  {
    item_.horz_velocity = SentryVehicleState::INVALID;
  }
}

void VehicleStateQueue::legGoalCallback(const ds_control_msgs::GoalLegState& msg)
{
  item_.goal_ned.x = static_cast<float>(msg.line_end.x);
  item_.goal_ned.y = static_cast<float>(msg.line_end.y);
  item_.trackline = static_cast<uint16_t>(msg.leg_number);
}

void VehicleStateQueue::bottomFollowerCallback(const ds_control_msgs::BottomFollow1D& msg)
{
  item_.goal_ned.z = static_cast<float>(msg.depth_goal);
  item_.altitude = static_cast<float>(msg.raw_altitude);
}

void VehicleStateQueue::batmanCallback(const ds_hotel_msgs::BatMan& msg)
{
  item_.battery_pct = static_cast<float>(msg.percentFull);
}

void VehicleStateQueue::abortCallback(const ds_core_msgs::Abort& msg)
{
  item_.abort_status = msg.abort;
}
}
