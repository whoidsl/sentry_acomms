/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_acomms/vehicle_shift_queue.h"
#include "sentry_acomms/serialization.h"
#include "ds_nav_msgs/Shift.h"

namespace sentry_acomms
{
VehicleShiftQueue::VehicleShiftQueue() : SimpleSingleQueue<SentryCurrentShift>()
{
}

VehicleShiftQueue::~VehicleShiftQueue() = default;

void VehicleShiftQueue::setup(ros::NodeHandle& nh)
{
  auto node_name = std::string{};
  auto str = std::string{};

  str = ros::names::resolve(ros::this_node::getName(), std::string{ "shift_topic" });

  if (!ros::param::get(str, node_name))
  {
    ROS_FATAL_STREAM("Parameter: '" << str << "' is not defined");
    ROS_BREAK();
  }

  shift_sub_ = nh.subscribe(node_name, 1, &VehicleShiftQueue::shiftCallback, this);
}

void VehicleShiftQueue::shiftCallback(const ds_nav_msgs::Shift& msg)
{
  // No updated needed.  (the shift topic publishes at a set rate)
  if (item_.x == msg.shift_easting && item_.y == msg.shift_northing)
  {
    return;
  }

  item_.stamp = msg.header.stamp;
  item_.x = static_cast<float>(msg.shift_easting);
  item_.y = static_cast<float>(msg.shift_northing);
}
}
