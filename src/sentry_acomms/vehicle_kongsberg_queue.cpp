/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

//
// Created by jvaccaro on 7/22/19.
//

#include "sentry_acomms/vehicle_kongsberg_queue.h"
#include "sentry_acomms/SentryKongsbergStatus2.h"
#include "sentry_acomms/serialization.h"

#include <ds_kctrl_msgs/PuInfoMessage.h>
#include <ds_kctrl_msgs/KctrlStatusLabel.h>
#include <ds_kloggerd_msgs/KloggerdStatus.h>
#include <ds_kloggerd_msgs/KloggerdPingInfo.h>

namespace sentry_acomms
{
VehicleKongsbergStatusQueue::VehicleKongsbergStatusQueue() : SimpleSingleQueue<SentryKongsbergStatus2>()
{
}

VehicleKongsbergStatusQueue::~VehicleKongsbergStatusQueue() = default;

void VehicleKongsbergStatusQueue::setup(ros::NodeHandle& nh)
{

  kctrl_pu_info_sub_ = nh.subscribe(
        "kctrl_pu_info", 1, &VehicleKongsbergStatusQueue::handleKctrlPuInfo, this);
  kloggerd_ping_info_sub_ = nh.subscribe(
        "kloggerd_ping_info", 1, &VehicleKongsbergStatusQueue::handleKloggerdPingInfo, this);
  kloggerd_status_sub_ = nh.subscribe(
        "kloggerd_status", 1, &VehicleKongsbergStatusQueue::handleKloggerdStatus, this);

  if (nh.hasParam("em2040_system_id")) {
    nh.getParam("em2040_system_id", em2040_system_id_);
    ROS_INFO_STREAM("kongsberg vehicle queue em2040 system id: " << em2040_system_id_);
  }
  else {
    ROS_ERROR_STREAM("no parameter named '" << nh.resolveName("em2040_system_id", true) << "'.  No KCTRL data will be available in kongsberg acoustic queue");
  }
}

void VehicleKongsbergStatusQueue::handleKctrlPuInfo(const ds_kctrl_msgs::PuInfoMessage& msg)
{
  if (msg.system_id != em2040_system_id_) {
    return;
  }
  auto set_status = [](std::uint8_t& out, const std::uint8_t in) {
    if (in == ds_kctrl_msgs::KctrlStatusLabel::STATUS_ACTIVE_OK) {
      out = sentry_acomms::SentryKongsbergStatus2::STATUS_GOOD_OR_ON;
    }
    else {
      out = sentry_acomms::SentryKongsbergStatus2::STATUS_BAD_OR_OFF;
    }
  };

  set_status(item_.status_position, msg.position[0].status);
  set_status(item_.status_attitude, msg.attitude[0].status);
  set_status(item_.status_sound_velocity, msg.sound_velocity.status);
  set_status(item_.status_time_sync, msg.time_sync.status);
  set_status(item_.status_pps, msg.pps.status);
  set_status(item_.status_depth, msg.depth_sensor.status); 

  auto set_flag = [](std::uint8_t& out, std::uint8_t in, const std::uint8_t flag) {
    if ((in & flag) != 0) {
      out = SentryKongsbergStatus2::STATUS_GOOD_OR_ON;
    }
    else {
      out =  SentryKongsbergStatus2::STATUS_BAD_OR_OFF;
    }
  };
                                                                                                           
  set_flag(item_.status_pinging, msg.status_flag, ds_kctrl_msgs::PuInfoMessage::PU_STATUS_FLAG_PINGING);
  set_flag(item_.status_water_column, msg.status_flag, ds_kctrl_msgs::PuInfoMessage::PU_STATUS_FLAG_WATERCOLUMN);


  is_empty_ = false;
}

void VehicleKongsbergStatusQueue::handleKloggerdStatus(const ds_kloggerd_msgs::KloggerdStatus& msg)
{
  item_.status_recording = msg.recording ? sentry_acomms::SentryKongsbergStatus2::STATUS_GOOD_OR_ON : sentry_acomms::SentryKongsbergStatus2::STATUS_BAD_OR_OFF;
  is_empty_ = false;
}

void VehicleKongsbergStatusQueue::handleKloggerdPingInfo(const ds_kloggerd_msgs::KloggerdPingInfo& msg)
{
  item_.ping_num = msg.ping_number;
  item_.num_soundings = msg.num_soundings_main;
  item_.percent_good = static_cast<std::uint16_t>(100 * static_cast<float>(msg.num_soundings_main_valid) / msg.num_soundings_main);
  item_.max_depth = msg.depth_max_m;
  item_.min_depth = msg.depth_min_m;
  item_.center_depth = msg.depth_center_beam_m;
  item_.approx_ping_rate = msg.ping_rate_hz;
  if (msg.frequency_mode_hz > 100000) {
    item_.ping_frequency = static_cast<decltype(item_.ping_frequency)>(msg.frequency_mode_hz / 1000);
  }
  else {
    item_.ping_frequency = static_cast<decltype(item_.ping_frequency)>(msg.frequency_mode_hz);
  }
  
  // port mean coverage is reported as a negative number
  item_.swath_width = msg.stbd_mean_coverage_m - msg.port_mean_coverage_m;
  
  is_empty_ = false;
}

bool VehicleKongsbergStatusQueue::encodeNext(std::vector<uint8_t>& buf, bool as_string)
{
  if (empty())
  {
    return false;
  }

  ds_acomms_serialization::serialize(item_, buf, as_string);

  // mark empty and reset queue data
  is_empty_ = true;
  item_ = {};

  return true;
}
 
#if 0
void VehicleKongsbergStatusQueue::kmstatusCallback(const ds_kongsberg_msgs::KongsbergStatus& msg)
{
  // PU Status
  item_.pu_good = (
      msg.pu_powered
      && msg.pu_connected
      && msg.kctrl_connected
      && msg.kmall_connected);
  // std::string ok{"OK;G"};
  // item_.sensors_good = (!msg.cpu_temperature.find(ok)
  //     && !msg.position_1.find(ok)
  //     && !msg.attitude_1.find(ok)
  //     && !msg.svp.find(ok)
  //     && !msg.time_sync.find(ok)
  //     && !msg.pps.find(ok));
  // item_.pinging = msg.pinging;
  
  // Settings
//# depth settings
  item_.rt_force_depth = std::stoi(msg.rt_force_depth, nullptr, 10);
  item_.rt_min_depth = std::stoi(msg.rt_min_depth, nullptr, 10);
  item_.rt_max_depth = std::stoi(msg.rt_max_depth, nullptr, 10);
  item_.rt_ping_freq = std::stoi(msg.rt_ping_freq, nullptr, 10);
  item_.rt_fm_disable = std::stoi(msg.rt_fm_disable, nullptr, 10);
  item_.rt_log_watercol = std::stoi(msg.rt_log_watercol, nullptr, 10);
  item_.rt_extra_detect = std::stoi(msg.rt_extra_detect, nullptr, 10);
  item_.rt_depth_mode = msg.rt_depth_mode;
//# Txmit settings
  item_.rt_pitch_stab = static_cast<float>(msg.rt_pitch_stab, nullptr, 10);
  item_.rt_tx_angle_along = static_cast<float>(msg.rt_tx_angle_along, nullptr, 10);
  item_.rt_max_ping_rate = std::stoi(msg.rt_max_ping_rate, nullptr, 10);
  item_.rt_min_swath_distance = std::stoi(msg.rt_min_swath_distance, nullptr, 10);
  item_.rt_trigger = std::stoi(msg.rt_trigger, nullptr, 10);

  // Latest Ping Status
  item_.ping_num = msg.ping_num;
  item_.num_soundings = msg.num_soundings;
  item_.percent_good = msg.percent_good;
  item_.max_depth = msg.max_depth;
  item_.min_depth = msg.min_depth;
}
#endif
} // namespace sentry_acomms
