/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 10/1/19.
//

#include "sentry_acomms/vehicle_mxstatus_queue.h"
#include "sentry_acomms/serialization.h"
#include "ds_mx_msgs/MxMissionStatus.h"

namespace sentry_acomms {

VehicleMxStatusQueue::VehicleMxStatusQueue() : SimpleSingleQueue<SentryMxStatus>()
{
}

VehicleMxStatusQueue::~VehicleMxStatusQueue() = default;

void VehicleMxStatusQueue::setup(ros::NodeHandle& nh)
{
  auto topic_name = std::string{};

  auto str = ros::names::resolve(ros::this_node::getName(), "mx_status_topic", true);
  if (!ros::param::get(str, topic_name))
  {
    ROS_FATAL_STREAM("Parameter: '" << str << "' is not defined");
    ROS_BREAK();
  }

  status_sub_ = nh.subscribe(topic_name, 1, &VehicleMxStatusQueue::mxStatusCallback, this);
}

void VehicleMxStatusQueue::mxStatusCallback(const ds_mx_msgs::MxMissionStatus& msg)
{
  ros::Time next_timeout;
  for (const auto& task : msg.active_tasks) {
    // timeout is uninitialized, so it doesn't apply
    if (task.timeout.sec == 0 && task.timeout.nsec == 0) {
      continue;
    }
    // next timeout is uninitialized, so set it
    if (next_timeout.sec == 0 && next_timeout.nsec == 0) {
      next_timeout = task.timeout;
      continue;
    }
    // both task and next timeouts are initialized.  Actually pick the lowest.
    if (task.timeout < next_timeout) {
      next_timeout = task.timeout;
    }
  }

  item_.next_timeout = next_timeout;
  item_.primitive_uuid = msg.active_tasks.back().uuid;
}


} // namespace sentry_acomms
