/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_acomms/sentry_micromodem.h"
#include "sentry_acomms/serialization.h"
#include "sentry_acomms/legacy_micromodem_serialization.h"

#include "sentry_acomms/SentryAcommQueueEnum.h"
#include "sentry_acomms/SentryVehicleState.h"

#include "ds_acomms_modem/whoi_micromodem_serialization.h"
#include "ds_acomms_msgs/AcousticModemData.h"
#include "ds_acomms_queue_manager/DataQueueMessage.h"
#include "ds_acomms_queue_manager/serialization.h"

namespace sentry_acomms
{
struct SentryMicromodemPrivate
{
};

SentryMicromodem::SentryMicromodem()
  : ds_acomms_modem::WhoiMicromodem(), d_ptr_(std::unique_ptr<SentryMicromodemPrivate>(new SentryMicromodemPrivate))
{
}
SentryMicromodem::SentryMicromodem(int argc, char** argv, const std::string& name)
  : WhoiMicromodem(argc, argv, name), d_ptr_(std::unique_ptr<SentryMicromodemPrivate>(new SentryMicromodemPrivate))
{
}

int SentryMicromodem::send(uint16_t remote_address, std::vector<uint8_t> bytes)
{
  // SDQ 0 messages get the hex-encoding treatment.  Everything else is sent as ascii.
  const auto is_string = ds_acomms_serialization::is_string_encoded(bytes);

  // TODO:  Handle byte encoding.
  auto msg_string = std::string{ std::begin(bytes), std::end(bytes) };
  ROS_INFO_STREAM("Sending: '" << msg_string << "'");
  const auto message_type = ds_acomms_queue_manager::queue_message_type(msg_string);
  if (message_type != ds_acomms_queue_manager::QueueManagerMessageEnum::SEND_DATA_QUEUE)
  {
    ROS_INFO_STREAM("Would encode as JasonTalk ASCII: " << msg_string);
    auto jt = umodem_jasontalk_ascii_t{};
    memset(&jt, 0, sizeof(jt));
    jt.dest = remote_address;
    jt.source = localAddress();
    jt.ack_requested = false;
    std::memcpy(jt.jt_str, msg_string.data(), std::min(msg_string.size(), MAX_UMODEM_JASONTALK_ASCII_STRING_LENGTH));

    auto buf = std::array<char, 1024>{};
    // UMODEM_WriteHROVStateXYMsg(buf.data(), buf.size() - 1, msg.local_addr, msg.remote_addr, 0, &hrov_statexy);
    UMODEM_Encode_HROV_JasonTalk_ASCII_Msg(&jt, buf.data(), buf.size() - 1);
    ROS_INFO("HEX Encoded data is: '%s'", buf.data());

    auto bytes = std::vector<uint8_t>{};
    if (!ds_acomms_modem::micromodem::hexstr_to_bytes(buf.data(), strlen(buf.data()), bytes))
    {
      ROS_ERROR_STREAM("Failed to get raw byte vector from HEX-encoded bytes");
      return 0;
    }
    return WhoiMicromodem::send(remote_address, std::move(bytes));
  }

  auto queue_msg = ds_acomms_queue_manager::DataQueueMessage{};
  if (!ds_acomms_serialization::deserialize(msg_string, queue_msg))
  {
    ROS_ERROR_STREAM("Failed to deserialize SDQ message: '" << msg_string << "'");
    return 0;
  }

  if (queue_msg.queue != SentryAcommQueueEnum::SENTRY_VEHICLE_STATE)
  {
    ROS_INFO_STREAM("Would encode as JasonTalk ASCII: " << msg_string);
    auto jt = umodem_jasontalk_ascii_t{};
    memset(&jt, 0, sizeof(jt));
    jt.dest = remote_address;
    jt.source = localAddress();
    jt.ack_requested = false;
    std::memcpy(jt.jt_str, msg_string.data(), std::min(msg_string.size(), MAX_UMODEM_JASONTALK_ASCII_STRING_LENGTH));

    auto buf = std::array<char, 1024>{};
    // UMODEM_WriteHROVStateXYMsg(buf.data(), buf.size() - 1, msg.local_addr, msg.remote_addr, 0, &hrov_statexy);
    UMODEM_Encode_HROV_JasonTalk_ASCII_Msg(&jt, buf.data(), buf.size() - 1);
    ROS_INFO("HEX Encoded data is: '%s'", buf.data());

    auto bytes = std::vector<uint8_t>{};
    if (!ds_acomms_modem::micromodem::hexstr_to_bytes(buf.data(), strlen(buf.data()), bytes))
    {
      ROS_ERROR_STREAM("Failed to get raw byte vector from HEX-encoded bytes");
      return 0;
    }
    return WhoiMicromodem::send(remote_address, std::move(bytes));
  }

  if (queue_msg.error != ds_acomms_queue_manager::DataQueueMessage::ERROR_NONE)
  {
    ROS_WARN_STREAM("Not sending: " << msg_string);
    return 0;
  }

  auto payload_str = std::string{ std::begin(queue_msg.payload), std::end(queue_msg.payload) };
  ROS_INFO_STREAM("Would encode SDQ " << static_cast<int>(SentryAcommQueueEnum::SENTRY_VEHICLE_STATE) << " data '"
                                      << payload_str << "' as hex");

  auto vehicle_state = SentryVehicleState{};
  if (!ds_acomms_serialization::deserialize(queue_msg.payload, vehicle_state, is_string))
  {
    ROS_ERROR_STREAM("Unable to deserialize SentryVehicleState from data");
    return 0;
  }

  umodem_hrov_statexy_t hrov_statexy;
  sentry_vehicle_state_to_hrov_statexy(vehicle_state, hrov_statexy);
  hrov_statexy.source = localAddress();
  hrov_statexy.dest = remote_address;

  auto buf = std::array<char, 1024>{};
  // UMODEM_WriteHROVStateXYMsg(buf.data(), buf.size() - 1, msg.local_addr, msg.remote_addr, 0, &hrov_statexy);
  UMODEM_Encode_HROVStateXYData(&hrov_statexy, buf.data(), buf.size() - 1);
  ROS_INFO("HEX Encoded data is: '%s'", buf.data());

  auto hex_bytes = std::vector<uint8_t>{};
  if (!ds_acomms_modem::micromodem::hexstr_to_bytes(buf.data(), strlen(buf.data()), hex_bytes))
  {
    ROS_ERROR_STREAM("Failed to get raw byte vector from HEX-encoded bytes");
    return 0;
  }

  return WhoiMicromodem::send(remote_address, std::move(hex_bytes));
}

void SentryMicromodem::handleMicromodemPacket(uint16_t src_address, std::vector<uint8_t> bytes)
{
  auto os = std::ostringstream{};
  os << std::hex << std::uppercase << std::setfill('0');
  for (const auto& b : bytes)
  {
    os << std::setw(2) << static_cast<int>(b);
  }

  const auto hex_str = os.str();

  int message_type = 0;
  if (!sscanf(hex_str.data(), "%02X", &message_type))
  {
    ROS_ERROR_STREAM("Unable to decode micromodem packet type.");
    return;
  }
  switch (message_type)
  {
    case (UMODEMHROVSTATEXY):
    {
      auto hrov = umodem_hrov_statexy_t{};
      if (UMODEM_Decode_HROVStateXYData(&hrov, hex_str.data(), hex_str.size()))
      {
        ROS_ERROR_STREAM("Unable to decode HROVStateXYData from: '" << hex_str << "'");
        return;
      }

      auto msg = SentryVehicleState{};

      // Convert hrov statexy to SentryVehicleState and reserialize
      hrov_statexy_to_sentry_vehicle_state(hrov, msg);
      // TODO:  Enable byte encoding
      ds_acomms_serialization::serialize(msg, bytes, true);
      publishReceivedAcousticModemData(src_address, std::move(bytes));
      break;
    }
    case (UMODEMHROVJASONTALK):
    {
      auto jt = umodem_jasontalk_ascii_t{};
      if (UMODEM_Decode_HROV_JasonTalk_ASCII_Msg(&jt, hex_str.data(), hex_str.size()))
      {
        ROS_ERROR_STREAM("Unable to decode HROVJasonTalkAscii from: '" << hex_str << "'");
        return;
      }

      ROS_INFO("Decoded JasonTalkAscii payload: %s", jt.jt_str);
      const auto len = strlen(jt.jt_str);
      bytes.resize(len);

      auto ptr = &jt.jt_str[0];
      for (auto i = 0; i < len; ++i)
      {
        bytes[i] = static_cast<uint8_t>(*ptr);
        ++ptr;
      }

      publishReceivedAcousticModemData(src_address, std::move(bytes));
      break;
    }
    default:
      ROS_WARN("Ignoring umodem packet type: %s", UMODEM_DATA_PACKET_TYPE_INT_TO_NAME(message_type));
      return;
  }
}

SentryMicromodem::~SentryMicromodem() = default;
}
