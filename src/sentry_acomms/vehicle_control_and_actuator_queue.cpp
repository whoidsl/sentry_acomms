/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_acomms/vehicle_control_and_actuator_queue.h"
#include "sentry_acomms/serialization.h"
#include "sail_con/SentryServoState.h"
#include "sentry_thruster_servo/State.h"
#include "ds_actuator_msgs/ThrusterState.h"
#include "geometry_msgs/WrenchStamped.h"

namespace sentry_acomms
{
VehicleControlAndActuatorQueue::VehicleControlAndActuatorQueue() : SimpleSingleQueue<SentryControlAndActuatorState>()
{
}

VehicleControlAndActuatorQueue::~VehicleControlAndActuatorQueue() = default;

void VehicleControlAndActuatorQueue::setup(ros::NodeHandle& nh)
{
  param_con_ = ds_param::ParamConnection::create(nh);

  auto param_str = std::string{};
  param_str = ros::names::resolve(ros::this_node::getName(), std::string{ "active_controller_param" });

  auto value_str = std::string{};

  if (!ros::param::get(param_str, value_str))
  {
    ROS_FATAL_STREAM("Parameter: '" << param_str << "' is not defined");
    ROS_BREAK();
  }

  active_controller_ = param_con_->connect<ds_param::IntParam>(value_str);

  param_str = ros::names::resolve(ros::this_node::getName(), std::string{ "active_allocation_param" });

  if (!ros::param::get(param_str, value_str))
  {
    ROS_FATAL_STREAM("Parameter: '" << param_str << "' is not defined");
    ROS_BREAK();
  }

  active_allocation_ = param_con_->connect<ds_param::IntParam>(value_str);

  param_con_->setCallback(boost::bind(&VehicleControlAndActuatorQueue::dsparamCallback, this, _1));

  // Setup subscriptions based on which servo is installed.
  param_str = ros::names::resolve(ros::this_node::getName(), std::string{ "fwd_servo_type" });
  std::string servo_type;
  if (!ros::param::get(param_str, servo_type))
  {
    ROS_FATAL_STREAM("Parameter: '" << param_str << "' is not defined");
    ROS_BREAK();
  }
 
  // Setup subscriptions based on which servo is installed.
  param_str = ros::names::resolve(ros::this_node::getName(), std::string{ "fwd_servo_state_topic" });
  if (!ros::param::get(param_str, value_str))
  {
    ROS_FATAL_STREAM("Parameter: '" << param_str << "' is not defined");
    ROS_BREAK();
  }

  if (servo_type == "sail") {
    fwd_servo_sub_ = nh.subscribe<sail_con::SentryServoState>(
        value_str, 1, boost::bind(&VehicleControlAndActuatorQueue::sailServoCallback, this, FWD_SERVO, _1));
  }
  else if (servo_type == "sail-serial") {
    fwd_servo_sub_ = nh.subscribe<sentry_thruster_servo::State>(
        value_str, 1, boost::bind(&VehicleControlAndActuatorQueue::sailSerialServoCallback, this, FWD_SERVO, _1));
  }
  else {
    ROS_FATAL_STREAM("Unknown forward servo type: '" << servo_type << "' is not 'sail' or 'sail-servo'");
    ROS_BREAK();
  }


  param_str = ros::names::resolve(ros::this_node::getName(), std::string{ "aft_servo_type" });
  if (!ros::param::get(param_str, servo_type))
  {
    ROS_FATAL_STREAM("Parameter: '" << param_str << "' is not defined");
    ROS_BREAK();
  }
  param_str = ros::names::resolve(ros::this_node::getName(), std::string{ "aft_servo_state_topic" });
  if (!ros::param::get(param_str, value_str))
  {
    ROS_FATAL_STREAM("Parameter: '" << param_str << "' is not defined");
    ROS_BREAK();
  }
  if (servo_type == "sail") {
    aft_servo_sub_ = nh.subscribe<sail_con::SentryServoState>(
        value_str, 1, boost::bind(&VehicleControlAndActuatorQueue::sailServoCallback, this, AFT_SERVO, _1));
  }
  else if (servo_type == "sail-serial") {
    aft_servo_sub_ = nh.subscribe<sentry_thruster_servo::State>(
        value_str, 1, boost::bind(&VehicleControlAndActuatorQueue::sailSerialServoCallback, this, AFT_SERVO, _1));
  }
  else {
    ROS_FATAL_STREAM("Unknown aft servo type: '" << servo_type << "' is not 'sail' or 'sail-servo'");
    ROS_BREAK();
  }

  param_str = ros::names::resolve(ros::this_node::getName(), std::string{ "fwd_port_thruster_state_topic" });
  if (!ros::param::get(param_str, value_str))
  {
    ROS_FATAL_STREAM("Parameter: '" << param_str << "' is not defined");
    ROS_BREAK();
  }
  fwd_port_thruster_sub_ = nh.subscribe<ds_actuator_msgs::ThrusterState>(
      value_str, 1, boost::bind(&VehicleControlAndActuatorQueue::thrusterCallback, this, FWD_PORT_THRUSTER, _1));

  param_str = ros::names::resolve(ros::this_node::getName(), std::string{ "fwd_stbd_thruster_state_topic" });
  if (!ros::param::get(param_str, value_str))
  {
    ROS_FATAL_STREAM("Parameter: '" << param_str << "' is not defined");
    ROS_BREAK();
  }
  fwd_stbd_thruster_sub_ = nh.subscribe<ds_actuator_msgs::ThrusterState>(
      value_str, 1, boost::bind(&VehicleControlAndActuatorQueue::thrusterCallback, this, FWD_STBD_THRUSTER, _1));

  param_str = ros::names::resolve(ros::this_node::getName(), std::string{ "aft_port_thruster_state_topic" });
  if (!ros::param::get(param_str, value_str))
  {
    ROS_FATAL_STREAM("Parameter: '" << param_str << "' is not defined");
    ROS_BREAK();
  }
  aft_port_thruster_sub_ = nh.subscribe<ds_actuator_msgs::ThrusterState>(
      value_str, 1, boost::bind(&VehicleControlAndActuatorQueue::thrusterCallback, this, AFT_PORT_THRUSTER, _1));
  param_str = ros::names::resolve(ros::this_node::getName(), std::string{ "aft_stbd_thruster_state_topic" });
  if (!ros::param::get(param_str, value_str))
  {
    ROS_FATAL_STREAM("Parameter: '" << param_str << "' is not defined");
    ROS_BREAK();
  }
  aft_stbd_thruster_sub_ = nh.subscribe<ds_actuator_msgs::ThrusterState>(
      value_str, 1, boost::bind(&VehicleControlAndActuatorQueue::thrusterCallback, this, AFT_STBD_THRUSTER, _1));

  param_str = ros::names::resolve(ros::this_node::getName(), std::string{ "control_wrench_topic" });
  if (!ros::param::get(param_str, value_str))
  {
    ROS_FATAL_STREAM("Parameter: '" << param_str << "' is not defined");
    ROS_BREAK();
  }
  wrench_sub_ = nh.subscribe(value_str, 1, &VehicleControlAndActuatorQueue::wrenchCallback, this);
}

void VehicleControlAndActuatorQueue::wrenchCallback(const geometry_msgs::WrenchStamped& wrench)
{
  item_.force_u = static_cast<float>(wrench.wrench.force.x);
  item_.force_w = static_cast<float>(wrench.wrench.force.z);
  item_.torque_q = static_cast<float>(wrench.wrench.torque.z);
}

void VehicleControlAndActuatorQueue::dsparamCallback(const ds_param::ParamConnection::ParamCollection& params)
{
  for (auto it = std::begin(params); it != std::end(params); ++it)
  {
    if (*it == active_controller_)
    {
      item_.active_controller =
          static_cast<SentryControlAndActuatorState::_active_controller_type>(active_controller_->Get());
      continue;
    }

    if (*it == active_allocation_)
    {
      item_.active_allocation =
          static_cast<SentryControlAndActuatorState::_active_allocation_type>(active_allocation_->Get());
      continue;
    }
  }
}

void VehicleControlAndActuatorQueue::sailServoCallback(SERVO_INDEX index, const sail_con::SentryServoStateConstPtr& msg)
{
  switch (index)
  {
    case FWD_SERVO:
      item_.fwd_servo_deg = static_cast<float>(msg->servo_state.actual_radians * 180.0 / M_PI);
      item_.fwd_servo_switch = msg->mem_switch;
      item_.fwd_servo_speed = msg->mem_speed;
      break;
    case AFT_SERVO:
      item_.aft_servo_deg = static_cast<float>(msg->servo_state.actual_radians * 180.0 / M_PI);
      item_.aft_servo_switch = msg->mem_switch;
      item_.aft_servo_speed = msg->mem_speed;
      break;
    default:
      break;
  }
}

void VehicleControlAndActuatorQueue::sailSerialServoCallback(SERVO_INDEX index, const sentry_thruster_servo::StateConstPtr& msg)
{
  switch (index)
  {
    case FWD_SERVO:
      item_.fwd_servo_deg = static_cast<float>(msg->angle_radians * 180.0 / M_PI);
      item_.fwd_servo_switch = msg->switch_state;
      item_.fwd_servo_speed = msg->speed_raw;
      break;
    case AFT_SERVO:
      item_.aft_servo_deg = static_cast<float>(msg->angle_radians * 180.0 / M_PI);
      item_.aft_servo_switch = msg->switch_state;
      item_.aft_servo_speed = msg->speed_raw;
      break;
    default:
      break;
  }
  
}

void VehicleControlAndActuatorQueue::thrusterCallback(THRUSTER_INDEX index,
                                                      const ds_actuator_msgs::ThrusterStateConstPtr& msg)
{
  switch (index)
  {
    case FWD_PORT_THRUSTER:
      item_.fwd_port_thruster_desired_current = static_cast<float>(msg->cmd_value);
      item_.fwd_port_thruster_measured_current = static_cast<float>(msg->actual_value);
      break;
    case FWD_STBD_THRUSTER:
      item_.fwd_stbd_thruster_desired_current = static_cast<float>(msg->cmd_value);
      item_.fwd_stbd_thruster_measured_current = static_cast<float>(msg->actual_value);
      break;
    case AFT_PORT_THRUSTER:
      item_.aft_port_thruster_desired_current = static_cast<float>(msg->cmd_value);
      item_.aft_port_thruster_measured_current = static_cast<float>(msg->actual_value);
      break;
    case AFT_STBD_THRUSTER:
      item_.aft_stbd_thruster_desired_current = static_cast<float>(msg->cmd_value);
      item_.aft_stbd_thruster_measured_current = static_cast<float>(msg->actual_value);
      break;
    default:
      break;
  }
}
}
