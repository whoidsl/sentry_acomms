/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_acomms/vehicle_htp_queue.h"
#include "sentry_acomms/serialization.h"
#include "ds_hotel_msgs/HTP.h"

#include <ros/ros.h>

namespace sentry_acomms
{
VehicleHtpQueue::VehicleHtpQueue() : SimpleSingleQueue<SentryHtp>()
{
  // auto byte_encoder = [](boost::any& a, std::vector<uint8_t>& b) {
  //   return ds_acomms::to_bytes<sentry_acomms::SentryHtp>(a, b);
  // };
  // setByteEncoder(std::move(byte_encoder));

  // auto string_encoder = [](boost::any& a, std::string& s){
  //   return sentry_vehicle_state::to_string(a, s);
  // };

  // auto string_encoder = [](boost::any& a, std::string& s) {
  //   single_type_queue_to_string<sentry_acomms::SentryHtp>(a, s);
  // };

  // setStringEncoder(std::move(string_encoder));

  // auto string_encoder = [](const SentryHtp& msg, std::string& buf)
  // {
  //   ds_acomms::serialize(msg, buf);
  // };
  // setStringEncoder(string_encoder);
}

VehicleHtpQueue::~VehicleHtpQueue() = default;

void VehicleHtpQueue::setup(ros::NodeHandle& nh)
{
  auto node_name = std::string{};
  auto str = std::string{};

  str = ros::names::resolve(ros::this_node::getName(), std::string{ "battery_htp_node" });

  // Battery HTP
  if (!ros::param::get(str, node_name))
  {
    ROS_FATAL_STREAM("Parameter: '" << str << "' is not defined");
    ROS_BREAK();
  }

  str = ros::names::resolve(node_name, std::string{ "htp" });
  battery_sub_ = nh.subscribe(str, 1, &VehicleHtpQueue::batteryHousingCallback, this);

  // Main HTP
  str = ros::names::resolve(ros::this_node::getName(), std::string{ "main_htp_node" });
  if (!ros::param::get(str, node_name))
  {
    ROS_FATAL_STREAM("Parameter: '" << str << "' is not defined");
    ROS_BREAK();
  }

  str = ros::names::resolve(node_name, std::string{ "htp" });
  main_sub_ = nh.subscribe(str, 1, &VehicleHtpQueue::mainHousingCallback, this);
}

void VehicleHtpQueue::mainHousingCallback(const ds_hotel_msgs::HTP& msg)
{
  item_.main_humidity = static_cast<float>(msg.humidity);
  item_.main_temperature = static_cast<float>(msg.temperature);
  item_.main_pressure = static_cast<float>(msg.pressure);
}

void VehicleHtpQueue::batteryHousingCallback(const ds_hotel_msgs::HTP& msg)
{
  item_.battery_humidity = static_cast<float>(msg.humidity);
  item_.battery_temperature = static_cast<float>(msg.temperature);
  item_.battery_pressure = static_cast<float>(msg.pressure);
}
}
