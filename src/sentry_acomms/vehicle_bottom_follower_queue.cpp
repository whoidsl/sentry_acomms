/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_acomms/vehicle_bottom_follower_queue.h"
#include "sentry_acomms/serialization.h"
#include "ds_control_msgs/BottomFollow1D.h"

namespace sentry_acomms
{
VehicleBottomFollowerQueue::VehicleBottomFollowerQueue() : SimpleSingleQueue<SentryBottomFollowerParameters>()
{
}

VehicleBottomFollowerQueue::~VehicleBottomFollowerQueue() = default;

void VehicleBottomFollowerQueue::setup(ros::NodeHandle& nh)
{
  const auto goals_ns = nh.param<std::string>("goals_ns", "");
  const auto nav_ns = nh.param<std::string>("nav_ns", "");
  const auto vehicle_ns = nh.param<std::string>("vehicle_ns", "");

  auto node_name = std::string{};
  auto str = std::string{};

  str = ros::names::resolve(ros::this_node::getName(), std::string{ "bottom_follower_node" });
  // Bottom follower
  if (!ros::param::get(str, node_name))
  {
    ROS_FATAL_STREAM("Parameter: '" << str << "' is not defined");
    ROS_BREAK();
  }

  if (!goals_ns.empty())
  {
    node_name = ros::names::resolve(goals_ns, node_name);
  }

  str = ros::names::resolve(node_name, std::string{ "bf_topic" });
  bf_sub_ = nh.subscribe(str, 1, &VehicleBottomFollowerQueue::bottomFollowerCallback, this);
}

void VehicleBottomFollowerQueue::bottomFollowerCallback(const ds_control_msgs::BottomFollow1D& msg)
{
  item_.altitude = static_cast<float>(msg.commanded_altitude);
  item_.depth_floor = static_cast<float>(msg.depth_floor);
  item_.alarm_timeout = static_cast<float>(msg.alarm_timeout);
  item_.min_speed = static_cast<float>(msg.min_speed);
  item_.depth_acceleration = static_cast<float>(msg.depth_accel_d);
  item_.depth_rate = static_cast<float>(msg.depth_rate_d);
  item_.speed = static_cast<float>(msg.ref_speed);
  item_.envelope = static_cast<float>(msg.depth_env.at(0));
  item_.speed_gain = static_cast<float>(msg.speed_gain);
  item_.user_override_active = msg.userDepthOverrideEngaged;
  item_.user_override_direction = msg.override_depth_direction;
}
}
