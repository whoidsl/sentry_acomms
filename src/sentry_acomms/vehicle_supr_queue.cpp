/**
* Copyright 2023 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

//
// Created by tmjoyce
//

#include "sentry_acomms/vehicle_supr_queue.h"

namespace sentry_acomms
{
VehicleSuprStatusQueue::VehicleSuprStatusQueue() : SimpleSingleQueue<ds_core_msgs::DsStringStamped>()
{
}

VehicleSuprStatusQueue::~VehicleSuprStatusQueue() = default;

void VehicleSuprStatusQueue::setup(ros::NodeHandle& nh)
{
  auto topic_name = std::string{};

  //Will add this topic mapping to sentry_config launches
  topic_name = ros::names::resolve(ros::this_node::getName(),
                                   std::string{"suprstatus_topic"});
  auto supr_topic = std::string{};
  if (!ros::param::get(topic_name, supr_topic))
  {
    ROS_FATAL_STREAM("Parameter: '" << topic_name << "' is not defined");
    ROS_BREAK();
  }
  supr_sub_ = nh.subscribe(supr_topic, 1,
                             &VehicleSuprStatusQueue::suprStatusCallback, this);
}

void VehicleSuprStatusQueue::suprStatusCallback(const ds_core_msgs::DsStringStamped& msg)
{
  item_.payload = msg.payload;
}

} // namespace sentry_acomms
