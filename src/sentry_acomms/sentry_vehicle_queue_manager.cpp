/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#define BYTE_ENCODING_ENABLED 0

#include "sentry_acomms/sentry_vehicle_queue_manager.h"
#include "sentry_acomms/vehicle_state_queue.h"
#include "sentry_acomms/vehicle_bottom_follower_queue.h"
#include "sentry_acomms/vehicle_depth_follower_queue.h"
#include "sentry_acomms/vehicle_leg_goal_queue.h"
#include "sentry_acomms/vehicle_scalar_science_queue.h"
#include "sentry_acomms/vehicle_htp_queue.h"
#include "sentry_acomms/vehicle_kongsberg_queue.h"
#include "sentry_acomms/vehicle_prosilica_camera_queue.h"
#include "sentry_acomms/vehicle_pwr_queue.h"
#include "sentry_acomms/vehicle_shift_queue.h"
#include "sentry_acomms/vehicle_syprid_queue.h"
#include "sentry_acomms/vehicle_blueview_queue.h"
#include "sentry_acomms/vehicle_control_and_actuator_queue.h"
#include "sentry_acomms/vehicle_reson_queue.h"
#include "sentry_acomms/vehicle_mxstatus_queue.h"
#include "sentry_acomms/vehicle_ins_queue.h"
#include "sentry_acomms/vehicle_ins_nav_queue.h"
#include "sentry_acomms/vehicle_supr_queue.h"
#include "sentry_acomms/vehicle_dvl_queue.h"
#include "sentry_acomms/serialization.h"
#include "sentry_acomms/SentryQueueManagerCommandEnum.h"
#include "sentry_acomms/MissionControllerCommand.h"
#include "sentry_acomms/BlueviewCommand.h"
#include "sentry_acomms/PhinsCommand.h"
#include "sentry_acomms/NavaggCommand.h"
#include "sentry_acomms/BottomFollowerParameterCommand.h"
#include "sentry_acomms/DepthFollowerParameterCommand.h"
#include "sentry_acomms/SetActiveDepthGoalCommand.h"
#include "sentry_acomms/ResonCommand.h"
#include "sentry_acomms/ThrustersEnabledCommand.h"
#include "sentry_acomms/AcousticScheduleCommand.h"
#include "sentry_acomms/UdpQueueCommand.h"
#include "sentry_acomms/SentryMxEvent.h"
#include "sentry_acomms/SentryMxSharedParam.h"
#include "sentry_acomms/SentryMxStatus.h"
#include "sentry_acomms/SentryNavShift.h"
#include "sentry_acomms/AcousticJoystickCommand.h"
#include "sentry_acomms/KongsbergParamCommand.h"
#include "sentry_acomms/KongsbergPingCommand.h"
#include "sentry_acomms/KongsbergKctrlCommand.h"
#include "sentry_acomms/SypridCommand.h"
#include "sentry_acomms/SuprCommand.h"
#include "sentry_acomms/AcousticJoystickCommand.h"

#include <ds_mx_msgs/MxSharedParams.h>
#include <ds_mx_msgs/MxEvent.h>
#include <ds_mx_msgs/MxMissionStatus.h>
#include <ds_control_msgs/ExternalBottomFollowTimedOverride.h>
#include <ds_nav_msgs/Shift.h>
#include <sentry_msgs/SypridCmd.h>
#include <sentry_msgs/SentryControllerEnum.h>
#include <sentry_msgs/TimedJoystickService.h>
#include <ds_acomms_queue_manager/simple_ds_connection_queue.h>
#include <ds_mx_msgs/MxUpdateSharedParams.h>
#include <ds_supr/SuprCmd.h>
#include <sentry_msgs/TimedJoystick.h>
#include <ds_acomms_msgs/AcousticModemData.h>
#include <ds_sensor_msgs/DvlStatus.h>
#include <ds_param/ds_param_guard.h>
#include <sensor_msgs/NavSatFix.h>

#include <std_msgs/String.h>
#include <std_msgs/Empty.h>

namespace sentry_acomms
{
struct SentryVehicleQueueManagerPrivate
{
  void resetMicromodemSchedule()
  {
    next_micromodem_schedule_delay_ = 0;
    const auto now = ros::Time::now().toSec();
    const auto next_period = static_cast<unsigned int>((now / micromodem_schedule_period_) + 1);
    const auto wait = ros::Duration{ next_period * micromodem_schedule_period_ - now + micromodem_schedule_delays_.at(0) };
    schedule_micromodem_timer_.setPeriod(wait, true);
    schedule_micromodem_timer_.start();
  }

  void resetAvtrakSchedule()
  {
    next_avtrak_schedule_delay_ = 0;
    const auto now = ros::Time::now().toSec();
    const auto wait = ros::Duration{ static_cast<unsigned int>((now / avtrak_schedule_period_) + 1) *
        avtrak_schedule_period_ -
        now + avtrak_schedule_delays_.at(0) };
    schedule_avtrak_timer_.setPeriod(wait, true);
    schedule_avtrak_timer_.start();
  }

  ros::Publisher bf_override_pub_;       //!< bottom follower override topic
  ros::Publisher mca_shim_jt_pub_;       //!< sentry_mc shim jasontalk topic publisher
  ros::Publisher blueview_shim_jt_pub_;  //!< blueview shim jasontalk topic publisher
  ros::Publisher phinsins_pub_;  //!< Command for the phinsins repeater
  ros::Publisher reson_shim_jt_pub_;     //!< reson shim jasontalk topic publisher
  ros::Publisher nav_shift_pub_; //!< Navigation Shift Publisher
  ros::Publisher mx_event_pub_; //!< MX Event Publisher
  ros::Publisher usbl_data_pub_; //!< USBL data sent down to the vehicle and published
  ros::Timer schedule_micromodem_timer_; //!< timer for sheduled transmissions
  ros::Timer schedule_avtrak_timer_;     //!< timer for sheduled transmissions

  ros::ServiceClient acoustic_joystick_srv_;  ///!< Acoustic Joystick service client.
  ros::ServiceClient mx_shared_param_srv_; ///!< MX Event SharedParameter Update Publisher
  ros::ServiceClient syprid_srv_;
  ros::ServiceClient supr_srv_; // supr cmd service

  unsigned int micromodem_schedule_period_ = 60;
  unsigned int avtrak_schedule_period_ = 60;
  // Seconds from start of the period (or previous message) to send next scheduled message.
  // NOTE: These are incremental delays, not absolute time from start of period,
  //       so { 0, 15, 30 } would trigger at :00, :15, :45.
  // std::vector<double> micromodem_schedule_delays_ = { 0, 15 };
  std::vector<double> micromodem_schedule_delays_ = { 0 };
  std::vector<double> avtrak_schedule_delays_ = { 15 };
  // Indices into the delay vector
  size_t next_micromodem_schedule_delay_ = 0;
  size_t next_avtrak_schedule_delay_ = 0;
  bool schedule_avtrak_enabled_      = false;
  bool schedule_micromodem_enabled_  = true;
  boost::shared_ptr<ds_asio::DsConnection> camera_;
  std::list<boost::shared_ptr<ds_asio::DsConnection>> udp_queue_;
};

SentryVehicleQueueManager::SentryVehicleQueueManager()
  : ds_acomms_queue_manager::QueueManager()
  , d_ptr_(std::unique_ptr<SentryVehicleQueueManagerPrivate>(new SentryVehicleQueueManagerPrivate))
{
}
SentryVehicleQueueManager::SentryVehicleQueueManager(int argc, char** argv, std::string& name)
  : ds_acomms_queue_manager::QueueManager(argc, argv, name)
  , d_ptr_(std::unique_ptr<SentryVehicleQueueManagerPrivate>(new SentryVehicleQueueManagerPrivate))
{
}

void SentryVehicleQueueManager::setupParameters()
{
  QueueManager::setupParameters();

  // navagg node name
  auto param_name = ros::names::resolve(ros::this_node::getName(), std::string{ "navagg_node_name" });
  auto param_value = std::string{};
  if (!ros::param::get(param_name, param_value))
  {
    ROS_FATAL_STREAM("No parameter named '" << param_name << "'");
    ROS_BREAK();
  }

  auto nh = nodeHandle();
  conn_ = ds_param::ParamConnection::create(nh);

  std::string navagg_nodename = param_value;
  enum_heading_ = conn_->connect<ds_param::EnumParam>(navagg_nodename + "/enum_heading");
  enum_pitchroll_ = conn_->connect<ds_param::EnumParam>(navagg_nodename + "/enum_pitchroll");;
  enum_xy_ = conn_->connect<ds_param::EnumParam>(navagg_nodename + "/enum_xy");;
  enum_depth_ = conn_->connect<ds_param::EnumParam>(navagg_nodename + "/enum_depth");;
  enum_sync_ = conn_->connect<ds_param::EnumParam>(navagg_nodename + "/enum_sync");;
}

void SentryVehicleQueueManager::setupQueues()
{
  addQueue(QueueType::QUEUE_VEHICLE_STATE, std::move(std::unique_ptr<VehicleStateQueue>(new VehicleStateQueue)));
  addQueue(QueueType::QUEUE_HTP, std::move(std::unique_ptr<VehicleHtpQueue>(new VehicleHtpQueue)));
  addQueue(QueueType::QUEUE_SCALAR_SCIENCE, std::move(std::unique_ptr<VehicleScalarScienceQueue>(new VehicleScalarScienceQueue)));
  addQueue(QueueType::QUEUE_KONGSBERG, std::move(std::unique_ptr<VehicleKongsbergStatusQueue>(new VehicleKongsbergStatusQueue)));
  addQueue(QueueType::QUEUE_SYPRID, std::move(std::unique_ptr<VehicleSypridStatusQueue>(new VehicleSypridStatusQueue)));
  addQueue(QueueType::QUEUE_PWR, std::move(std::unique_ptr<VehiclePwrQueue>(new VehiclePwrQueue)));
  addQueue(QueueType::QUEUE_MC_SHIFT, std::move(std::unique_ptr<VehicleShiftQueue>(new VehicleShiftQueue)));
  addQueue(QueueType::QUEUE_BOTTOM_FOLLOWER,
           std::move(std::unique_ptr<VehicleBottomFollowerQueue>(new VehicleBottomFollowerQueue)));
  addQueue(QueueType::QUEUE_DEPTH_FOLLOWER,
           std::move(std::unique_ptr<VehicleDepthFollowerQueue>(new VehicleDepthFollowerQueue)));
  addQueue(QueueType::QUEUE_GOAL_LEG, std::move(std::unique_ptr<VehicleLegGoalQueue>(new VehicleLegGoalQueue)));
  addQueue(QueueType::QUEUE_BLUEVIEW, std::move(std::unique_ptr<VehicleBlueviewQueue>(new VehicleBlueviewQueue)));
  addQueue(QueueType::QUEUE_CONTROL_AND_ACTUATOR,
           std::move(std::unique_ptr<VehicleControlAndActuatorQueue>(new VehicleControlAndActuatorQueue)));
  addQueue(QueueType::QUEUE_RESON, std::move(std::unique_ptr<VehicleResonQueue>(new VehicleResonQueue)));
  addQueue(QueueType::QUEUE_MXSTATUS, std::move(std::unique_ptr<VehicleMxStatusQueue>(new VehicleMxStatusQueue)));
  addQueue(QueueType::QUEUE_INS, std::move(std::unique_ptr<VehicleInsQueue>(new VehicleInsQueue)));
  addQueue(QueueType::QUEUE_INS_NAV, std::move(std::unique_ptr<VehicleInsNavQueue>(new VehicleInsNavQueue)));
  addQueue(QueueType::QUEUE_SUPR, std::move(std::unique_ptr<VehicleSuprStatusQueue>(new VehicleSuprStatusQueue)));
  addQueue(QueueType::QUEUE_VEHICLE_DVL, std::move(std::unique_ptr<VehicleDvlStatusQueue>(new VehicleDvlStatusQueue)));

  // These queues interact with non-ros sources that provide data via udp/serial
  DS_D(SentryVehicleQueueManager);

  auto str = std::string{ "camera" };
  if (!ros::param::has(ros::names::resolve(ros::this_node::getName(), str)))
  {
    ROS_FATAL_STREAM("Parameter: '" << str << "' is not defined");
    ROS_BREAK();
  }

  auto camera_queue = std::unique_ptr<VehicleProsilicaCameraQueue>(new VehicleProsilicaCameraQueue);
  d->camera_ = addConnection(str, boost::bind(&VehicleProsilicaCameraQueue::cameraCallback, camera_queue.get(), _1));
  addQueue(QueueType::QUEUE_PROSILICA_CAMERA, std::move(camera_queue));

  auto nh = nodeHandle();
  for (int i = UDP_QUEUE_START; i < UDP_QUEUE_END; ++i)
  {
    auto os = std::ostringstream{};
    os << "udp_queue_" << i - UDP_QUEUE_START;
    const auto name = os.str();
    if (!ros::param::has(ros::names::resolve(ros::this_node::getName(), name)))
    {
      continue;
    }

    auto udp_queue = std::unique_ptr<ds_acomms_queue_manager::SimpleDsConnectionQueue<std_msgs::String>>(
        new ds_acomms_queue_manager::SimpleDsConnectionQueue<std_msgs::String>(*this, name));
    addQueue(static_cast<QueueType>(i), std::move(udp_queue));
  }

  // Finally, run setup on all the queues
  QueueManager::setupQueues();
}

bool SentryVehicleQueueManager::handleQueueRequestAdditionalPayload(std::vector<uint8_t>& buf, bool as_string)
{
  DS_D(SentryVehicleQueueManager);

  auto payload_str = std::string(std::begin(buf), std::end(buf));

  double nav_time, lat, lon, depth, x_stddev, y_stddev, z_stddev;
  const auto num_parsed =
      sscanf(payload_str.data(), "PHBL %lf %lf %lf %lf %lf %lf %lf", &nav_time,
             &lat, &lon, &depth, &x_stddev, &y_stddev, &z_stddev);

  if (num_parsed == 7)
  {
    sensor_msgs::NavSatFix fix;

    fix.latitude = lat;
	  fix.longitude = lon;
	  fix.altitude = -depth;
	  // In practice for usbl fixes err_ellipse_semimajor = err_ellipse_semiminor, this simplifies the covariance matrix calculation
	  fix.position_covariance[0] = x_stddev * x_stddev;
	  fix.position_covariance[4] = y_stddev * y_stddev;
	  fix.position_covariance[8] = z_stddev * z_stddev;
    fix.position_covariance_type = fix.COVARIANCE_TYPE_DIAGONAL_KNOWN;
	  fix.status.status = fix.status.STATUS_FIX;
	  fix.status.service = fix.status.SERVICE_GPS;
	  ros::Time timestamp(nav_time); // Construct ros::Time from unixtime
    fix.header.stamp = timestamp;
    fix.header.frame_id = "phins_frame"; // TODO - make this a param server parameter

    d->usbl_data_pub_.publish(fix);

    return true;
  }
  else {
	  ROS_ERROR_STREAM("Num_parsed: "<<num_parsed<<" doesn't match expected val(7)!");
	  return false;
  }
}

bool SentryVehicleQueueManager::handleCustomQueueMessage(const ds_acomms_msgs::AcousticModemData& msg, bool as_string)
{
  // TODO: Enable byte encoding
  as_string = true;

  if (msg.payload.empty())
  {
    ROS_WARN_STREAM("Ignoring empty message from remote addr: " << msg.remote_addr
                                                                << " on modem with addr: " << msg.local_addr);
    return true;
  }

  auto type_id = 0u;
  if (as_string)
  {
    const auto str_buf = std::string{ std::begin(msg.payload), std::end(msg.payload) };
    type_id = sentry_acomms::queue_message_type(str_buf);
    ROS_DEBUG_STREAM("rx acoustic message: '" << str_buf << "' with type id: " << type_id);
  }
  else
  {
// TODO: Enable byte encoding
#if BYTE_ENCODING_ENABLED
    type_id = message_type(msg.payload)
#endif
        ROS_ERROR_STREAM("Ignoring binary encoded message from remote addr: "
                         << msg.remote_addr << " on modem with addr: " << msg.local_addr);
    return false;
  }

  DS_D(SentryVehicleQueueManager);

  // Mission Controller Command (MCA)
  if (type_id == sentry_acomms::SentryQueueManagerCommandEnum::MISSION_CONTROLLER_COMMAND)
  {
    auto mca_msg = sentry_acomms::MissionControllerCommand{};
    const auto ok = ds_acomms_serialization::deserialize(msg.payload, mca_msg, as_string);
    if (ok)
    {
      auto jt_msg = std_msgs::String{};
      ds_acomms_serialization::serialize(mca_msg, jt_msg.data);
      ROS_INFO_STREAM("Passing through command to MC shim: '" << jt_msg.data << "'");
      d->mca_shim_jt_pub_.publish(jt_msg);
    }
    return ok;
  }

  // Blueview command (BLV)
  if (type_id == sentry_acomms::SentryQueueManagerCommandEnum::BLUEVIEW_COMMAND)
  {
    auto blueview_msg = sentry_acomms::BlueviewCommand{};
    const auto ok = ds_acomms_serialization::deserialize(msg.payload, blueview_msg, as_string);
    if (ok)
    {
      auto jt_msg = std_msgs::String{};
      ds_acomms_serialization::serialize(blueview_msg, jt_msg.data);
      ROS_INFO_STREAM("Passing through command to BLUEVIEW shim: '" << jt_msg.data << "'");
      d->blueview_shim_jt_pub_.publish(jt_msg);
    }
    return ok;
  }

  // Phins command (PHI)
  if (type_id == sentry_acomms::SentryQueueManagerCommandEnum::PHINS_COMMAND)
  {
    auto phins_msg = sentry_acomms::PhinsCommand{};
    const auto ok = ds_acomms_serialization::deserialize(msg.payload, phins_msg, as_string);
    if (ok)
    {
      auto jt_msg = std_msgs::String{};
      ds_acomms_serialization::serialize(phins_msg, jt_msg.data);
      ROS_INFO_STREAM("Passing through command to PHINS driver: '" << jt_msg.data << "'");
      d->phinsins_pub_.publish(jt_msg);
    }
    return ok;
  }

  // Navaggregator command (NAG) to change switchyards
  if (type_id == sentry_acomms::SentryQueueManagerCommandEnum::NAVAGG_COMMAND)
  {
    auto navagg_msg = sentry_acomms::NavaggCommand{};
    const auto ok = ds_acomms_serialization::deserialize(msg.payload, navagg_msg, as_string);
    if (ok)
    {

      // TODO - Check if we are requesting an index unsupported
      auto values_heading = enum_heading_->getNamedValues();
      auto values_pitchroll = enum_pitchroll_->getNamedValues();
      auto values_xy = enum_xy_->getNamedValues();
      auto values_depth = enum_depth_->getNamedValues();
      auto values_sync = enum_sync_->getNamedValues();
      
      ds_param::ParamGuard lock(conn_);
      enum_heading_->Set(navagg_msg.enum_heading);
      enum_pitchroll_->Set(navagg_msg.enum_pitchroll);
      enum_xy_->Set(navagg_msg.enum_xy);
      enum_depth_->Set(navagg_msg.enum_depth);
      enum_sync_->Set(navagg_msg.enum_sync);
      
    }
    return ok;
  }

  // Acoustic Schedule command (ASCHED)
  if (type_id == sentry_acomms::SentryQueueManagerCommandEnum::ACOUSTIC_SCHEDULE_COMMAND) {
    auto schedule_msg = sentry_acomms::AcousticScheduleCommand{};
    const auto ok = ds_acomms_serialization::deserialize(msg.payload, schedule_msg, as_string);

    if (!ok) {
      ROS_WARN_STREAM("Unable to parse ASCHED command.");
      return ok;
    }

    DS_D(SentryVehicleQueueManager);
    if (schedule_msg.modem == schedule_msg.MODEM_MICROMODEM) {

      if (schedule_msg.command == schedule_msg.COMMAND_ENABLE) {
        ROS_WARN_STREAM("Enabling MicroModem TDMA schedule");
        d->schedule_micromodem_enabled_ = true;
        if (!d->schedule_micromodem_timer_.hasPending()) {
          d->resetMicromodemSchedule();
        }
      }

      else if (schedule_msg.command == schedule_msg.COMMAND_DISABLE) {
        ROS_WARN_STREAM("Disabling MicroModem TDMA schedule");
        d->schedule_micromodem_enabled_ = false;
        d->schedule_micromodem_timer_.stop();
      }

      return ok;
    }

    else if (schedule_msg.modem == schedule_msg.MODEM_AVTRAK)
    {
      if (schedule_msg.command == schedule_msg.COMMAND_ENABLE) {
        ROS_WARN_STREAM("Enabling AvTrak TDMA schedule");
        d->schedule_avtrak_enabled_ = true;
        if (!d->schedule_avtrak_timer_.hasPending()) {
          d->resetAvtrakSchedule();
        }
      }
      else if (schedule_msg.command == schedule_msg.COMMAND_DISABLE) {
        ROS_WARN_STREAM("Disabling AvTrak TDMA schedule");
        d->schedule_avtrak_enabled_ = false;
        d->schedule_avtrak_timer_.stop();
      }

      return ok;
    }

    else {
      ROS_ERROR("Invalid ASCHED Modem id: %d", schedule_msg.modem);
    }
  }

  // Write Bottom Follower Parameter (WBFP)
  if (type_id == sentry_acomms::SentryQueueManagerCommandEnum::BOTTOM_FOLLOWER_PARAM_COMMAND)
  {
    auto wbfp_msg = sentry_acomms::BottomFollowerParameterCommand{};
    const auto ok = ds_acomms_serialization::deserialize(msg.payload, wbfp_msg, as_string);
    if (ok)
    {
      auto jt_msg = std_msgs::String{};
      ds_acomms_serialization::serialize(wbfp_msg, jt_msg.data);
      ROS_INFO_STREAM("Passing through command to MC shim: '" << jt_msg.data << "'");
      d->mca_shim_jt_pub_.publish(jt_msg);
    }
    return ok;
  }

  // Write Bottom Follower Parameter (WDFP)
  if (type_id == sentry_acomms::SentryQueueManagerCommandEnum::DEPTH_FOLLOWER_PARAM_COMMAND)
  {
    auto wdfp_msg = sentry_acomms::DepthFollowerParameterCommand{};
    const auto ok = ds_acomms_serialization::deserialize(msg.payload, wdfp_msg, as_string);
    if (ok)
    {
      auto jt_msg = std_msgs::String{};
      ds_acomms_serialization::serialize(wdfp_msg, jt_msg.data);
      ROS_INFO_STREAM("Passing through command to MC shim: '" << jt_msg.data << "'");
      d->mca_shim_jt_pub_.publish(jt_msg);
    }
    return ok;
  }

  // Set Active Depth Goal (SADG)
  if (type_id == sentry_acomms::SentryQueueManagerCommandEnum::SET_ACTIVE_DEPTH_GOAL_COMMAND)
  {
    auto sadg = sentry_acomms::SetActiveDepthGoalCommand{};
    const auto ok = ds_acomms_serialization::deserialize(msg.payload, sadg, as_string);
    if (ok)
    {
      auto jt_msg = std_msgs::String{};
      ds_acomms_serialization::serialize(sadg, jt_msg.data);
      ROS_INFO_STREAM("Passing through command to MC shim: '" << jt_msg.data << "'");
      d->mca_shim_jt_pub_.publish(jt_msg);
    }
    return ok;
  }

  // Reson Command ('RESONMSG')
  if (type_id == sentry_acomms::SentryQueueManagerCommandEnum::RESON_COMMAND)
  {
    auto reson_msg = sentry_acomms::ResonCommand{};
    const auto ok = ds_acomms_serialization::deserialize(msg.payload, reson_msg, as_string);
    if (ok)
    {
      auto jt_msg = std_msgs::String{};
      ds_acomms_serialization::serialize(reson_msg, jt_msg.data);
      ROS_INFO_STREAM("Passing through command to RESON shim: '" << jt_msg.data << "'");
      d->reson_shim_jt_pub_.publish(jt_msg);
    }
    return ok;
  }

  // Kongsberg param msg ('KM_PARAM')
  if (type_id == sentry_acomms::SentryQueueManagerCommandEnum::KONGSBERG_EM2040_PARAM_COMMAND)
  {
    auto jt_cmd = std_msgs::String{};
    if(ds_acomms_serialization::deserialize(msg.payload, jt_cmd, as_string)) {
      //auto mca_msg = sentry_acomms::MissionControllerCommand{};
      //ds_acomms_serialization::serialize(mca_msg, cmd.data);
      ROS_INFO_STREAM("passing '" << jt_cmd.data << "' to mc shim");
      d->mca_shim_jt_pub_.publish(jt_cmd);
    }
    else {
      ROS_WARN_STREAM("unable to deserialize acoustic payload for " << sentry_acomms::SentryQueueManagerCommandEnum::KONGSBERG_EM2040_PARAM_CMD_STR);
    }
  }
  // Kongsberg Ping Command ('KM_PING')
  if (type_id == sentry_acomms::SentryQueueManagerCommandEnum::KONGSBERG_EM2040_PING_COMMAND)
  {
    auto jt_cmd = std_msgs::String{};
    if(ds_acomms_serialization::deserialize(msg.payload, jt_cmd, as_string)) {
      //auto mca_msg = sentry_acomms::MissionControllerCommand{};
      //ds_acomms_serialization::serialize(mca_msg, cmd.data);
      ROS_INFO_STREAM("passing '" << jt_cmd.data << "' to mc shim");
      d->mca_shim_jt_pub_.publish(jt_cmd);
    }
    else {
      ROS_WARN_STREAM("unable to deserialize acoustic payload for " << sentry_acomms::SentryQueueManagerCommandEnum::KONGSBERG_EM2040_PING_CMD_STR);
    }
  }
  // Kongsberg Kctrl Command ('KM_WC')
  if (type_id == sentry_acomms::SentryQueueManagerCommandEnum::KONGSBERG_EM2040_WATERCOLUMN_COMMAND)
  {
    auto jt_cmd = std_msgs::String{};
    if(ds_acomms_serialization::deserialize(msg.payload, jt_cmd, as_string)) {
      //auto mca_msg = sentry_acomms::MissionControllerCommand{};
      //ds_acomms_serialization::serialize(mca_msg, cmd.data);
      ROS_INFO_STREAM("passing '" << jt_cmd.data << "' to mc shim");
      d->mca_shim_jt_pub_.publish(jt_cmd);
    }
    else {
      ROS_WARN_STREAM("unable to deserialize acoustic payload for " << sentry_acomms::SentryQueueManagerCommandEnum::KONGSBERG_EM2040_WATERCOLUMN_CMD_STR);
    }
  }
  // Kongsberg klogger Command ('KLOGGER')
  if (type_id == sentry_acomms::SentryQueueManagerCommandEnum::KONGSBERG_EM2040_KLOGGER_COMMAND)
  {
    auto jt_cmd = std_msgs::String{};
    if(ds_acomms_serialization::deserialize(msg.payload, jt_cmd, as_string)) {
      //auto mca_msg = sentry_acomms::MissionControllerCommand{};
      //ds_acomms_serialization::serialize(mca_msg, cmd.data);
      ROS_INFO_STREAM("passing '" << jt_cmd.data << "' to mc shim");
      d->mca_shim_jt_pub_.publish(jt_cmd);
    }
    else {
      ROS_WARN_STREAM("unable to deserialize acoustic payload for " << sentry_acomms::SentryQueueManagerCommandEnum::KONGSBERG_EM2040_KLOGGER_CMD_STR);
    }
  }
  // Syprid Command ('PKZ')
  if (type_id == sentry_acomms::SentryQueueManagerCommandEnum::SYPRID_COMMAND)
  {
    auto syprid_cmd = sentry_acomms::SypridCommand{};
    const auto ok = ds_acomms_serialization::deserialize(msg.payload, syprid_cmd);
    if (ok) {
      ROS_ERROR_STREAM("Syprid message deserialized successfully! Syprid cmd is "<< static_cast<uint32_t>(syprid_cmd.pkz_cmd));
      auto srv = sentry_msgs::SypridCmd{};
      srv.request.pkz_cmd = syprid_cmd.pkz_cmd;
      srv.request.torque_1 = syprid_cmd.torque_1;
      srv.request.torque_2 = syprid_cmd.torque_2;
      if (d->syprid_srv_.exists())
      {
        if (d->syprid_srv_.call(srv.request, srv.response))
        {
          ROS_ERROR_STREAM("Syprid cmd service called successfully");
        }
        else
        {
          ROS_ERROR_STREAM("Syprid cmd service call failed");
        }
      }
      else
      {
        ROS_ERROR_STREAM("Syprid cmd service on " << d->syprid_srv_.getService() << "doesn't exist");
      }
    }
  }

  // Supr Command ('SUPR')
  if (type_id == sentry_acomms::SentryQueueManagerCommandEnum::SUPR_COMMAND)
  {
    auto supr_cmd = sentry_acomms::SuprCommand{};
    const auto ok = ds_acomms_serialization::deserialize(msg.payload, supr_cmd);
    if (ok) {
      ROS_ERROR_STREAM("Supr message deserialized successfully! Supr cmd is "<< static_cast<uint32_t>(supr_cmd.supr_cmd));
      auto srv = ds_supr::SuprCmd{};
      srv.request.supr_cmd = supr_cmd.supr_cmd;

      if (d->supr_srv_.exists())
      {
        if (d->supr_srv_.call(srv.request, srv.response))
        {
          ROS_ERROR_STREAM("Supr cmd service called successfully");
        }
        else
        {
          ROS_ERROR_STREAM("Supr cmd service call failed");
        }
      }
      else
      {
        ROS_ERROR_STREAM("Supr cmd service on " << d->supr_srv_.getService() << "doesn't exist");
      }
    }
  }

  // Bottom Follower Override ('BF_OVERRIDE')
  if (type_id == sentry_acomms::SentryQueueManagerCommandEnum::BOTTOM_FOLLOWER_OVERRIDE_COMMAND)
  {
    auto bf_override_msg = ds_control_msgs::ExternalBottomFollowTimedOverride{};
    const auto ok = ds_acomms_serialization::deserialize(msg.payload, bf_override_msg, as_string);
    if (ok)
    {
      ROS_INFO_STREAM("Sending override message to bottom follower, TIME: "
                      << bf_override_msg.timeout.toSec()
                      << " DIR:" << static_cast<int>(bf_override_msg.override_depth_direction));
      d->bf_override_pub_.publish(bf_override_msg);
    }
    return ok;
  }

  // Thrusters Enabled commmand
  if (type_id == sentry_acomms::SentryQueueManagerCommandEnum::THRUSTERS_ENABLED_COMMAND)
  {
    auto thrusters_enabled_msg = sentry_acomms::ThrustersEnabledCommand{};
    const auto ok = ds_acomms_serialization::deserialize(msg.payload, thrusters_enabled_msg, as_string);
    if (ok)
    {
      auto jt_msg = std_msgs::String{};
      jt_msg.data =
          thrusters_enabled_msg.enabled ? std::string{ "THRUSTERS_ENABLE" } : std::string{ "THRUSTERS_DISABLE" };
      ROS_INFO_STREAM("Sending thruster output command:" << jt_msg.data
                                                         << "to MC JT SHIM topic:" << d->mca_shim_jt_pub_.getTopic());
      d->mca_shim_jt_pub_.publish(jt_msg);
    }
    return ok;
  }

  // UDP Passthrough commands
  if (type_id >= UDP_PASSTHROUGH_COMMAND_START && type_id < UDP_PASSTHROUGH_COMMAND_END)
  {
    auto udp_msg = sentry_acomms::UdpQueueCommand{};
    const auto ok = ds_acomms_serialization::deserialize(msg.payload, udp_msg, as_string);
    if (ok)
    {
      auto queue_id = type_id - UDP_PASSTHROUGH_COMMAND_START + UDP_QUEUE_START;
      // ds_acomms_queue_manager::SimpleDsConnectionQueue* queue_ptr
      auto queue_ptr =
          dynamic_cast<ds_acomms_queue_manager::SimpleDsConnectionQueue<std_msgs::String>*>(queue(queue_id));

      if (queue_ptr != nullptr)
      {
        ROS_INFO_STREAM("Sending passthrough string: \"" << udp_msg.command << "\"");
        queue_ptr->conn()->send(udp_msg.command);
        return ok;
      }
      else
      {
        ROS_ERROR_STREAM("UDP Pasthrough queue could not be cast to SimpleDsConnectionQueue.  This should not be "
                         "possible");
      }
    }
  }

  // Acoustic Joystick Command ('AJOY')
  if (type_id == sentry_acomms::SentryQueueManagerCommandEnum::ACOUSTIC_JOYSTICK_COMMAND)
  {
    auto ajoy_msg = sentry_acomms::AcousticJoystickCommand{};
    const auto ok = ds_acomms_serialization::deserialize(msg.payload, ajoy_msg, as_string);
    if (ok)
    {
      ROS_INFO_STREAM("Successfully deserialized AJOY command.");
      auto srv = sentry_msgs::TimedJoystickService{};
      auto& req = srv.request;

      req.joystick.state.surge_u.value = ajoy_msg.surge_u;
      req.joystick.state.surge_u.valid = true;

      req.joystick.state.heave_w.value = ajoy_msg.heave_w;
      req.joystick.state.heave_w.valid = true;

      req.joystick.state.r.value = ajoy_msg.torque_w;
      req.joystick.state.r.valid = true;

      req.joystick.allocation = ajoy_msg.allocation_enum;
      req.joystick.controller = sentry_msgs::SentryControllerEnum::JOYSTICK;
      req.joystick.timeout = ajoy_msg.timeout;

      ROS_INFO_STREAM("Calling acoustic joystick service on '"
                      << d->acoustic_joystick_srv_.getService()
                      << "' with command with alloc: " << static_cast<int>(req.joystick.allocation)
                      << " duration: " << req.joystick.timeout << " surge: " << req.joystick.state.surge_u.value
                      << " (valid: " << static_cast<bool>(req.joystick.state.surge_u.valid) << ") "
                      << " heave: " << req.joystick.state.heave_w.value
                      << " (valid: " << static_cast<bool>(req.joystick.state.heave_w.valid) << ") "
                      << " torque: " << req.joystick.state.r.value
                      << " (valid: " << static_cast<bool>(req.joystick.state.r.valid) << ") ");
      if (d->acoustic_joystick_srv_.exists())
      {
        if (d->acoustic_joystick_srv_.call(srv.request, srv.response))
        {
          ROS_INFO_STREAM("Acoustic joystick service called successfully");
        }
        else
        {
          ROS_ERROR_STREAM("Acoustic joystick service call failed.");
        }
      }
      else
      {
        ROS_ERROR_STREAM("Acoustic joystick service on " << d->acoustic_joystick_srv_.getService() << "does not "
                                                                                                      "exist!");
      }
    }
    else if (as_string)
    {
      ROS_ERROR_STREAM("Unable to deserialze AJOY command: '"
                       << std::string(std::begin(msg.payload), std::end(msg.payload)) << "'");
    }
    else
    {
      ROS_ERROR_STREAM("Unable to deserialze binary AJOY command");
    }

    return ok;
  }

  // UDP Passthrough commands
  if (type_id >= UDP_PASSTHROUGH_COMMAND_START && type_id < UDP_PASSTHROUGH_COMMAND_END)
  {
    auto udp_msg = sentry_acomms::UdpQueueCommand{};
    const auto ok = ds_acomms_serialization::deserialize(msg.payload, udp_msg, as_string);
    if (ok)
    {
      auto queue_id = type_id - UDP_PASSTHROUGH_COMMAND_START + UDP_QUEUE_START;
      // ds_acomms_queue_manager::SimpleDsConnectionQueue* queue_ptr
      auto queue_ptr =
          dynamic_cast<ds_acomms_queue_manager::SimpleDsConnectionQueue<std_msgs::String>*>(queue(queue_id));

      if (queue_ptr != nullptr)
      {
        ROS_INFO_STREAM("Sending passthrough string: \"" << udp_msg.command << "\"");
        queue_ptr->conn()->send(udp_msg.command);
        return ok;
      }
      else
      {
        ROS_ERROR_STREAM("UDP Pasthrough queue could not be cast to SimpleDsConnectionQueue.  This should not be "
                         "possible");
      }
    }
  }

  // Navigation Shift (SHFTABS)
  if (type_id == sentry_acomms::SentryQueueManagerCommandEnum::NAV_SHIFT_COMMAND) {

    auto shift_msg = sentry_acomms::SentryNavShift{};
    const auto ok = ds_acomms_serialization::deserialize(msg.payload, shift_msg, as_string);
    if (ok) {
      ROS_INFO_STREAM("Sending NAV SHIFT message to the ds_nav_shift node");
      auto shift = ds_nav_msgs::Shift{};
      shift.header.stamp = msg.stamp;
      shift.shift_easting = shift_msg.easting;
      shift.shift_northing = shift_msg.northing;
      shift.shift_like_mc = true; // just hard-code for now
      d->nav_shift_pub_.publish(shift);
      return ok;
    }
    else if (as_string)
    {
      ROS_ERROR_STREAM("Unable to deserialze SHFTABS command: '"
                           << std::string(std::begin(msg.payload), std::end(msg.payload)) << "'");
    }
    else
    {
      ROS_ERROR_STREAM("Unable to deserialze binary SHFTABS command");
    }
  }

  // MX Event
  if (type_id == sentry_acomms::SentryQueueManagerCommandEnum::MX_EVENT_COMMAND) {
    auto evt_msg = sentry_acomms::SentryMxEvent{};
    const auto ok = ds_acomms_serialization::deserialize(msg.payload, evt_msg, as_string);
    if (ok) {
      ROS_INFO_STREAM("Sending MX EVENT message to the mxexec node");

      ds_mx_msgs::MxEvent evt;
      evt.header.stamp = msg.stamp;
      evt.eventid = evt_msg.event;

      d->mx_event_pub_.publish(evt);

      return ok;
    }
    else if (as_string)
    {
      ROS_ERROR_STREAM("Unable to deserialze MX Event command: '"
                           << std::string(std::begin(msg.payload), std::end(msg.payload)) << "'");
    }
    else
    {
      ROS_ERROR_STREAM("Unable to deserialze binary MX Event command");
    }
  }

  // MX Shared Parameter
  if (type_id == sentry_acomms::SentryQueueManagerCommandEnum::MX_SHARED_PARAM_COMMAND) {
    auto param_msg = sentry_acomms::SentryMxSharedParam{};
    const auto ok = ds_acomms_serialization::deserialize(msg.payload, param_msg, as_string);
    if (ok) {
      ROS_INFO_STREAM("Sending MX SHARED PARAM message to the mxexec node");

      ds_mx_msgs::MxUpdateSharedParams srv;

      srv.request.requested.header.stamp = msg.stamp;
      srv.request.requested.values = param_msg.values;

      if (d->mx_shared_param_srv_.exists())
      {
        if (d->mx_shared_param_srv_.call(srv.request, srv.response))
        {
          ROS_INFO_STREAM("MX shared parameter update called successfully");
        }
        else
        {
          ROS_ERROR_STREAM("MX shared parameter update  service call failed with: "
                               <<srv.response.response);
        }
      }
      else
      {
        ROS_ERROR_STREAM("MX shared parameter update service on "
                             << d->mx_shared_param_srv_.getService() << "does not exist!");
      }

      return ok;
    }
    else if (as_string)
    {
      ROS_ERROR_STREAM("Unable to deserialze MX SharedParam command: '"
                           << std::string(std::begin(msg.payload), std::end(msg.payload)) << "'");
    }
    else
    {
      ROS_ERROR_STREAM("Unable to deserialze binary MX SharedParam command");
    }
  }

  return false;
}

void SentryVehicleQueueManager::setupPublishers()
{
  QueueManager::setupPublishers();
  DS_D(SentryVehicleQueueManager);
  auto nh = nodeHandle();
  auto node_name = std::string{};
  auto str = std::string{};

  str = ros::names::resolve(ros::this_node::getName(), std::string{ "mc_jtros_topic" });
  if (!ros::param::get(str, node_name))
  {
    ROS_FATAL_STREAM("Parameter: '" << str << "' is not defined");
    ROS_BREAK();
  }
  d->mca_shim_jt_pub_ = nh.advertise<std_msgs::String>(node_name, 1, false);

  str = ros::names::resolve(ros::this_node::getName(), std::string{ "blueview_jtros_topic" });
  if (!ros::param::get(str, node_name))
  {
    ROS_FATAL_STREAM("Parameter: '" << str << "' is not defined");
    ROS_BREAK();
  }
  d->blueview_shim_jt_pub_ = nh.advertise<std_msgs::String>(node_name, 1, false);

  str = ros::names::resolve(ros::this_node::getName(), std::string{ "phinsins_command_topic" });
  if (!ros::param::get(str, node_name))
  {
    ROS_FATAL_STREAM("Parameter: '" << str << "' is not defined");
    ROS_BREAK();
  }
  d->phinsins_pub_ = nh.advertise<std_msgs::String>(node_name, 1, false);

  str = ros::names::resolve(ros::this_node::getName(), std::string{ "reson_jtros_topic" });
  if (!ros::param::get(str, node_name))
  {
    ROS_FATAL_STREAM("Parameter: '" << str << "' is not defined");
    ROS_BREAK();
  }
  d->reson_shim_jt_pub_ = nh.advertise<std_msgs::String>(node_name, 1, false);

  str = ros::names::resolve(ros::this_node::getName(), std::string{ "bf_override_topic" });
  if (!ros::param::get(str, node_name))
  {
    ROS_FATAL_STREAM("Parameter: '" << str << "' is not defined");
    ROS_BREAK();
  }
  d->bf_override_pub_ = nh.advertise<ds_control_msgs::ExternalBottomFollowTimedOverride>(node_name, 1, false);

  str = ros::names::resolve(ros::this_node::getName(), std::string{ "new_nav_shift" });
  if (!ros::param::get(str, node_name))
  {
    ROS_FATAL_STREAM("Parameter: '" << str << "' is not defined");
    ROS_BREAK();
  }
  d->nav_shift_pub_ = nh.advertise<ds_nav_msgs::Shift>(node_name, 1, false);

  str = ros::names::resolve(ros::this_node::getName(), std::string{ "mx_new_event" });
  if (!ros::param::get(str, node_name))
  {
    ROS_FATAL_STREAM("Parameter: '" << str << "' is not defined");
    ROS_BREAK();
  }
  d->mx_event_pub_ = nh.advertise<ds_mx_msgs::MxEvent>(node_name, 1, false);

  // 2021-11-18 SS - this allows to publish a navsatfix with usbl data sent to the vehicle
  str = ros::names::resolve(ros::this_node::getName(), std::string{ "usbl_data" });
  if (!ros::param::get(str, node_name))
  {
    ROS_FATAL_STREAM("Parameter: '" << str << "' is not defined");
    ROS_BREAK();
  }
  d->usbl_data_pub_ = nh.advertise<sensor_msgs::NavSatFix>(node_name, 1, false);

}

void SentryVehicleQueueManager::setupTimers()
{
  QueueManager::setupTimers();

  DS_D(SentryVehicleQueueManager);
  d->schedule_micromodem_timer_ = nodeHandle().createTimer(ros::Duration{ 0 }, &SentryVehicleQueueManager::scheduleMicroModemTimerCallback,
                                                this, true, false);
  d->resetMicromodemSchedule();

  d->schedule_avtrak_timer_ = nodeHandle().createTimer(ros::Duration{ 0 }, &SentryVehicleQueueManager::scheduleAvtrakTimerCallback,
                                                       this, true, false);
  d->resetAvtrakSchedule();
}

void SentryVehicleQueueManager::scheduleAvtrakTimerCallback(const ros::TimerEvent&)
{
  DS_D(SentryVehicleQueueManager);
  d->schedule_avtrak_timer_.stop();

  // Do nothing if the schedule is not enabled.
  if (!d->schedule_avtrak_enabled_)
  {
    return;
  }

  auto buf = std::vector<uint8_t>{};

  // QUESTION(LEL). Why is this hard-coded here? It exists as a parameter
  //  elsewhere in the system ...
  encodeQueueData(0, buf, true);
  sendModemData(1201, 1009, std::move(buf));

  ++d->next_avtrak_schedule_delay_;
  if (d->next_avtrak_schedule_delay_ == d->avtrak_schedule_delays_.size())
  {
    d->resetAvtrakSchedule();
    return;
  }

  const auto wait = ros::Duration{ d->avtrak_schedule_delays_.at(d->next_avtrak_schedule_delay_) };
  d->schedule_avtrak_timer_.setPeriod(wait, true);
  d->schedule_avtrak_timer_.start();
}

void SentryVehicleQueueManager::scheduleMicroModemTimerCallback(const ros::TimerEvent &){
  DS_D(SentryVehicleQueueManager);
  d->schedule_micromodem_timer_.stop();

  auto buf = std::vector<uint8_t>{};

  // Hard-coded for SDQ 0 to be sent out on the micromodem
  encodeQueueData(0, buf, true);
  sendModemData(0, 15, std::move(buf));

  ++d->next_micromodem_schedule_delay_;
  if (d->next_micromodem_schedule_delay_ == d->micromodem_schedule_delays_.size())
  {
    d->resetMicromodemSchedule();
    return;
  }

  const auto wait = ros::Duration{ d->micromodem_schedule_delays_.at(d->next_micromodem_schedule_delay_) };
  d->schedule_micromodem_timer_.setPeriod(wait, true);
  d->schedule_micromodem_timer_.start();
}

void SentryVehicleQueueManager::setupServices()
{
  DsProcess::setupServices();
  DS_D(SentryVehicleQueueManager);

  // acoustic joystick service
  auto param_name = ros::names::resolve(ros::this_node::getName(), std::string{ "acoustic_joystick_service" });
  auto param_value = std::string{};
  if (!ros::param::get(param_name, param_value))
  {
    ROS_FATAL_STREAM("No parameter named '" << param_name << "'");
    ROS_BREAK();
  }

  d->acoustic_joystick_srv_ = nodeHandle().serviceClient<sentry_msgs::TimedJoystickService>(param_value);
  ROS_INFO_STREAM("Waiting 10s for TimedJoystickService on '" << param_value << "'");
  if (!d->acoustic_joystick_srv_.waitForExistence(ros::Duration(10)))
  {
    ROS_FATAL_STREAM("Timed out waiting for TimedJoystickService on '" << param_value << "'");
    ROS_BREAK();
  }

  // MX shared parameter updating service
  param_name = ros::names::resolve(ros::this_node::getName(), std::string{ "mx_new_shared_param" });
  param_value = std::string{};
  if (!ros::param::get(param_name, param_value))
  {
    ROS_FATAL_STREAM("Parameter: '" << param_name << "' is not defined");
    ROS_BREAK();
  }
  d->mx_shared_param_srv_ = nodeHandle().serviceClient<ds_mx_msgs::MxUpdateSharedParams>(param_value);
  /**
   // Uncomment when MX is ready for prime-time
  ROS_INFO_STREAM("Waiting 10s for MxUpdateSharedParams Service on '" <<param_value <<"'");
  if (!d->mx_shared_param_srv_.waitForExistence(ros::Duration(10)))
  {
    ROS_FATAL_STREAM("Timed out waiting for MxUpdateSharedParams on '" << param_value << "'");
    ROS_BREAK();
  }
  */

  auto syprid_service =
      ros::param::param<std::string>("~syprid_srv", "/sentry/pkz/syprid_cmd");
  d->syprid_srv_ = nodeHandle().serviceClient<sentry_msgs::SypridCmd>(syprid_service);

  auto supr_service =
      ros::param::param<std::string>("~supr_srv", "/sentry/samplers/supr/supr_cmd");
  d->supr_srv_ = nodeHandle().serviceClient<ds_supr::SuprCmd>(supr_service);
  
}

SentryVehicleQueueManager::~SentryVehicleQueueManager() = default;
}
