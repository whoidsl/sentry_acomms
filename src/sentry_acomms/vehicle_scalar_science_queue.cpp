/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

// Created by llindzey on 31 Jan 2019

#include "sentry_acomms/vehicle_scalar_science_queue.h"

//#include "sentry_acomms/serialization.h"

#include "ds_hotel_msgs/A2D2.h"
#include "ds_sensor_msgs/Ctd.h"
#include "ds_sensor_msgs/OxygenConcentration.h"
#include "ds_sensor_msgs/DepthPressure.h"

#include <ros/ros.h>

namespace sentry_acomms
{
VehicleScalarScienceQueue::VehicleScalarScienceQueue() : SimpleSingleQueue<SentryScalarScience>()
{
}

VehicleScalarScienceQueue::~VehicleScalarScienceQueue() = default;

void VehicleScalarScienceQueue::setup(ros::NodeHandle& nh)
{

  auto topic_name = std::string{};
  topic_name = ros::names::resolve(ros::this_node::getName(),
				   std::string{"anderaa_optode_data_topic"});
  auto anderaa_topic = std::string{};
  if (!ros::param::get(topic_name, anderaa_topic))
  {
    ROS_FATAL_STREAM("Parameter: '" << topic_name << "' is not defined");
    ROS_BREAK();
  }
  anderaa_optode_sub_ = nh.subscribe(anderaa_topic, 1,
				     &VehicleScalarScienceQueue::anderaaOptodeCallback, this);

  topic_name = ros::names::resolve(ros::this_node::getName(), std::string{"obs_data_topic"});
  auto obs_topic = std::string{};
  if (!ros::param::get(topic_name, obs_topic))
  {
    ROS_FATAL_STREAM("Parameter: '" << topic_name << "' is not defined");
    ROS_BREAK();
  }
  obs_sub_ = nh.subscribe(obs_topic, 1, &VehicleScalarScienceQueue::obsCallback, this);

  topic_name = ros::names::resolve(ros::this_node::getName(), std::string{"orp_data_topic"});
  auto orp_topic = std::string{};
  if (!ros::param::get(topic_name, orp_topic))
  {
    ROS_FATAL_STREAM("Parameter: '" << topic_name << "' is not defined");
    ROS_BREAK();
  }
  orp_sub_ = nh.subscribe(orp_topic, 1, &VehicleScalarScienceQueue::orpCallback, this);

  topic_name = ros::names::resolve(ros::this_node::getName(), std::string{"sbe49_ctd_data_topic"});
  auto sbe49_topic = std::string{};
  if (!ros::param::get(topic_name, sbe49_topic))
  {
    ROS_FATAL_STREAM("Parameter: '" << topic_name << "' is not defined");
    ROS_BREAK();
  }
  sbe49_sub_ = nh.subscribe(sbe49_topic, 1, &VehicleScalarScienceQueue::sbe49Callback, this);

  topic_name = ros::names::resolve(ros::this_node::getName(), std::string{"paro_depth_data_topic"});
  auto paro_topic = std::string{};
  if (!ros::param::get(topic_name, paro_topic))
  {
    ROS_FATAL_STREAM("Parameter: '" << topic_name << "' is not defined");
    ROS_BREAK();
  }
  paro_sub_ = nh.subscribe(paro_topic, 1, &VehicleScalarScienceQueue::paroCallback, this);

}

void VehicleScalarScienceQueue::anderaaOptodeCallback(const ds_sensor_msgs::OxygenConcentration& msg)
{
  item_.oxygen_concentration = static_cast<float>(msg.concentration);
}

void VehicleScalarScienceQueue::obsCallback(const ds_hotel_msgs::A2D2& msg)
{
  // Based on the post-processing code, we only ever plot the raw values
  // of the first element in the vector.
  item_.obs_raw = msg.raw[0];
}

void VehicleScalarScienceQueue::orpCallback(const ds_hotel_msgs::A2D2& msg)
{
  item_.orp_raw = msg.raw[0];
}

void VehicleScalarScienceQueue::sbe49Callback(const ds_sensor_msgs::Ctd& msg)
{
  item_.ctd_temperature = msg.temperature;
  item_.ctd_salinity = msg.salinity;
}

void VehicleScalarScienceQueue::paroCallback(const ds_sensor_msgs::DepthPressure& msg)
{
  // TODO(LEL): I haven't yet tracked down for sure that this is the correct
  //  field to use. The message definition's fields suggest that this may have
  //  been corrected for latitude by the instrument or driver?
  item_.paro_depth = msg.depth;
}

} // namespace sentry_acomms
