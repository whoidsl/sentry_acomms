/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_acomms/vehicle_depth_follower_queue.h"
#include "sentry_acomms/serialization.h"
#include "ds_control_msgs/DepthFollow.h"

namespace sentry_acomms
{
VehicleDepthFollowerQueue::VehicleDepthFollowerQueue() : SimpleSingleQueue<SentryDepthFollowerParameters>()
{
}

VehicleDepthFollowerQueue::~VehicleDepthFollowerQueue() = default;

void VehicleDepthFollowerQueue::setup(ros::NodeHandle& nh)
{
  const auto goals_ns = nh.param<std::string>("goals_ns", "");
  const auto nav_ns = nh.param<std::string>("nav_ns", "");
  const auto vehicle_ns = nh.param<std::string>("vehicle_ns", "");

  auto node_name = std::string{};
  auto str = std::string{};

  str = ros::names::resolve(ros::this_node::getName(), std::string{ "depth_follower_node" });
  // Depth follower
  if (!ros::param::get(str, node_name))
  {
    ROS_FATAL_STREAM("Parameter: '" << str << "' is not defined");
    ROS_BREAK();
  }

  if (!goals_ns.empty())
  {
    node_name = ros::names::resolve(goals_ns, node_name);
  }

  str = ros::names::resolve(node_name, std::string{ "state" });
  sub_ = nh.subscribe(str, 1, &VehicleDepthFollowerQueue::depthFollowerCallback, this);
}

void VehicleDepthFollowerQueue::depthFollowerCallback(const ds_control_msgs::DepthFollow& msg)
{
  item_.depth = static_cast<float>(msg.desired_depth);
  item_.envelope = static_cast<float>(msg.envelope);
  item_.speed = static_cast<float>(msg.desired_speed);
  item_.speed_gain = static_cast<float>(msg.speed_gain);
  item_.depth_rate = static_cast<float>(msg.depth_rate);
  item_.depth_acceleration = static_cast<float>(msg.depth_acceleration);
  item_.minimum_speed = static_cast<float>(msg.minimum_speed);
  item_.minimum_altitude = static_cast<float>(msg.minimum_altitude);
  item_.depth_floor = static_cast<float>(msg.depth_floor);
  item_.next_depth_goal = msg.next_depth_goal;
  item_.next_speed_goal = msg.next_speed_goal;
  item_.current_altitude = msg.current_altitude;
  item_.enabled = msg.enabled;
}
}
