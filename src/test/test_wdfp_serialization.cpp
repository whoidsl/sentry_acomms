/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_acomms/serialization.h"
#include "sentry_acomms/BottomFollowerParameterCommand.h"

#include <gtest/gtest.h>

TEST(VehicleSingleBottomFollowerSerialization, serialize_as_string)
{
  const auto expected_str = std::string{ "WBFP 5 20.00" };

  auto msg = sentry_acomms::BottomFollowerParameterCommand{};
  msg.index = 5;
  msg.value = 20.0;

  auto serialized_string = std::string{};
  ds_acomms_serialization::serialize(msg, serialized_string);

  EXPECT_STREQ(expected_str.data(), serialized_string.data());
}

TEST(VehicleSingleBottomFollowerSerialization, deserialize_from_string)
{
  const auto str = std::string{ "WBFP 5 20.00" };

  auto expected_msg = sentry_acomms::BottomFollowerParameterCommand{};
  expected_msg.index = 5;
  expected_msg.value = 20;

  auto result = sentry_acomms::BottomFollowerParameterCommand{};

  ds_acomms_serialization::deserialize(str, result);

  EXPECT_EQ(expected_msg.index, result.index);
  EXPECT_FLOAT_EQ(expected_msg.value, result.value);
}

TEST(VehicleSingleBottomFollowerSerialization, deserialize_invalid_from_string)
{
  const auto invalid_strings =
      std::vector<std::string>{ "WBF 52.0 24.0 12.0 0.62 20.0 72.0 234.0 73.2", "WBFP 60 234.0", "WBFP -3 324.0" };

  auto result = sentry_acomms::BottomFollowerParameterCommand{};

  ASSERT_FALSE(ds_acomms_serialization::deserialize(invalid_strings.at(0), result));
  EXPECT_EQ(sentry_acomms::BottomFollowerParameterCommand::INDEX_INVALID, result.index);

  ASSERT_FALSE(ds_acomms_serialization::deserialize(invalid_strings.at(1), result));
  EXPECT_EQ(sentry_acomms::BottomFollowerParameterCommand::INDEX_INVALID, result.index);

  ASSERT_FALSE(ds_acomms_serialization::deserialize(invalid_strings.at(2), result));
  EXPECT_EQ(sentry_acomms::BottomFollowerParameterCommand::INDEX_INVALID, result.index);
}
// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
