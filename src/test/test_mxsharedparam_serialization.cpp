/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 10/1/19.
//

#include <gtest/gtest.h>

#include "sentry_acomms/serialization.h"
#include "sentry_acomms/SentryMxSharedParam.h"

TEST(SentryMxSharedParamSerialization, serialize_as_string)
{
  std::string expected_str("MXSP mb_alt:40,mb_speed:0.6");
  sentry_acomms::SentryMxSharedParam msg;
  msg.values.resize(2);
  msg.values[0].key = "mb_alt";
  msg.values[0].value = "40";
  msg.values[1].key = "mb_speed";
  msg.values[1].value = "0.6";

  std::string serialized_str;

  ds_acomms_serialization::serialize(msg, serialized_str);
  EXPECT_EQ(expected_str, serialized_str);
}

TEST(SentryMxSharedParamSerialization, deserialize_as_string)
{
  std::string str("MXSP mb_alt:40,mb_speed:0.6");
  sentry_acomms::SentryMxSharedParam expected_msg;
  expected_msg.values.resize(2);
  expected_msg.values[0].key = "mb_alt";
  expected_msg.values[0].value = "40";
  expected_msg.values[1].key = "mb_speed";
  expected_msg.values[1].value = "0.6";

  sentry_acomms::SentryMxSharedParam result;
  EXPECT_TRUE(ds_acomms_serialization::deserialize(str, result));

  ASSERT_EQ(expected_msg.values.size(), result.values.size());
  EXPECT_EQ(expected_msg.values[0].key, result.values[0].key);
  EXPECT_EQ(expected_msg.values[0].value, result.values[0].value);
  EXPECT_EQ(expected_msg.values[1].key, result.values[1].key);
  EXPECT_EQ(expected_msg.values[1].value, result.values[1].value);
}

TEST(SentryMxSharedParamSerialization, deserialize_as_string_trailing)
{
  std::string str("MXSP mb_alt:40,mb_speed:0.6,");
  sentry_acomms::SentryMxSharedParam expected_msg;
  expected_msg.values.resize(2);
  expected_msg.values[0].key = "mb_alt";
  expected_msg.values[0].value = "40";
  expected_msg.values[1].key = "mb_speed";
  expected_msg.values[1].value = "0.6";

  sentry_acomms::SentryMxSharedParam result;
  EXPECT_TRUE(ds_acomms_serialization::deserialize(str, result));

  ASSERT_EQ(expected_msg.values.size(), result.values.size());
  EXPECT_EQ(expected_msg.values[0].key, result.values[0].key);
  EXPECT_EQ(expected_msg.values[0].value, result.values[0].value);
  EXPECT_EQ(expected_msg.values[1].key, result.values[1].key);
  EXPECT_EQ(expected_msg.values[1].value, result.values[1].value);
}

TEST(SentryMxSharedParamSerialization, deserialize_as_string_novalue)
{
  std::string str("MXSP mb_alt:40,mb_speed:");
  sentry_acomms::SentryMxSharedParam expected_msg;
  expected_msg.values.resize(2);
  expected_msg.values[0].key = "mb_alt";
  expected_msg.values[0].value = "40";
  expected_msg.values[1].key = "mb_speed";
  expected_msg.values[1].value = "";

  sentry_acomms::SentryMxSharedParam result;
  EXPECT_TRUE(ds_acomms_serialization::deserialize(str, result));

  ASSERT_EQ(expected_msg.values.size(), result.values.size());
  EXPECT_EQ(expected_msg.values[0].key, result.values[0].key);
  EXPECT_EQ(expected_msg.values[0].value, result.values[0].value);
  EXPECT_EQ(expected_msg.values[1].key, result.values[1].key);
  EXPECT_EQ(expected_msg.values[1].value, result.values[1].value);
}

TEST(SentryMxSharedParamSerialization, deserialize_as_string_novalue_trailing)
{
  std::string str("MXSP mb_alt:40,mb_speed:,");
  sentry_acomms::SentryMxSharedParam expected_msg;
  expected_msg.values.resize(2);
  expected_msg.values[0].key = "mb_alt";
  expected_msg.values[0].value = "40";
  expected_msg.values[1].key = "mb_speed";
  expected_msg.values[1].value = "";

  sentry_acomms::SentryMxSharedParam result;
  EXPECT_TRUE(ds_acomms_serialization::deserialize(str, result));

  ASSERT_EQ(expected_msg.values.size(), result.values.size());
  EXPECT_EQ(expected_msg.values[0].key, result.values[0].key);
  EXPECT_EQ(expected_msg.values[0].value, result.values[0].value);
  EXPECT_EQ(expected_msg.values[1].key, result.values[1].key);
  EXPECT_EQ(expected_msg.values[1].value, result.values[1].value);
}

TEST(SentryMxSharedParamSerialization, deserialize_as_string_nocolon)
{
  std::string str("MXSP mb_alt:40,mb_speed");
  sentry_acomms::SentryMxSharedParam expected_msg;
  expected_msg.values.resize(2);
  expected_msg.values[0].key = "mb_alt";
  expected_msg.values[0].value = "40";
  expected_msg.values[1].key = "mb_speed";
  expected_msg.values[1].value = "";

  sentry_acomms::SentryMxSharedParam result;
  EXPECT_FALSE(ds_acomms_serialization::deserialize(str, result));
}

TEST(SentryMxSharedParamSerialization, deserialize_as_string_nocolon_trailing)
{
  std::string str("MXSP mb_alt:40,mb_speed,");
  sentry_acomms::SentryMxSharedParam expected_msg;
  expected_msg.values.resize(2);
  expected_msg.values[0].key = "mb_alt";
  expected_msg.values[0].value = "40";
  expected_msg.values[1].key = "mb_speed";
  expected_msg.values[1].value = "";

  sentry_acomms::SentryMxSharedParam result;
  EXPECT_FALSE(ds_acomms_serialization::deserialize(str, result));
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  ros::Time::init();
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
