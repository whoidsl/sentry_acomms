/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_acomms/serialization.h"
#include "sentry_acomms/SentryBottomFollowerParameters.h"
#include "test_util.h"

#include <gtest/gtest.h>

TEST(VehicleBottomFollowerSerialization, serialize_as_string)
{
  const auto expected_str = std::string{ "140.00 20.00 1.00 4.00 0.33 0.07 0.40 15.00 4500.00 0 -1" };

  auto msg = sentry_acomms::SentryBottomFollowerParameters{};
  msg.altitude = 140;
  msg.envelope = 20;
  msg.speed = 1.0;
  msg.speed_gain = 4.0;
  msg.depth_rate = 0.33;
  msg.depth_acceleration = 0.07;
  msg.min_speed = 0.4;
  msg.alarm_timeout = 15;
  msg.depth_floor = 4500;
  msg.user_override_active = false;
  msg.user_override_direction = -1;

  auto serialized_string = std::string{};
  ds_acomms_serialization::serialize(msg, serialized_string);

  ASSERT_TRUE(sentry_acomms::valid_avtrak_string(serialized_string));
  EXPECT_STREQ(expected_str.data(), serialized_string.data());
}

TEST(VehicleBottomFollowerSerialization, deserialize_from_string)
{
  const auto str = std::string{ "140.00 20.00 1.00 4.00 0.33 0.07 0.40 15.00 4500.00 0 -1" };

  auto expected_msg = sentry_acomms::SentryBottomFollowerParameters{};
  expected_msg.altitude = 140;
  expected_msg.envelope = 20;
  expected_msg.speed = 1.0;
  expected_msg.speed_gain = 4.0;
  expected_msg.depth_rate = 0.33;
  expected_msg.depth_acceleration = 0.07;
  expected_msg.min_speed = 0.4;
  expected_msg.alarm_timeout = 15;
  expected_msg.depth_floor = 4500;
  expected_msg.user_override_active = false;
  expected_msg.user_override_direction = -1;

  auto result = sentry_acomms::SentryBottomFollowerParameters{};

  ds_acomms_serialization::deserialize(str, result);

  EXPECT_FLOAT_EQ(expected_msg.altitude, result.altitude);
  EXPECT_FLOAT_EQ(expected_msg.envelope, result.envelope);
  EXPECT_FLOAT_EQ(expected_msg.speed, result.speed);
  EXPECT_FLOAT_EQ(expected_msg.speed_gain, result.speed_gain);
  EXPECT_FLOAT_EQ(expected_msg.depth_rate, result.depth_rate);
  EXPECT_FLOAT_EQ(expected_msg.depth_acceleration, result.depth_acceleration);
  EXPECT_FLOAT_EQ(expected_msg.min_speed, result.min_speed);
  EXPECT_FLOAT_EQ(expected_msg.alarm_timeout, result.alarm_timeout);
  EXPECT_FLOAT_EQ(expected_msg.depth_floor, result.depth_floor);
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
