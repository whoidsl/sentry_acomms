/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_acomms/serialization.h"
#include "sentry_acomms/SentryDepthFollowerParameters.h"
#include "test_util.h"

#include <gtest/gtest.h>

TEST(VehicleBottomFollowerSerialization, serialize_as_string)
{
  const auto expected_str = std::string{ "140.00 20.00 1.00 4.00 0.33 0.07 0.40 15.00 4500.00 120.32 1.00 -3.00 1" };

  auto msg = sentry_acomms::SentryDepthFollowerParameters{};
  msg.depth = 140;
  msg.envelope = 20;
  msg.speed = 1.0;
  msg.speed_gain = 4.0;
  msg.depth_rate = 0.33;
  msg.depth_acceleration = 0.07;
  msg.minimum_speed = 0.4;
  msg.minimum_altitude = 15;
  msg.depth_floor = 4500;
  msg.next_depth_goal = 120.32;
  msg.next_speed_goal = 1.0;
  msg.current_altitude = -3.0;
  msg.enabled = true;

  auto serialized_string = std::string{};
  ds_acomms_serialization::serialize(msg, serialized_string);

  ASSERT_TRUE(sentry_acomms::valid_avtrak_string(serialized_string));
  EXPECT_STREQ(expected_str.data(), serialized_string.data());
}

TEST(VehicleBottomFollowerSerialization, deserialize_from_string)
{
  const auto str = std::string{ "140.00 20.00 1.00 4.00 0.33 0.07 0.40 15.00 4500.00 120.32 1.00 65.00 1" };

  auto expected_msg = sentry_acomms::SentryDepthFollowerParameters{};
  expected_msg.depth = 140;
  expected_msg.envelope = 20;
  expected_msg.speed = 1.0;
  expected_msg.speed_gain = 4.0;
  expected_msg.depth_rate = 0.33;
  expected_msg.depth_acceleration = 0.07;
  expected_msg.minimum_speed = 0.4;
  expected_msg.minimum_altitude = 15;
  expected_msg.depth_floor = 4500;
  expected_msg.next_depth_goal = 120.32;
  expected_msg.next_speed_goal = 1.0;
  expected_msg.current_altitude = 65.0;
  expected_msg.enabled = true;

  auto result = sentry_acomms::SentryDepthFollowerParameters{};

  ds_acomms_serialization::deserialize(str, result);

  EXPECT_FLOAT_EQ(expected_msg.depth, result.depth);
  EXPECT_FLOAT_EQ(expected_msg.envelope, result.envelope);
  EXPECT_FLOAT_EQ(expected_msg.speed, result.speed);
  EXPECT_FLOAT_EQ(expected_msg.speed_gain, result.speed_gain);
  EXPECT_FLOAT_EQ(expected_msg.depth_rate, result.depth_rate);
  EXPECT_FLOAT_EQ(expected_msg.depth_acceleration, result.depth_acceleration);
  EXPECT_FLOAT_EQ(expected_msg.minimum_speed, result.minimum_speed);
  EXPECT_FLOAT_EQ(expected_msg.minimum_altitude, result.minimum_altitude);
  EXPECT_FLOAT_EQ(expected_msg.depth_floor, result.depth_floor);
  EXPECT_FLOAT_EQ(expected_msg.next_depth_goal, result.next_depth_goal);
  EXPECT_FLOAT_EQ(expected_msg.next_speed_goal, result.next_speed_goal);
  EXPECT_FLOAT_EQ(expected_msg.current_altitude, result.current_altitude);
  EXPECT_FLOAT_EQ(expected_msg.enabled, result.enabled);
}


// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
