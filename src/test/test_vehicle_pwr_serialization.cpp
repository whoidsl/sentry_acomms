/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_acomms/serialization.h"
#include "sentry_acomms/SentryPowerSwitches.h"
#include "test_util.h"

#include <gtest/gtest.h>

TEST(VehiclePowerSwitchesSerialization, serialize_as_string)
{
  auto msg = sentry_acomms::SentryPowerSwitches{};

  msg.power[0] = 0xAF;
  msg.power[1] = 0xFF;
  msg.power[2] = 0x0A;
  msg.power[3] = 0x1;
  msg.power[4] = 0;
  msg.power[5] = 0x50;
  msg.power[6] = 0x33;
  msg.power[7] = 0x55;

  const auto expected_str = std::string{ "AF,FF,0A,01,00,50,33,55" };

  auto serialized_string = std::string{};
  ds_acomms_serialization::serialize(msg, serialized_string);

  ASSERT_TRUE(sentry_acomms::valid_avtrak_string(serialized_string));
  EXPECT_STREQ(expected_str.data(), serialized_string.data());
}

TEST(VehiclePowerSwitchesSerialization, deserialize_from_string)
{
  const auto str = std::string{ "AF,FF,0A,01,00,50,33,55" };
  auto expected_msg = sentry_acomms::SentryPowerSwitches{};

  expected_msg.power[0] = 0xAF;
  expected_msg.power[1] = 0xFF;
  expected_msg.power[2] = 0x0A;
  expected_msg.power[3] = 0x1;
  expected_msg.power[4] = 0;
  expected_msg.power[5] = 0x50;
  expected_msg.power[6] = 0x33;
  expected_msg.power[7] = 0x55;

  auto result = sentry_acomms::SentryPowerSwitches{};

  ds_acomms_serialization::deserialize(str, result);

  EXPECT_EQ(expected_msg.power[0], result.power[0]);
  EXPECT_EQ(expected_msg.power[1], result.power[1]);
  EXPECT_EQ(expected_msg.power[2], result.power[2]);
  EXPECT_EQ(expected_msg.power[3], result.power[3]);
  EXPECT_EQ(expected_msg.power[4], result.power[4]);
  EXPECT_EQ(expected_msg.power[5], result.power[5]);
  EXPECT_EQ(expected_msg.power[6], result.power[6]);
  EXPECT_EQ(expected_msg.power[7], result.power[7]);
}

TEST(VehiclePowerSwitchesSerialization, deserialize_from_string_failures)
{
  auto msg = sentry_acomms::SentryPowerSwitches{};

  const auto str_short = std::string{ "AF,3F,55,30" };
  EXPECT_FALSE(ds_acomms_serialization::deserialize(str_short, msg));
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
