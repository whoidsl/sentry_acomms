/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_acomms/serialization.h"
#include "sentry_acomms/SentryControlAndActuatorState.h"
#include "test_util.h"

#include <gtest/gtest.h>

TEST(VehicleControlActuatorSerialization, serialize_as_string)
{
  auto msg = sentry_acomms::SentryControlAndActuatorState{};
  const auto expected_str = std::string{ "3 1 230 20 253 30.0 34.0 30 34 FF F4 8.0 7.5 -8.0 -7.5 3.0 3.5 2.0 -3.0" };

  msg.active_controller = 3;
  msg.active_allocation = 1;
  msg.force_u = 230;
  msg.force_w = 20;
  msg.torque_q = 253;
  msg.fwd_servo_deg = 30;
  msg.aft_servo_deg = 34;
  msg.fwd_servo_switch = 0x30;
  msg.aft_servo_switch = 0x34;
  msg.fwd_servo_speed = 0xFF;
  msg.aft_servo_speed = 0xF4;
  msg.fwd_port_thruster_desired_current = 8;
  msg.fwd_port_thruster_measured_current = 7.5;
  msg.fwd_stbd_thruster_desired_current = -8;
  msg.fwd_stbd_thruster_measured_current = -7.5;
  msg.aft_port_thruster_desired_current = 3.0;
  msg.aft_port_thruster_measured_current = 3.5;
  msg.aft_stbd_thruster_desired_current = 2;
  msg.aft_stbd_thruster_measured_current = -3;
  auto serialized_string = std::string{};
  ds_acomms_serialization::serialize(msg, serialized_string);

  ASSERT_TRUE(sentry_acomms::valid_avtrak_string(serialized_string));
  EXPECT_STREQ(expected_str.data(), serialized_string.data());
}

TEST(VehicleControlActuatorSerialization, deserialize_from_string)
{
  auto expected_msg = sentry_acomms::SentryControlAndActuatorState{};
  const auto str = std::string{ "3 1 230 20 253 30.0 34.0 30 34 FF F4 8.0 7.5 -8.0 -7.5 3.0 3.5 2.0 -3.0" };

  expected_msg.active_controller = 3;
  expected_msg.active_allocation = 1;
  expected_msg.force_u = 230;
  expected_msg.force_w = 20;
  expected_msg.torque_q = 253;
  expected_msg.fwd_servo_deg = 30;
  expected_msg.aft_servo_deg = 34;
  expected_msg.fwd_servo_switch = 0x30;
  expected_msg.aft_servo_switch = 0x34;
  expected_msg.fwd_servo_speed = 0xFF;
  expected_msg.aft_servo_speed = 0xF4;
  expected_msg.fwd_port_thruster_desired_current = 8;
  expected_msg.fwd_port_thruster_measured_current = 7.5;
  expected_msg.fwd_stbd_thruster_desired_current = -8;
  expected_msg.fwd_stbd_thruster_measured_current = -7.5;
  expected_msg.aft_port_thruster_desired_current = 3.0;
  expected_msg.aft_port_thruster_measured_current = 3.5;
  expected_msg.aft_stbd_thruster_desired_current = 2;
  expected_msg.aft_stbd_thruster_measured_current = -3;

  auto result = sentry_acomms::SentryControlAndActuatorState{};

  ASSERT_TRUE(ds_acomms_serialization::deserialize(str, result));

  EXPECT_EQ(expected_msg.active_controller, result.active_controller);
  EXPECT_EQ(expected_msg.active_allocation, result.active_allocation);
  EXPECT_FLOAT_EQ(expected_msg.force_u, result.force_u);
  EXPECT_FLOAT_EQ(expected_msg.force_w, result.force_w);
  EXPECT_FLOAT_EQ(expected_msg.torque_q, result.torque_q);
  EXPECT_FLOAT_EQ(expected_msg.fwd_servo_deg, result.fwd_servo_deg);
  EXPECT_FLOAT_EQ(expected_msg.aft_servo_deg, result.aft_servo_deg);
  EXPECT_FLOAT_EQ(expected_msg.fwd_servo_switch, result.fwd_servo_switch);
  EXPECT_FLOAT_EQ(expected_msg.aft_servo_switch, result.aft_servo_switch);
  EXPECT_FLOAT_EQ(expected_msg.fwd_servo_speed, result.fwd_servo_speed);
  EXPECT_FLOAT_EQ(expected_msg.aft_servo_speed, result.aft_servo_speed);
  EXPECT_FLOAT_EQ(expected_msg.fwd_port_thruster_desired_current, result.fwd_port_thruster_desired_current);
  EXPECT_FLOAT_EQ(expected_msg.fwd_port_thruster_measured_current, result.fwd_port_thruster_measured_current);
  EXPECT_FLOAT_EQ(expected_msg.fwd_stbd_thruster_desired_current, result.fwd_stbd_thruster_desired_current);
  EXPECT_FLOAT_EQ(expected_msg.fwd_stbd_thruster_measured_current, result.fwd_stbd_thruster_measured_current);
  EXPECT_FLOAT_EQ(expected_msg.aft_port_thruster_desired_current, result.aft_port_thruster_desired_current);
  EXPECT_FLOAT_EQ(expected_msg.aft_port_thruster_measured_current, result.aft_port_thruster_measured_current);
  EXPECT_FLOAT_EQ(expected_msg.aft_stbd_thruster_desired_current, result.aft_stbd_thruster_desired_current);
  EXPECT_FLOAT_EQ(expected_msg.aft_stbd_thruster_measured_current, result.aft_stbd_thruster_measured_current);
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
