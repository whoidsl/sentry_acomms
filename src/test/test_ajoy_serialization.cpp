/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_acomms/serialization.h"
#include "sentry_acomms/AcousticJoystickCommand.h"

#include <gtest/gtest.h>

TEST(VehicleAcoustickJoystickSerialization, serialize_as_string)
{
  auto expected_str = std::string{ "AJOY 00:02:36 1 1.0 0.5 0.2" };

  auto msg = sentry_acomms::AcousticJoystickCommand{};
  msg.timeout = ros::Duration{ 2 * 60.0 + 36 };
  msg.allocation_enum = 1;
  msg.surge_u = 1.0;
  msg.heave_w = 0.5;
  msg.torque_w = 0.2;

  auto serialized_string = std::string{};
  ds_acomms_serialization::serialize(msg, serialized_string);

  EXPECT_STREQ(expected_str.data(), serialized_string.data());
}

TEST(VehicleAcoustickJoystickSerialization, deserialize_from_string)
{
  const auto str = std::string{ "AJOY 00:02:36 1 1.0 0.5 0.2" };

  auto expected_msg = sentry_acomms::AcousticJoystickCommand{};
  expected_msg.surge_u = 1.0;
  expected_msg.heave_w = 0.5;
  expected_msg.torque_w = 0.2;
  expected_msg.allocation_enum = 1;
  expected_msg.timeout = ros::Duration{ 2 * 60 + 36 };

  auto result = sentry_acomms::AcousticJoystickCommand{};

  EXPECT_TRUE(ds_acomms_serialization::deserialize(str, result));

  EXPECT_FLOAT_EQ(expected_msg.surge_u, result.surge_u);
  EXPECT_FLOAT_EQ(expected_msg.heave_w, result.heave_w);
  EXPECT_FLOAT_EQ(expected_msg.torque_w, result.torque_w);
  EXPECT_EQ(expected_msg.allocation_enum, result.allocation_enum);
  EXPECT_EQ(expected_msg.timeout, result.timeout);
}

TEST(VehicleAcoustickJoystickSerialization, deserialization_failures)
{
  // too short
  auto str = std::string{ "AJOY 00:02:36 1 1.0 0.5" };
  auto result = sentry_acomms::AcousticJoystickCommand{};

  EXPECT_FALSE(ds_acomms_serialization::deserialize(str, result));

  // Timeout in seconds.
  str = std::string{ "AJOY 600 1 1.0 0.5 0.2" };
  EXPECT_FALSE(ds_acomms_serialization::deserialize(str, result));

  // Timeout in HH:MM
  str = std::string{ "AJOY 02:36 1 1.0 0.5 0.2" };
  EXPECT_FALSE(ds_acomms_serialization::deserialize(str, result));
}
// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
