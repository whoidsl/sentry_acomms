/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_acomms/serialization.h"
#include "sentry_acomms/SentrySypridStatus.h"
#include "sentry_acomms/SypridCommand.h"
#include "test_util.h"

#include <gtest/gtest.h>

TEST(VehicleSypridSerialization, serialize_as_string)
{
  auto msg = sentry_acomms::SentrySypridStatus{};
  //const auto expected_str = std::string{ "MAIN:H9,T17.7,P17.9 BAT:H47,T3.7,P6.6" };
  const auto expected_str = std::string{ "PKZ 1 5 55.45 0.73 5 0 0 48.75 0 0 2 1 4 3" };

  msg.dcam_port_state = 1;
  msg.elmo_port_torque_desired = 5.0;
  msg.elmo_port_volts_latest = 55.45;
  msg.elmo_port_amps_active_latest = 0.73;
  msg.elmo_port_rpm_latest = 5.0;
  msg.dcam_stbd_state = 0;
  msg.elmo_stbd_torque_desired = 0.0;
  msg.elmo_stbd_volts_latest = 48.75;
  msg.elmo_stbd_amps_active_latest = 0.0;
  msg.elmo_stbd_rpm_latest = 0.0;
  msg.dcam_persistence_state = 2;
  msg.flow_sensor_state = 1;
  msg.flow_running_avg = 4;
  msg.flow_total_counts = 3;

  auto serialized_string = std::string{};
  ds_acomms_serialization::serialize(msg, serialized_string);

  ASSERT_TRUE(sentry_acomms::valid_avtrak_string(serialized_string));
  EXPECT_STREQ(expected_str.data(), serialized_string.data());
}

TEST(VehicleSypridSerialization, deserialize_from_string)
{
  auto expected_msg = sentry_acomms::SentrySypridStatus{};
  const auto str = std::string{ "PKZ 1 5.0 55.45 0.73 5.0 0 0.0 48.75 0.0 0.0 1 0 4 12" };

  expected_msg.dcam_port_state = 1;
  expected_msg.elmo_port_torque_desired = 5.0;
  expected_msg.elmo_port_volts_latest = 55.45;
  expected_msg.elmo_port_amps_active_latest = 0.73;
  expected_msg.elmo_port_rpm_latest = 5.0;
  expected_msg.dcam_stbd_state = 0;
  expected_msg.elmo_stbd_torque_desired = 0.0;
  expected_msg.elmo_stbd_volts_latest = 48.75;
  expected_msg.elmo_stbd_amps_active_latest = 0.0;
  expected_msg.elmo_stbd_rpm_latest = 0.0;
  
  expected_msg.dcam_persistence_state = 1;
  expected_msg.flow_sensor_state = 0;
  expected_msg.flow_running_avg = 4;
  expected_msg.flow_total_counts = 12;

  auto result = sentry_acomms::SentrySypridStatus{};

  ds_acomms_serialization::deserialize(str, result);

  EXPECT_FLOAT_EQ(expected_msg.dcam_port_state, result.dcam_port_state);
  EXPECT_FLOAT_EQ(expected_msg.elmo_port_torque_desired, result.elmo_port_torque_desired);
  EXPECT_FLOAT_EQ(expected_msg.elmo_port_volts_latest, result.elmo_port_volts_latest);
  EXPECT_FLOAT_EQ(expected_msg.elmo_port_amps_active_latest, result.elmo_port_amps_active_latest);
  EXPECT_FLOAT_EQ(expected_msg.elmo_port_rpm_latest, result.elmo_port_rpm_latest);

  EXPECT_FLOAT_EQ(expected_msg.dcam_stbd_state, result.dcam_stbd_state);
  EXPECT_FLOAT_EQ(expected_msg.elmo_stbd_torque_desired, result.elmo_stbd_torque_desired);
  EXPECT_FLOAT_EQ(expected_msg.elmo_stbd_volts_latest, result.elmo_stbd_volts_latest);
  EXPECT_FLOAT_EQ(expected_msg.elmo_stbd_amps_active_latest, result.elmo_stbd_amps_active_latest);
  EXPECT_FLOAT_EQ(expected_msg.elmo_stbd_rpm_latest, result.elmo_stbd_rpm_latest);

  EXPECT_FLOAT_EQ(expected_msg.dcam_persistence_state, result.dcam_persistence_state);
  EXPECT_FLOAT_EQ(expected_msg.flow_sensor_state, result.flow_sensor_state);
  EXPECT_FLOAT_EQ(expected_msg.flow_running_avg, result.flow_running_avg);
  EXPECT_FLOAT_EQ(expected_msg.flow_total_counts, result.flow_total_counts);
}

TEST(VehicleSypridSerialization, deserialize_from_string_failures)
{
  auto msg = sentry_acomms::SentrySypridStatus{};

  const auto str_missing_stbd_data = std::string{ "PKZ 1 5.0 55.45 0.73 5.0" };
  EXPECT_FALSE(ds_acomms_serialization::deserialize(str_missing_stbd_data, msg));
} 

TEST(VehicleSypridSerialization, deserialize_commands)
{
  auto msg = sentry_acomms::SypridCommand{};


  auto command_str = std::string{ "PKZ START ALL 3.2 2.1" };
  EXPECT_TRUE(ds_acomms_serialization::deserialize(command_str, msg));
  EXPECT_EQ(sentry_acomms::SypridCommand::PKZ_START_ALL, msg.pkz_cmd);
  EXPECT_NEAR(3.2, msg.torque_1, 0.1);
  EXPECT_NEAR(2.1, msg.torque_2, 0.1);
  
  
  command_str = std::string{ "PKZ TORQUE PORT 3.4" };
  EXPECT_TRUE(ds_acomms_serialization::deserialize(command_str, msg));
  EXPECT_EQ(sentry_acomms::SypridCommand::PKZ_TORQUE_PORT, 14);
  EXPECT_NEAR(3.4, msg.torque_1, 0.1);
  

  command_str = std::string{ "PKZ STOP ALL" };
  EXPECT_TRUE(ds_acomms_serialization::deserialize(command_str, msg));
  EXPECT_EQ(sentry_acomms::SypridCommand::PKZ_STOP_ALL, msg.pkz_cmd);
  
  command_str = std::string{ "PKZ STOP PORT" };
  EXPECT_TRUE(ds_acomms_serialization::deserialize(command_str, msg));
  EXPECT_EQ(sentry_acomms::SypridCommand::PKZ_STOP_PORT, msg.pkz_cmd);

  command_str = std::string{ "PKZ STOP STBD" };
  EXPECT_TRUE(ds_acomms_serialization::deserialize(command_str, msg));
  EXPECT_EQ(sentry_acomms::SypridCommand::PKZ_STOP_STBD, msg.pkz_cmd);

  command_str = std::string{ "PKZ OPEN STBD" };
  EXPECT_TRUE(ds_acomms_serialization::deserialize(command_str, msg));
  EXPECT_EQ(sentry_acomms::SypridCommand::PKZ_OPEN_DCAM_STBD, msg.pkz_cmd);

  command_str = std::string{ "PKZ OPEN PORT" };
  EXPECT_TRUE(ds_acomms_serialization::deserialize(command_str, msg));
  EXPECT_EQ(sentry_acomms::SypridCommand::PKZ_OPEN_DCAM_PORT, msg.pkz_cmd);

  command_str = std::string{ "PKZ CLOSE STBD" };
  EXPECT_TRUE(ds_acomms_serialization::deserialize(command_str, msg));
  EXPECT_EQ(sentry_acomms::SypridCommand::PKZ_CLOSE_DCAM_STBD, msg.pkz_cmd);

  command_str = std::string{ "PKZ CLOSE PORT" };
  EXPECT_TRUE(ds_acomms_serialization::deserialize(command_str, msg));
  EXPECT_EQ(sentry_acomms::SypridCommand::PKZ_CLOSE_DCAM_PORT, msg.pkz_cmd);

  // TORQUE
  command_str = std::string{ "PKZ TORQUE PORT 2.3" };
  EXPECT_TRUE(ds_acomms_serialization::deserialize(command_str, msg));
  EXPECT_EQ(sentry_acomms::SypridCommand::PKZ_TORQUE_PORT, msg.pkz_cmd);
  EXPECT_NEAR(2.3, msg.torque_1, 0.1);

  command_str = std::string{ "PKZ TORQUE STBD 3.5" };
  EXPECT_TRUE(ds_acomms_serialization::deserialize(command_str, msg));
  EXPECT_EQ(sentry_acomms::SypridCommand::PKZ_TORQUE_STBD, msg.pkz_cmd);
  EXPECT_NEAR(3.5, msg.torque_1, 0.1);

  // START PORT/STBD
  command_str = std::string{ "PKZ START PORT 2.7" };
  EXPECT_TRUE(ds_acomms_serialization::deserialize(command_str, msg));
  EXPECT_EQ(sentry_acomms::SypridCommand::PKZ_START_PORT, msg.pkz_cmd);
  EXPECT_NEAR(2.7, msg.torque_1, 0.1);

  command_str = std::string{ "PKZ START STBD 4.2" };
  EXPECT_TRUE(ds_acomms_serialization::deserialize(command_str, msg));
  EXPECT_EQ(sentry_acomms::SypridCommand::PKZ_START_STBD, msg.pkz_cmd);
  EXPECT_NEAR(4.2, msg.torque_1, 0.1);
  
  /////// DCAM PERSISTENCE
  command_str = std::string{ "PKZ ENABLE DCAM PERSISTENCE" };
  EXPECT_TRUE(ds_acomms_serialization::deserialize(command_str, msg));
  EXPECT_EQ(sentry_acomms::SypridCommand::PKZ_ENABLE_DCAM_PERSISTENCE, msg.pkz_cmd);
  
  command_str = std::string{ "PKZ DISABLE DCAM PERSISTENCE" };
  EXPECT_TRUE(ds_acomms_serialization::deserialize(command_str, msg));
  EXPECT_EQ(sentry_acomms::SypridCommand::PKZ_DISABLE_DCAM_PERSISTENCE, msg.pkz_cmd);
  
  /////// FLOW
  command_str = std::string{ "PKZ REQUEST FLOW SENSOR DATA" };
  EXPECT_TRUE(ds_acomms_serialization::deserialize(command_str, msg)); 
  EXPECT_EQ(sentry_acomms::SypridCommand::PKZ_REQUEST_FLOW_SENSOR_DATA, msg.pkz_cmd);
} 

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
