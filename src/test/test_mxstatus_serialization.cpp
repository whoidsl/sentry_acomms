/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 10/1/19.
//

#include "sentry_acomms/serialization.h"
#include "sentry_acomms/SentryMxStatus.h"
#include "boost/date_time/posix_time/posix_time.hpp"

#include <gtest/gtest.h>

TEST(SentryMxStatusSerialization, serialize_as_string)
{
  sentry_acomms::SentryMxStatus msg;
  for (size_t i=0; i<16; i++) {
    msg.primitive_uuid[i] = i;
  }
  msg.next_timeout = ros::Time::now();

  auto os = std::ostringstream{};
  auto facet = new boost::posix_time::time_facet("%H:%M:%S");
  os.imbue(std::locale(os.getloc(), facet));
  os << msg.next_timeout.toBoost() << " " <<"000102030405060708090a0b0c0d0e0f";

  std::string serialized_str;

  ds_acomms_serialization::serialize(msg, serialized_str);
  EXPECT_EQ(os.str(), serialized_str);
}

TEST(SentryMxStatusSerialization, deserialize_as_string)
{
  sentry_acomms::SentryMxStatus expected_msg;
  for (size_t i=0; i<16; i++) {
    expected_msg.primitive_uuid[i] = i;
  }
  expected_msg.next_timeout = ros::Time::now() + ros::Duration(10);
  auto os = std::ostringstream{};
  auto facet = new boost::posix_time::time_facet("%H:%M:%S");
  os.imbue(std::locale(os.getloc(), facet));
  os << expected_msg.next_timeout.toBoost() << " " <<"000102030405060708090a0b0c0d0e0f";

  sentry_acomms::SentryMxStatus result;

  EXPECT_TRUE(ds_acomms_serialization::deserialize(os.str(), result));

  EXPECT_NEAR(expected_msg.next_timeout.toSec(), result.next_timeout.toSec(), 1.0);
  for (size_t i=0; i<expected_msg.primitive_uuid.size(); i++) {
    EXPECT_EQ(expected_msg.primitive_uuid[i], result.primitive_uuid[i]);
  }
}

TEST(SentryMxStatusSerialization, deserialize_with_wrap)
{
  sentry_acomms::SentryMxStatus expected_msg;
  for (size_t i=0; i<16; i++) {
    expected_msg.primitive_uuid[i] = i;
  }
  // fast-forward 23 hours
  expected_msg.next_timeout = ros::Time::now() + ros::Duration(23*3600);
  auto os = std::ostringstream{};
  auto facet = new boost::posix_time::time_facet("%H:%M:%S");
  os.imbue(std::locale(os.getloc(), facet));
  os << expected_msg.next_timeout.toBoost() << " " <<"000102030405060708090a0b0c0d0e0f";

  sentry_acomms::SentryMxStatus result;

  EXPECT_TRUE(ds_acomms_serialization::deserialize(os.str(), result));

  EXPECT_NEAR(expected_msg.next_timeout.toSec(), result.next_timeout.toSec(), 1.0);
  for (size_t i=0; i<expected_msg.primitive_uuid.size(); i++) {
    EXPECT_EQ(expected_msg.primitive_uuid[i], result.primitive_uuid[i]);
  }
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  ros::Time::init();
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
