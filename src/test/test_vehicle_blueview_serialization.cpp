/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_acomms/serialization.h"
#include "sentry_acomms/SentryBlueviewStatus.h"
#include "test_util.h"

#include <gtest/gtest.h>

TEST(VehicleBlueviewSerialization, serialize_as_string)
{
  auto msg = sentry_acomms::SentryBlueviewStatus{};
  auto expected_str = std::string{ "1234567 SONAR_OK 0 1 0" };

  msg.ok = true;
  msg.time = 1234567.0f;
  msg.ret = 0;
  msg.enabled = true;
  msg.active = false;

  auto serialized_string = std::string{};
  ds_acomms_serialization::serialize(msg, serialized_string);
  ASSERT_TRUE(sentry_acomms::valid_avtrak_string(serialized_string));
  EXPECT_STREQ(expected_str.data(), serialized_string.data());

  msg = sentry_acomms::SentryBlueviewStatus{};
  expected_str = std::string{ "1234567 SONAR_BAD 5" };

  msg.ok = false;
  msg.time = 1234567.89;
  msg.ret = 5;

  serialized_string.clear();
  ds_acomms_serialization::serialize(msg, serialized_string);
}

TEST(VehicleBlueviewSerialization, deserialize_from_string)
{
  auto expected_msg = sentry_acomms::SentryBlueviewStatus{};
  auto str = std::string{ "1234567 SONAR_OK 0 1 0" };

  expected_msg.ok = true;
  expected_msg.time = 1234567.0f;
  expected_msg.ret = 0;
  expected_msg.enabled = true;
  expected_msg.active = false;

  auto result = sentry_acomms::SentryBlueviewStatus{};

  EXPECT_TRUE(ds_acomms_serialization::deserialize(str, result));

  EXPECT_EQ(expected_msg.ok, result.ok);
  EXPECT_FLOAT_EQ(expected_msg.time, result.time);
  EXPECT_EQ(expected_msg.ret, result.ret);
  EXPECT_EQ(expected_msg.enabled, result.enabled);
  EXPECT_EQ(expected_msg.active, result.active);

  expected_msg = sentry_acomms::SentryBlueviewStatus{};
  str = std::string{ "1234567 SONAR_BAD 10" };

  expected_msg.ok = false;
  expected_msg.time = 1234567.0f;
  expected_msg.ret = 10;

  result = sentry_acomms::SentryBlueviewStatus{};

  EXPECT_TRUE(ds_acomms_serialization::deserialize(str, result));

  EXPECT_EQ(expected_msg.ok, result.ok);
  EXPECT_FLOAT_EQ(expected_msg.time, result.time);
  EXPECT_EQ(expected_msg.ret, result.ret);
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
