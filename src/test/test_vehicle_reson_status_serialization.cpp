/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_acomms/serialization.h"
#include "sentry_acomms/SentryResonStatus.h"
#include "test_util.h"

#include <gtest/gtest.h>

TEST(VehicleResonStatusSerialization, serialize_as_string)
{
  const auto expected_str = std::string{ "RSTAT REC1,S=NCOM,CMD=-1,0,#523412,AP0,GAIN212.0,RNG180.0,PWR218.0,CA120.0" };

  auto msg = sentry_acomms::SentryResonStatus{};
  auto parsed_status = std::array<char, 10>{ "NCOM\0\0\0\0\0" };
  msg.recording = true;
  msg.ping_number = 523412;
  msg.status = std::string{ parsed_status.data() };
  msg.mode = -1;
  msg.gain = 212.0f;
  msg.power = 218.0f;
  msg.range = 180.0f;
  msg.coverage_angle_deg = 120.0f;
  msg.auto_pilot = 0;
  auto serialized_string = std::string{};
  ds_acomms_serialization::serialize(msg, serialized_string);

  EXPECT_TRUE(sentry_acomms::valid_avtrak_string(serialized_string));
  EXPECT_STREQ(expected_str.data(), serialized_string.data());
}

TEST(VehicleResonStatusSerialization, deserialize_from_string)
{
  const auto str = std::string{ "RSTAT REC1,S=NCOM,CMD=-1,0,#523412,AP0,GAIN212.0,RNG180.0,PWR218.0,CA120.0" };

  auto expected_msg = sentry_acomms::SentryResonStatus{};
  expected_msg.recording = true;
  expected_msg.ping_number = 523412;
  expected_msg.status = "NCOM";
  expected_msg.auto_pilot = 0;
  expected_msg.mode = -1;
  expected_msg.gain = 212.0f;
  expected_msg.power = 218.0f;
  expected_msg.range = 180.0f;
  expected_msg.coverage_angle_deg = 120.0f;

  auto result = sentry_acomms::SentryResonStatus{};

  EXPECT_TRUE(ds_acomms_serialization::deserialize(str, result));

  EXPECT_EQ(expected_msg.recording, result.recording);
  EXPECT_EQ(expected_msg.ping_number, result.ping_number);
  EXPECT_EQ(expected_msg.mode, result.mode);
  EXPECT_EQ(expected_msg.auto_pilot, result.auto_pilot);
  EXPECT_STRCASEEQ(expected_msg.status.data(), result.status.data());
  EXPECT_FLOAT_EQ(expected_msg.gain, result.gain);
  EXPECT_FLOAT_EQ(expected_msg.power, result.power);
  EXPECT_FLOAT_EQ(expected_msg.range, result.range);
  EXPECT_FLOAT_EQ(expected_msg.coverage_angle_deg, result.coverage_angle_deg);
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
