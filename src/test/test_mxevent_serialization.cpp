/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 10/1/19.
//

#include "sentry_acomms/serialization.h"
#include "sentry_acomms/SentryMxEvent.h"

#include <gtest/gtest.h>

TEST(SentryMxEventSerialization, serialize_as_string)
{
  std::string expected_str("MXEV /fault/something/broke");
  sentry_acomms::SentryMxEvent msg;
  msg.event = "/fault/something/broke";
  std::string serialized_str;

  ds_acomms_serialization::serialize(msg, serialized_str);
  EXPECT_EQ(expected_str, serialized_str);
}

TEST(SentryMxEventSerialization, deserialize_as_string)
{
  std::string str("MXEV /fault/something/broke");
  sentry_acomms::SentryMxEvent expected_msg;
  expected_msg.event = "/fault/something/broke";

  sentry_acomms::SentryMxEvent result;
  EXPECT_TRUE(ds_acomms_serialization::deserialize(str, result));

  EXPECT_EQ(expected_msg.event, result.event);
}

TEST(SentryMxEventSerialization, deserialize_empty)
{
  std::string str("MXEV");
  sentry_acomms::SentryMxEvent expected_msg;
  expected_msg.event = "/fault/something/broke";

  sentry_acomms::SentryMxEvent result;
  EXPECT_FALSE(ds_acomms_serialization::deserialize(str,result));
}

TEST(SentryMxEventSerialization, deserialize_bad)
{
  std::string str("MXE");
  sentry_acomms::SentryMxEvent expected_msg;
  expected_msg.event = "/fault/something/broke";

  sentry_acomms::SentryMxEvent result;
  EXPECT_FALSE(ds_acomms_serialization::deserialize(str, result));
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  ros::Time::init();
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
