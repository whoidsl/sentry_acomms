/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_acomms/serialization.h"
#include "sentry_acomms/NavaggCommand.h"

#include <gtest/gtest.h>

TEST(VehicleNavaggSerialization, serialize_as_string)
{
  auto expected_str = std::string{ "NAG 0 0 0 0 0" };

  auto msg = sentry_acomms::NavaggCommand{};
  msg.enum_xy = 0;
  msg.enum_heading = 0;
  msg.enum_pitchroll = 0;
  msg.enum_depth = 0;
  msg.enum_sync = 0;

  auto serialized_string = std::string{};
  ds_acomms_serialization::serialize(msg, serialized_string);

  EXPECT_STREQ(expected_str.data(), serialized_string.data());
}

TEST(VehicleNavaggSerialization, deserialize_from_string)
{
  const auto str = std::string{ "NAG 0 0 0 0 0" };

  auto expected_msg = sentry_acomms::NavaggCommand{};
  expected_msg.enum_xy = 0;
  expected_msg.enum_heading = 0;
  expected_msg.enum_pitchroll = 0;
  expected_msg.enum_depth = 0;
  expected_msg.enum_sync = 0;

  auto result = sentry_acomms::NavaggCommand{};

  EXPECT_TRUE(ds_acomms_serialization::deserialize(str, result));

  EXPECT_FLOAT_EQ(expected_msg.enum_xy, result.enum_xy);
  EXPECT_FLOAT_EQ(expected_msg.enum_heading, result.enum_heading);
  EXPECT_FLOAT_EQ(expected_msg.enum_pitchroll, result.enum_pitchroll);
  EXPECT_EQ(expected_msg.enum_depth, result.enum_depth);
  EXPECT_EQ(expected_msg.enum_sync, result.enum_sync);
}

TEST(VehicleNavaggSerialization, deserialization_failures)
{
  // too short
  auto str = std::string{ "NAG 0 0"};
  auto result = sentry_acomms::NavaggCommand{};

  EXPECT_FALSE(ds_acomms_serialization::deserialize(str, result));

}
// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
