/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_acomms/serialization.h"
#include "sentry_acomms/SentryGoalLegState.h"
#include "test_util.h"

#include <gtest/gtest.h>

TEST(VehicleLegGoalSerialization, serialize_as_string)
{
  const auto expected_str = std::string{ "1.00 2.00 3.00 4.00 5.00 6.00 7.00 8.00 9.00 10.00 11" };

  auto msg = sentry_acomms::SentryGoalLegState{};
  msg.line_start.x = 1;
  msg.line_start.y = 2;
  msg.line_end.x = 3;
  msg.line_end.y = 4;
  msg.angle_line_segment = 5;
  msg.off_line_vect = 6;
  msg.sign_of_vect = 7;
  msg.kappa = 8;
  msg.old_goal = 9;
  msg.new_goal = 10;
  msg.leg_number = 11;

  auto serialized_string = std::string{};
  ds_acomms_serialization::serialize(msg, serialized_string);

  ASSERT_TRUE(sentry_acomms::valid_avtrak_string(serialized_string));
  EXPECT_STREQ(expected_str.data(), serialized_string.data());
}

TEST(VehicleLegGoalSerialization, deserialize_from_string)
{
  const auto str = std::string{ "1.00 2.00 3.00 4.00 5.00 6.00 7.00 8.00 9.00 10.00 11" };

  auto expected_msg = sentry_acomms::SentryGoalLegState{};
  expected_msg.line_start.x = 1;
  expected_msg.line_start.y = 2;
  expected_msg.line_end.x = 3;
  expected_msg.line_end.y = 4;
  expected_msg.angle_line_segment = 5;
  expected_msg.off_line_vect = 6;
  expected_msg.sign_of_vect = 7;
  expected_msg.kappa = 8;
  expected_msg.old_goal = 9;
  expected_msg.new_goal = 10;
  expected_msg.leg_number = 11;

  auto result = sentry_acomms::SentryGoalLegState{};

  EXPECT_TRUE(ds_acomms_serialization::deserialize(str, result));

  EXPECT_FLOAT_EQ(expected_msg.line_start.x, result.line_start.x);
  EXPECT_FLOAT_EQ(expected_msg.line_start.y, result.line_start.y);
  EXPECT_FLOAT_EQ(expected_msg.line_end.x, result.line_end.x);
  EXPECT_FLOAT_EQ(expected_msg.line_end.y, result.line_end.y);
  EXPECT_FLOAT_EQ(expected_msg.angle_line_segment, result.angle_line_segment);
  EXPECT_FLOAT_EQ(expected_msg.off_line_vect, result.off_line_vect);
  EXPECT_FLOAT_EQ(expected_msg.sign_of_vect, result.sign_of_vect);
  EXPECT_FLOAT_EQ(expected_msg.kappa, result.kappa);
  EXPECT_FLOAT_EQ(expected_msg.old_goal, result.old_goal);
  EXPECT_FLOAT_EQ(expected_msg.new_goal, result.new_goal);
  EXPECT_EQ(expected_msg.leg_number, result.leg_number);
}
// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
