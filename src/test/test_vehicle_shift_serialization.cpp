/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_acomms/serialization.h"
#include "sentry_acomms/SentryCurrentShift.h"

#include <boost/date_time.hpp>
#include <gtest/gtest.h>

TEST(SentryCurrentShift, serialize_string)
{
  auto expected = sentry_acomms::SentryCurrentShift{};
  expected.x = 5;
  expected.y = 50;

  expected.stamp = ros::Time::now();

  auto os = std::ostringstream{};
  auto facet = new boost::posix_time::time_facet("%H:%M");
  os.imbue(std::locale(os.getloc(), facet));
  os << expected.stamp.toBoost() << " " << std::setprecision(0) << std::fixed << expected.x << " " << expected.y;

  const auto expected_string = os.str();
  auto result = std::string{};
  ds_acomms_serialization::serialize(expected, result);
  EXPECT_STREQ(expected_string.data(), result.data());
}

TEST(SentryCurrentShift, deserialize_string)
{
  auto expected = sentry_acomms::SentryCurrentShift{};
  expected.x = 5;
  expected.y = 50;

  expected.stamp = ros::Time::now();
  auto os = std::ostringstream{};
  auto facet = new boost::posix_time::time_facet("%H:%M");
  os.imbue(std::locale(os.getloc(), facet));
  os << expected.stamp.toBoost() << " " << std::setprecision(0) << std::fixed << expected.x << " " << expected.y;

  auto buf = os.str();
  auto result = sentry_acomms::SentryCurrentShift{};
  EXPECT_TRUE(ds_acomms_serialization::deserialize(buf, result));
  EXPECT_EQ(expected.x, result.x);
  EXPECT_EQ(expected.y, result.y);
  EXPECT_NEAR(expected.stamp.toSec(), result.stamp.toSec(), 60);
}

TEST(LastReceivedMessage, deserialize_string_with_timestamp_wrap)
{
  auto expected = sentry_acomms::SentryCurrentShift{};
  expected.x = 5;
  expected.y = 50;

  // rewind the clock 23 hours
  expected.stamp = ros::Time::now() - ros::Duration(23 * 3600);
  auto os = std::ostringstream{};
  auto facet = new boost::posix_time::time_facet("%H:%M");
  os.imbue(std::locale(os.getloc(), facet));
  os << expected.stamp.toBoost() << " " << std::setprecision(0) << expected.x << " " << expected.y;

  const auto buf = os.str();
  auto result = sentry_acomms::SentryCurrentShift{};
  EXPECT_TRUE(ds_acomms_serialization::deserialize(buf, result));
  EXPECT_EQ(expected.x, result.x);
  EXPECT_EQ(expected.y, result.y);
  EXPECT_NEAR(expected.stamp.toSec(), result.stamp.toSec(), 60);
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  ros::Time::init();
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
