/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_acomms/serialization.h"
#include "sentry_acomms/SentryHtp.h"
#include "test_util.h"

#include <gtest/gtest.h>

TEST(VehicleHtpSerialization, serialize_as_string)
{
  auto msg = sentry_acomms::SentryHtp{};
  const auto expected_str = std::string{ "MAIN:H9,T17.7,P17.9 BAT:H47,T3.7,P6.6" };

  msg.main_humidity = 9;
  msg.main_temperature = 17.7;
  msg.main_pressure = 17.9;

  msg.battery_humidity = 47;
  msg.battery_temperature = 3.7;
  msg.battery_pressure = 6.6;

  auto serialized_string = std::string{};
  ds_acomms_serialization::serialize(msg, serialized_string);

  ASSERT_TRUE(sentry_acomms::valid_avtrak_string(serialized_string));
  EXPECT_STREQ(expected_str.data(), serialized_string.data());
}

TEST(VehicleHtpSerialization, deserialize_from_string)
{
  auto expected_msg = sentry_acomms::SentryHtp{};
  const auto str = std::string{ "MAIN:H9,T17.7,P17.9 BAT:H47,T3.7,P6.6" };

  expected_msg.main_humidity = 9;
  expected_msg.main_temperature = 17.7;
  expected_msg.main_pressure = 17.9;

  expected_msg.battery_humidity = 47;
  expected_msg.battery_temperature = 3.7;
  expected_msg.battery_pressure = 6.6;

  auto result = sentry_acomms::SentryHtp{};

  ds_acomms_serialization::deserialize(str, result);

  EXPECT_FLOAT_EQ(expected_msg.main_humidity, result.main_humidity);
  EXPECT_FLOAT_EQ(expected_msg.main_temperature, result.main_temperature);
  EXPECT_FLOAT_EQ(expected_msg.main_pressure, result.main_pressure);

  EXPECT_FLOAT_EQ(expected_msg.battery_humidity, result.battery_humidity);
  EXPECT_FLOAT_EQ(expected_msg.battery_temperature, result.battery_temperature);
  EXPECT_FLOAT_EQ(expected_msg.battery_pressure, result.battery_pressure);
}

TEST(VehicleHtpSerialization, deserialize_from_string_failures)
{
  auto msg = sentry_acomms::SentryHtp{};

  const auto str_missing_battery_data = std::string{ "MAIN:H9,T17.7,P17.9" };
  EXPECT_FALSE(ds_acomms_serialization::deserialize(str_missing_battery_data, msg));
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
