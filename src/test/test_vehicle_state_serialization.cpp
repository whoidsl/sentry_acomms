/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_acomms/serialization.h"
#include "sentry_acomms/SentryVehicleState.h"

#include <gtest/gtest.h>

TEST(VehicleStateSerialization, serialize_as_string)
{
  auto msg = sentry_acomms::SentryVehicleState{};
  const auto expected_str = std::string{ "9904,6041,2508,140.2,4.2,0.97,-0.10,A0,0,93.1,9904,6089,0,0,0,0,0,0,0,0,12" };

  msg.pos_ned.x = 9904;
  msg.pos_ned.y = 6041;
  msg.pos_ned.z = 2508;
  msg.altitude = 140.2;
  msg.heading = 4.2;
  msg.horz_velocity = 0.97;
  msg.vert_velocity = -0.10;
  msg.abort_status = false;
  msg.battery_pct = 93.1;
  msg.goal_ned.x = 9904;
  msg.goal_ned.y = 6089;
  msg.goal_ned.z = 0;
  msg.trackline = 12;

  auto serialized_string = std::string{};
  ds_acomms_serialization::serialize(msg, serialized_string);

  EXPECT_STREQ(expected_str.data(), serialized_string.data());
}

TEST(VehicleStateSerialization, deserialize_from_string)
{
  auto expected_msg = sentry_acomms::SentryVehicleState{};
  const auto str = std::string{ "9904,6041,2508,140.2,4.2,0.97,-0.10,A0,0,93.1,9904,6089,0,0,0,0,0,0,0,0,12" };

  expected_msg.pos_ned.x = 9904;
  expected_msg.pos_ned.y = 6041;
  expected_msg.pos_ned.z = 2508;
  expected_msg.altitude = 140.2;
  expected_msg.heading = 4.2;
  expected_msg.horz_velocity = 0.97;
  expected_msg.vert_velocity = -0.10;
  expected_msg.abort_status = false;
  expected_msg.battery_pct = 93.1;
  expected_msg.goal_ned.x = 9904;
  expected_msg.goal_ned.y = 6089;
  expected_msg.goal_ned.z = 0;
  expected_msg.trackline = 12;

  auto result = sentry_acomms::SentryVehicleState{};

  ds_acomms_serialization::deserialize(str, result);

  EXPECT_FLOAT_EQ(expected_msg.pos_ned.x, result.pos_ned.x);
  EXPECT_FLOAT_EQ(expected_msg.pos_ned.y, result.pos_ned.y);
  EXPECT_FLOAT_EQ(expected_msg.pos_ned.z, result.pos_ned.z);
  EXPECT_FLOAT_EQ(expected_msg.altitude, result.altitude);
  EXPECT_FLOAT_EQ(expected_msg.heading, result.heading);
  EXPECT_FLOAT_EQ(expected_msg.horz_velocity, result.horz_velocity);
  EXPECT_FLOAT_EQ(expected_msg.vert_velocity, result.vert_velocity);
  EXPECT_EQ(expected_msg.abort_status, result.abort_status);
  EXPECT_FLOAT_EQ(expected_msg.battery_pct, result.battery_pct);
  EXPECT_FLOAT_EQ(expected_msg.goal_ned.x, result.goal_ned.x);
  EXPECT_FLOAT_EQ(expected_msg.goal_ned.y, result.goal_ned.y);
  EXPECT_FLOAT_EQ(expected_msg.goal_ned.z, result.goal_ned.z);
  EXPECT_EQ(expected_msg.trackline, result.trackline);
}

TEST(VehicleStateSerialization, deserialize_from_string_failures)
{
  auto msg = sentry_acomms::SentryVehicleState{};

  const auto str_missing_abort_label =
      std::string{ "9904,6041,2508,140.2,4.2,0.97,-0.10,0,0,93.1,9904,6089,0,0,0,0,0,0,0,0,12" };
  EXPECT_FALSE(ds_acomms_serialization::deserialize(str_missing_abort_label, msg));

  const auto str_short = std::string{ "9904,6041,2508,140.2,4.2,0.97" };
  EXPECT_FALSE(ds_acomms_serialization::deserialize(str_short, msg));
}
// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
