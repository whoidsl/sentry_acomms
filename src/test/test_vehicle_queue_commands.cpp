/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_acomms/serialization.h"
#include "sentry_acomms/SentryQueueManagerCommandEnum.h"

#include <gtest/gtest.h>

using namespace sentry_acomms;

TEST(VehicleQueueCommands, valid_commands)
{
  EXPECT_EQ(SentryQueueManagerCommandEnum::MISSION_CONTROLLER_COMMAND, queue_message_type("MCA"));

  EXPECT_EQ(SentryQueueManagerCommandEnum::RESON_COMMAND, queue_message_type("RESONMSG"));

  EXPECT_EQ(SentryQueueManagerCommandEnum::BLUEVIEW_COMMAND, queue_message_type("BLV"));

  EXPECT_EQ(SentryQueueManagerCommandEnum::PHINS_COMMAND, queue_message_type("PHI"));

  EXPECT_EQ(SentryQueueManagerCommandEnum::BOTTOM_FOLLOWER_PARAM_COMMAND, queue_message_type("WBFP"));

  EXPECT_EQ(SentryQueueManagerCommandEnum::BOTTOM_FOLLOWER_OVERRIDE_COMMAND, queue_message_type("BF_OVERRIDE"));

  EXPECT_EQ(SentryQueueManagerCommandEnum::ACOUSTIC_JOYSTICK_COMMAND, queue_message_type("AJOY"));

  EXPECT_EQ(SentryQueueManagerCommandEnum::THRUSTERS_ENABLED_COMMAND, queue_message_type("THRUSTERS_ENABLED"));

  EXPECT_EQ(SentryQueueManagerCommandEnum::KONGSBERG_EM2040_PARAM_COMMAND, queue_message_type("KM_PARAM"));

  EXPECT_EQ(SentryQueueManagerCommandEnum::SYPRID_COMMAND, queue_message_type("PKZ"));

  EXPECT_EQ(SentryQueueManagerCommandEnum::NAVAGG_COMMAND, queue_message_type("NAG"));
}

TEST(VehicleQueueCommands, invalid_commands)
{
  EXPECT_EQ(ds_acomms_queue_manager::QueueManagerMessageEnum::INVALID_MESSAGE_TYPE,
            queue_message_type("SOMEBADCOMMANDNAME"));
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
