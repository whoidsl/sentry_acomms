/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

//
// Created by jvaccaro on 7/22/19.
//

#include "sentry_acomms/serialization.h"
#include "sentry_acomms/SentryKongsbergStatus.h"
#include "sentry_acomms/KongsbergParamCommand.h"
#include "sentry_acomms/KongsbergPingCommand.h"
#include "sentry_acomms/KongsbergKctrlCommand.h"
#include "test_util.h"
#include <gtest/gtest.h>

TEST(VehicleKongsbergSerialization, serialize_as_string)
{
  auto msg = sentry_acomms::SentryKongsbergStatus{};
  const auto expected_str = std::string{ "S111 D65 20 150 2 101 Deep T1 160 200 01 #15057 877 56 82 64 " };

  msg.pu_good = 1;
  msg.sensors_good = 1;
  msg.pinging = 1;
  // Dep
  msg.rt_force_depth = 65;
  msg.rt_min_depth = 20;
  msg.rt_max_depth = 150;
  msg.rt_ping_freq = 2;
  msg.rt_fm_disable = 1;
  msg.rt_log_watercol = 0;
  msg.rt_extra_detect = 1;
  msg.rt_depth_mode = "Deep";
  // Txmit
  msg.rt_tx_angle_along = 1;
  msg.rt_max_ping_rate = 160;
  msg.rt_min_swath_distance = 200;
  msg.rt_pitch_stab = 0;
  msg.rt_trigger = 1;
  // Latest ping
  msg.ping_num = 15057;
  msg.num_soundings = 877;
  msg.percent_good = 56;
  msg.max_depth = 82;
  msg.min_depth = 64;

  auto serialized_string = std::string{};
  ds_acomms_serialization::serialize(msg, serialized_string);
  ASSERT_TRUE(sentry_acomms::valid_avtrak_string(expected_str));
  ASSERT_TRUE(sentry_acomms::valid_avtrak_string(serialized_string));
  EXPECT_STREQ(expected_str.data(), serialized_string.data());
}

TEST(VehicleKongsbergSerialization, deserialize_from_string)
{
  auto expected_msg = sentry_acomms::SentryKongsbergStatus{};
  const auto str = std::string{ "S111 D65 20 150 2 101 Deep T1 160 200 01 #15057 877 56 82 64 " };

  expected_msg.pu_good = 1;
  expected_msg.sensors_good = 1;
  expected_msg.pinging = 1;
  // Dep
  expected_msg.rt_force_depth = 65;
  expected_msg.rt_min_depth = 20;
  expected_msg.rt_max_depth = 150;
  expected_msg.rt_ping_freq = 2;
  expected_msg.rt_fm_disable = 1;
  expected_msg.rt_log_watercol = 0;
  expected_msg.rt_extra_detect = 1;
  expected_msg.rt_depth_mode = "Deep";
  // Txmit
  expected_msg.rt_tx_angle_along = 1;
  expected_msg.rt_max_ping_rate = 160;
  expected_msg.rt_min_swath_distance = 200;
  expected_msg.rt_pitch_stab = 0;
  expected_msg.rt_trigger = 1;
  // Latest ping
  expected_msg.ping_num = 15057;
  expected_msg.num_soundings = 877;
  expected_msg.percent_good = 56;
  expected_msg.max_depth = 82;
  expected_msg.min_depth = 64;

  auto result = sentry_acomms::SentryKongsbergStatus{};

  ds_acomms_serialization::deserialize(str, result);

  EXPECT_FLOAT_EQ(expected_msg.pu_good, result.pu_good);
  EXPECT_FLOAT_EQ(expected_msg.sensors_good, result.sensors_good);
  EXPECT_FLOAT_EQ(expected_msg.pinging, result.pinging);
  EXPECT_FLOAT_EQ(expected_msg.ping_num, result.ping_num);
  EXPECT_FLOAT_EQ(expected_msg.num_soundings, result.num_soundings);
  EXPECT_FLOAT_EQ(expected_msg.percent_good, result.percent_good);
  EXPECT_FLOAT_EQ(expected_msg.max_depth, result.max_depth);
  EXPECT_FLOAT_EQ(expected_msg.min_depth, result.min_depth);
}

TEST(VehicleKongsbergSerialization, deserialize_from_string_failures)
{
  auto msg = sentry_acomms::SentryKongsbergStatus{};

  const auto str_missing_status = std::string{ " D65 20 150 2 101 T1 160 200 01 #15057 877 56 82 64 " };
  EXPECT_FALSE(ds_acomms_serialization::deserialize(str_missing_status, msg));
}

TEST(KongsbergParamSerialization, serialize_from_msg)
{
  auto msg = sentry_acomms::KongsbergParamCommand{};
  const auto expected_string = std::string{"KM_PARAM PARAM_1 Hello!"};
  msg.param_name = "PARAM_1";
  msg.param_value = "Hello!";
  std::string serialized_string;
  ds_acomms_serialization::serialize(msg, serialized_string);
  ASSERT_TRUE(sentry_acomms::valid_avtrak_string(serialized_string));
  EXPECT_STREQ(expected_string.data(), serialized_string.data());
}

TEST(KongsbergParamSerialization, serialize_from_msg_multiword)
{
  auto msg = sentry_acomms::KongsbergParamCommand{};
  const auto expected_string = std::string{"KM_PARAM PARAM_1 \"Hello, how are you today?\""};
  msg.param_name = "PARAM_1";
  msg.param_value = "\"Hello, how are you today?\"";
  std::string serialized_string;
  ds_acomms_serialization::serialize(msg, serialized_string);
  ASSERT_TRUE(sentry_acomms::valid_avtrak_string(serialized_string));
  EXPECT_STREQ(expected_string.data(), serialized_string.data());
}

TEST(KongsbergParamSerialization, deserialize_from_string)
{
  auto expected_msg = sentry_acomms::KongsbergParamCommand{};
  const auto string = std::string{"KM_PARAM PARAM_1 Hello!"};
  expected_msg.param_name = "PARAM_1";
  expected_msg.param_value = "Hello!";
  auto result_msg = sentry_acomms::KongsbergParamCommand{};
  EXPECT_TRUE(ds_acomms_serialization::deserialize(string, result_msg));
  EXPECT_STREQ(result_msg.param_name.data(), expected_msg.param_name.data());
  EXPECT_STREQ(result_msg.param_value.data(), expected_msg.param_value.data());
}

TEST(KongsbergParamSerialization, deserialize_from_string_multiword)
{
  auto expected_msg = sentry_acomms::KongsbergParamCommand{};
  const auto string = std::string{"KM_PARAM PARAM_1 \"Hello, how are you today?\""};
  expected_msg.param_name = "PARAM_1";
  expected_msg.param_value = "\"Hello, how are you today?\"";
  auto result_msg = sentry_acomms::KongsbergParamCommand{};
  EXPECT_TRUE(ds_acomms_serialization::deserialize(string, result_msg));
  EXPECT_STREQ(result_msg.param_name.data(), expected_msg.param_name.data());
  EXPECT_STREQ(result_msg.param_value.data(), expected_msg.param_value.data());
}

TEST(KongsbergPingSerialization, deserialize_from_string_1)
{
  auto expected_msg = sentry_acomms::KongsbergPingCommand{};
  const auto string = std::string{"KM_PING 1"};
  expected_msg.ping = 1;
  auto result_msg = sentry_acomms::KongsbergPingCommand{};
  EXPECT_TRUE(ds_acomms_serialization::deserialize(string, result_msg));
  EXPECT_EQ(result_msg.ping, expected_msg.ping);
}
TEST(KongsbergPingSerialization, deserialize_from_string_2)
{
  auto expected_msg = sentry_acomms::KongsbergPingCommand{};
  const auto string = std::string{"KM_PING 2"};
  expected_msg.ping = 2;
  auto result_msg = sentry_acomms::KongsbergPingCommand{};
  EXPECT_TRUE(ds_acomms_serialization::deserialize(string, result_msg));
  EXPECT_EQ(result_msg.ping, expected_msg.ping);
}
TEST(KongsbergPingSerialization, deserialize_from_string_3)
{
  auto expected_msg = sentry_acomms::KongsbergPingCommand{};
  const auto string = std::string{"KM_PING 3"};
  expected_msg.ping = 3;
  auto result_msg = sentry_acomms::KongsbergPingCommand{};
  EXPECT_TRUE(ds_acomms_serialization::deserialize(string, result_msg));
  EXPECT_EQ(result_msg.ping, expected_msg.ping);
}

TEST(KongsbergKctrlSerialization, deserialize_from_string_1)
{
  auto expected_msg = sentry_acomms::KongsbergKctrlCommand{};
  const auto string = std::string{"KM_KCTRL 1"};
  expected_msg.kctrl = 1;
  auto result_msg = sentry_acomms::KongsbergKctrlCommand{};
  EXPECT_TRUE(ds_acomms_serialization::deserialize(string, result_msg));
  EXPECT_EQ(result_msg.kctrl, expected_msg.kctrl);
}
TEST(KongsbergKctrlSerialization, deserialize_from_string_2)
{
  auto expected_msg = sentry_acomms::KongsbergKctrlCommand{};
  const auto string = std::string{"KM_KCTRL 2"};
  expected_msg.kctrl = 2;
  auto result_msg = sentry_acomms::KongsbergKctrlCommand{};
  EXPECT_TRUE(ds_acomms_serialization::deserialize(string, result_msg));
  EXPECT_EQ(result_msg.kctrl, expected_msg.kctrl);
}
TEST(KongsbergKctrlSerialization, deserialize_from_string_3)
{
  auto expected_msg = sentry_acomms::KongsbergKctrlCommand{};
  const auto string = std::string{"KM_KCTRL 3"};
  expected_msg.kctrl = 3;
  auto result_msg = sentry_acomms::KongsbergKctrlCommand{};
  EXPECT_TRUE(ds_acomms_serialization::deserialize(string, result_msg));
  EXPECT_EQ(result_msg.kctrl, expected_msg.kctrl);
}
// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
