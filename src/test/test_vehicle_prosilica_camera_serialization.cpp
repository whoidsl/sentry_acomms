/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_acomms/serialization.h"
#include "sentry_acomms/SentryProsilicaCameraStatus.h"
#include "test_util.h"

#include <gtest/gtest.h>

TEST(VehicleProsilicaCameraSerialization, serialize_as_string)
{
  auto msg = sentry_acomms::SentryProsilicaCameraStatus{};
  const auto expected_str = std::string{ "P0,A0,NF20,FC1344,E20000,G10,FR0.30" };

  msg.power = false;
  msg.active = false;
  msg.nf = 20;
  msg.frames = 1344;
  msg.exposure = 20000;
  msg.gain = 10;
  msg.frame_rate = 0.30;

  auto serialized_string = std::string{};
  ds_acomms_serialization::serialize(msg, serialized_string);

  ASSERT_TRUE(sentry_acomms::valid_avtrak_string(serialized_string));
  EXPECT_STREQ(expected_str.data(), serialized_string.data());
}

TEST(VehicleProsilicaCameraSerialization, deserialize_from_string)
{
  auto expected_msg = sentry_acomms::SentryProsilicaCameraStatus{};
  auto str = std::string{ "P0,A0,NF20,FC1344,E20000,G10,FR0.30" };

  expected_msg.power = false;
  expected_msg.active = false;
  expected_msg.nf = 20;
  expected_msg.frames = 1344;
  expected_msg.exposure = 20000;
  expected_msg.gain = 10;
  expected_msg.frame_rate = 0.30;

  auto result = sentry_acomms::SentryProsilicaCameraStatus{};

  EXPECT_TRUE(ds_acomms_serialization::deserialize(str, result));

  EXPECT_EQ(expected_msg.power, result.power);
  EXPECT_EQ(expected_msg.active, result.active);
  EXPECT_EQ(expected_msg.nf, result.nf);

  EXPECT_EQ(expected_msg.frames, result.frames);
  EXPECT_EQ(expected_msg.exposure, result.exposure);
  EXPECT_EQ(expected_msg.gain, result.gain);
  EXPECT_FLOAT_EQ(expected_msg.frame_rate, result.frame_rate);

  // Raw stings also have a time stamp that we don't care about.
  str = std::string{ "1510633797,P0,A0,NF20,FC1344,E20000,G10,FR0.30" };
  EXPECT_TRUE(ds_acomms_serialization::deserialize(str, result));

  EXPECT_EQ(expected_msg.power, result.power);
  EXPECT_EQ(expected_msg.active, result.active);
  EXPECT_EQ(expected_msg.nf, result.nf);

  EXPECT_EQ(expected_msg.frames, result.frames);
  EXPECT_EQ(expected_msg.exposure, result.exposure);
  EXPECT_EQ(expected_msg.gain, result.gain);
  EXPECT_FLOAT_EQ(expected_msg.frame_rate, result.frame_rate);
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
