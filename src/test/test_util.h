/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef PROJECT_TEST_UTIL_H
#define PROJECT_TEST_UTIL_H

#include <string>
#include <algorithm>

namespace sentry_acomms
{
/// @brief Validate an AvTrak message
///
/// AvTrak ASCII strings can only contain characters between decimal 32 (space) and 126 ('~')
///
/// \param msg
/// \return
bool valid_avtrak_string(const std::string& msg)
{
  return std::all_of(msg.cbegin(), msg.cend(), [](uint8_t c) { return c >= 32 && c <= 126; });
}
std::string avtrak_failing_char(const std::string& msg)
{
  auto msg_str = msg.c_str();
  for (int i=0; i<msg.length(); i++){
    auto c = msg_str[i];
    if (c >= 32 && c <= 126)
      continue;
    else
      return msg.substr(0,i);
  }
  return msg;
}
}

#endif  // PROJECT_TEST_UTIL_H
