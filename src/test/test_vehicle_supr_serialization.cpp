/**
* Copyright 2023 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_acomms/serialization.h"
#include "ds_core_msgs/DsStringStamped.h"
#include "sentry_acomms/SuprCommand.h"
#include "test_util.h"

#include <gtest/gtest.h>

TEST(VehicleSuprSerialization, serialize_as_string)
{
  auto msg = ds_core_msgs::DsStringStamped{};
  //const auto expected_str = std::string{ "MAIN:H9,T17.7,P17.9 BAT:H47,T3.7,P6.6" };
  const auto expected_str = std::string{ "SUPR SAMPLING 2.5 00000526 00012689 000235.1 000151.2" };

  msg.payload = "SUPR SAMPLING 2.5 00000526 00012689 000235.1 000151.2";


  auto serialized_string = std::string{};
  ds_acomms_serialization::serialize(msg, serialized_string);
  
  //should be fine to use this avtrak thing, that checks if string has weird chars
  ASSERT_TRUE(sentry_acomms::valid_avtrak_string(serialized_string));
  EXPECT_STREQ(expected_str.data(), serialized_string.data());
}

TEST(VehicleSuprSerialization, deserialize_from_string)
{
  auto expected_msg = ds_core_msgs::DsStringStamped{};
  const auto str = std::string{ "SUPR SAMPLING 2.5 00000526 00012689 000235.1 000151.2" };

  expected_msg.payload = "SUPR SAMPLING 2.5 00000526 00012689 000235.1 000151.2";

  auto result = ds_core_msgs::DsStringStamped{};

  ds_acomms_serialization::deserialize(str, result);

  EXPECT_EQ(expected_msg.payload, result.payload);

}

TEST(VehicleSuprSerialization, deserialize_from_string_failures)
{
  auto msg = ds_core_msgs::DsStringStamped{};
  
  //str bad format
  const auto str_bad_format = std::string{ "SPRH SAMPLING 2.5 00000526 00012689 000235.1 000151.2" };
  EXPECT_FALSE(ds_acomms_serialization::deserialize(str_bad_format, msg));
} 

TEST(VehicleSuprSerialization, deserialize_commands)
{
  auto msg = sentry_acomms::SuprCommand{};


  auto command_str = std::string{ "SUPR SAMPLE" };
  EXPECT_TRUE(ds_acomms_serialization::deserialize(command_str, msg));
  EXPECT_EQ(sentry_acomms::SuprCommand::START_SAMPLE, msg.supr_cmd);
  
} 

TEST(VehicleSuprSerialization, serialize_commands)
{
  auto msg = sentry_acomms::SuprCommand{};
  msg.supr_cmd = sentry_acomms::SuprCommand::START_SAMPLE;

  auto expected_str = std::string("SUPR SAMPLE");
  
  auto serialized_string = std::string{};
  ds_acomms_serialization::serialize(msg, serialized_string);
  EXPECT_EQ(serialized_string, expected_str);
  
} 

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
