/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_acomms/serialization.h"
#include "sentry_acomms/SentryScalarScience.h"

#include <gtest/gtest.h>

TEST(ScalarScienceSerialization, serialize_as_string)
{
  auto msg = sentry_acomms::SentryScalarScience{};
  const auto expected_str = std::string{"69.02 0.0165 3.3561 3.41 34.6015 1234.56"};

  msg.oxygen_concentration = 69.02;
  msg.obs_raw = 0.0165;
  msg.orp_raw = 3.3561;
  msg.ctd_temperature = 3.41;
  msg.ctd_salinity = 34.6015;
  msg.paro_depth = 1234.56;

  auto serialized_string = std::string{};
  ds_acomms_serialization::serialize(msg, serialized_string);

  EXPECT_STREQ(expected_str.data(), serialized_string.data());
}

TEST(ScalarScienceSerialization, deserialize_from_string)
{
  auto expected_msg = sentry_acomms::SentryScalarScience{};
  const auto str = std::string{"69.02 0.0165 3.3561 3.41 34.6015 1234.56"};

  expected_msg.oxygen_concentration = 69.02;
  expected_msg.obs_raw = 0.0165;
  expected_msg.orp_raw = 3.3561;
  expected_msg.ctd_temperature = 3.41;
  expected_msg.ctd_salinity = 34.6015;
  expected_msg.paro_depth = 1234.56;

  auto result = sentry_acomms::SentryScalarScience{};

  ds_acomms_serialization::deserialize(str, result);

  EXPECT_FLOAT_EQ(expected_msg.oxygen_concentration, result.oxygen_concentration);
  EXPECT_FLOAT_EQ(expected_msg.obs_raw, result.obs_raw);
  EXPECT_FLOAT_EQ(expected_msg.orp_raw, result.orp_raw);
  EXPECT_FLOAT_EQ(expected_msg.ctd_temperature, result.ctd_temperature);
  EXPECT_FLOAT_EQ(expected_msg.ctd_salinity, result.ctd_salinity);
  EXPECT_FLOAT_EQ(expected_msg.paro_depth, result.paro_depth);
}

TEST(ScalarScienceSerialization, deserialize_from_string_failures)
{
  auto msg = sentry_acomms::SentryScalarScience{};

  const auto str_short = std::string{"69.02 0.0165 3.3561 3.41 34.6015"};
  EXPECT_FALSE(ds_acomms_serialization::deserialize(str_short, msg));
}
// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
