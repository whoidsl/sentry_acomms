/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "sentry_acomms/serialization.h"
#include "sentry_acomms/SentryDvlStatus.h"
#include "test_util.h"

#include <gtest/gtest.h>

TEST(VehicleDvlSerialization, serialize_as_string)
{
  auto msg = sentry_acomms::SentryDvlStatus{};
  const auto expected_str = std::string{ "64.50 105.60 0.91 4 60 10 34 2 65422" };

  msg.altitude = 64.50;
  msg.course_gnd = 105.60;
  msg.speed_gnd = 0.91;

  msg.num_good_beams = 4;

  msg.time_last_raw_dvl = 60;
  msg.time_good_parse = 10;
  msg.window_size = 34;
  msg.parses_tried_in_window = 2;
  msg.parses_ok_in_window = 65422;

  

  auto serialized_string = std::string{};
  ds_acomms_serialization::serialize(msg, serialized_string);

  ASSERT_TRUE(sentry_acomms::valid_avtrak_string(serialized_string));
  EXPECT_STREQ(expected_str.data(), serialized_string.data());
}

TEST(VehicleDvlSerialization, deserialize_from_string)
{
  auto expected_msg = sentry_acomms::SentryDvlStatus{};
  const auto str = std::string{ "64.50 105.60 0.91 3 60 10 34 2 65422" };

  expected_msg.altitude = 64.50;
  expected_msg.course_gnd = 105.60;
  expected_msg.speed_gnd = 0.91;

  expected_msg.num_good_beams = 3;

  expected_msg.time_last_raw_dvl = 60;
  expected_msg.time_good_parse = 10;
  expected_msg.window_size = 34;
  expected_msg.parses_tried_in_window = 2;
  expected_msg.parses_ok_in_window = 65422;

  auto result = sentry_acomms::SentryDvlStatus{};

  ds_acomms_serialization::deserialize(str, result);

  EXPECT_FLOAT_EQ(expected_msg.altitude, result.altitude);
  EXPECT_FLOAT_EQ(expected_msg.course_gnd, result.course_gnd);
  EXPECT_FLOAT_EQ(expected_msg.speed_gnd, result.speed_gnd);

  EXPECT_FLOAT_EQ(expected_msg.num_good_beams, result.num_good_beams);

  EXPECT_EQ(expected_msg.time_last_raw_dvl, result.time_last_raw_dvl);
  EXPECT_EQ(expected_msg.time_good_parse, result.time_good_parse);
  EXPECT_EQ(expected_msg.window_size, result.window_size);
  EXPECT_EQ(expected_msg.parses_tried_in_window, result.parses_tried_in_window);
  EXPECT_EQ(expected_msg.parses_ok_in_window, result.parses_ok_in_window);
}

TEST(VehicleDvlSerialization, deserialize_from_string_failures)
{
  auto msg = sentry_acomms::SentryDvlStatus{};

  const auto str_missing_cog_data = std::string{ "64.50 0.91 3 3 6" };
  EXPECT_FALSE(ds_acomms_serialization::deserialize(str_missing_cog_data, msg));
}

TEST(VehicleDvlSerialization, deserialize_from_extra_string)
{
  auto msg = sentry_acomms::SentryDvlStatus{};

  const auto str_extra_data = std::string{ "64.50 105.60 0.91 3 60 10 34 2 65422 hello 345 24.325" };
  EXPECT_TRUE(ds_acomms_serialization::deserialize(str_extra_data, msg));
}

TEST(VehicleDvlSerialization, deserialize_from_bad_string)
{
  auto msg = sentry_acomms::SentryDvlStatus{};

  const auto str_bad_data = std::string{ "-nan Shallow 0.91 12 60 10 34 2 65422" };
  EXPECT_FALSE(ds_acomms_serialization::deserialize(str_bad_data, msg));
}

// Run all the tests that were declared with TEST()
int main(int argc, char** argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
