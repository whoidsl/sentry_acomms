/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef SENTRY_ACOMMS_CONTROL_AND_ACTUATOR_QUEUE_H
#define SENTRY_ACOMMS_CONTROL_AND_ACTUATOR_QUEUE_H

#include "ds_acomms_queue_manager/simple_single_queue.h"
#include "sentry_acomms/SentryControlAndActuatorState.h"

#include "ds_param/ds_param_conn.h"

#include <ros/node_handle.h>

namespace geometry_msgs
{
ROS_DECLARE_MESSAGE(WrenchStamped)
}

namespace ds_actuator_msgs
{
ROS_DECLARE_MESSAGE(ThrusterState)
}

namespace sail_con
{
ROS_DECLARE_MESSAGE(SentryServoState)
}

namespace sentry_thruster_servo
{
ROS_DECLARE_MESSAGE(State)
}

namespace sentry_acomms
{
class VehicleControlAndActuatorQueue : public ds_acomms_queue_manager::SimpleSingleQueue<SentryControlAndActuatorState>
{
public:
  enum SERVO_INDEX
  {
    FWD_SERVO = 0,
    AFT_SERVO
  };

  enum THRUSTER_INDEX
  {
    FWD_PORT_THRUSTER = 0,
    FWD_STBD_THRUSTER,
    AFT_PORT_THRUSTER,
    AFT_STBD_THRUSTER,
  };

  explicit VehicleControlAndActuatorQueue();
  ~VehicleControlAndActuatorQueue() override;

  void setup(ros::NodeHandle& nh) override;

protected:
  void dsparamCallback(const ds_param::ParamConnection::ParamCollection& params);
  void sailSerialServoCallback(SERVO_INDEX index, const sentry_thruster_servo::StateConstPtr& msg);
  void sailServoCallback(SERVO_INDEX index, const sail_con::SentryServoStateConstPtr& msg);
  void thrusterCallback(THRUSTER_INDEX index, const ds_actuator_msgs::ThrusterStateConstPtr& msg);
  void wrenchCallback(const geometry_msgs::WrenchStamped& wrench);

private:
  ds_param::ParamConnection::Ptr param_con_;
  ds_param::IntParam::Ptr active_controller_;
  ds_param::IntParam::Ptr active_allocation_;

  ros::Subscriber fwd_servo_sub_;
  ros::Subscriber aft_servo_sub_;

  ros::Subscriber fwd_port_thruster_sub_;
  ros::Subscriber fwd_stbd_thruster_sub_;
  ros::Subscriber aft_port_thruster_sub_;
  ros::Subscriber aft_stbd_thruster_sub_;

  ros::Subscriber wrench_sub_;
};
}
#endif  // SENTRY_ACOMMS_CONTROL_AND_ACTUATOR_QUEUE_H
