/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef SENTRY_ACOMMS_SERIALIZATION_H
#define SENTRY_ACOMMS_SERIALIZATION_H

#include "ds_acomms_queue_manager/QueueManagerMessageEnum.h"
#include "ds_acomms_serialization/serialization.h"
#include <ds_core_msgs/DsStringStamped.h>

#include <boost/any.hpp>

namespace ds_control_msgs
{
ROS_DECLARE_MESSAGE(ExternalBottomFollowTimedOverride)
}

namespace sentry_acomms
{
// Queue Manager Commands
ROS_DECLARE_MESSAGE(BottomFollowerParameterCommand)
ROS_DECLARE_MESSAGE(DepthFollowerParameterCommand)
ROS_DECLARE_MESSAGE(MissionControllerCommand)
ROS_DECLARE_MESSAGE(BlueviewCommand)
ROS_DECLARE_MESSAGE(ResonCommand)
ROS_DECLARE_MESSAGE(PhinsCommand)
ROS_DECLARE_MESSAGE(AcousticJoystickCommand)
ROS_DECLARE_MESSAGE(ThrustersEnabledCommand)
ROS_DECLARE_MESSAGE(AcousticScheduleCommand)
ROS_DECLARE_MESSAGE(SentryMxEvent)
ROS_DECLARE_MESSAGE(SentryMxSharedParam)
ROS_DECLARE_MESSAGE(SentryNavShift)
ROS_DECLARE_MESSAGE(KongsbergParamCommand)
ROS_DECLARE_MESSAGE(KongsbergPingCommand)
ROS_DECLARE_MESSAGE(KongsbergKctrlCommand)
ROS_DECLARE_MESSAGE(SypridCommand)
ROS_DECLARE_MESSAGE(SuprCommand)
ROS_DECLARE_MESSAGE(SetActiveDepthGoalCommand)

// Queues
ROS_DECLARE_MESSAGE(SentryVehicleState)
ROS_DECLARE_MESSAGE(SentryHtp)
ROS_DECLARE_MESSAGE(SentryProsilicaCameraStatus)
ROS_DECLARE_MESSAGE(SentryPowerSwitches)
ROS_DECLARE_MESSAGE(SentryCurrentShift)
ROS_DECLARE_MESSAGE(SentryBottomFollowerParameters)
ROS_DECLARE_MESSAGE(SentryDepthFollowerParameters)
ROS_DECLARE_MESSAGE(SentryGoalLegState)
ROS_DECLARE_MESSAGE(SentryBlueviewStatus)
ROS_DECLARE_MESSAGE(SentryControlAndActuatorState)
ROS_DECLARE_MESSAGE(SentryResonStatus)
ROS_DECLARE_MESSAGE(SentryScalarScience)
ROS_DECLARE_MESSAGE(SentryMxStatus)
ROS_DECLARE_MESSAGE(SentryKongsbergStatus)
ROS_DECLARE_MESSAGE(SentryKongsbergStatus2)
ROS_DECLARE_MESSAGE(SentrySypridStatus)
ROS_DECLARE_MESSAGE(SentryDvlStatus)

/// @brief Determine the message type of the provided string.
///
/// Returns QueueManagerMessageEnum::INVALID_MESSAGE if unable to match a message
/// type to the string.
///
/// \param buf
/// \return
uint8_t queue_message_type(const std::string& buf);

inline uint8_t queue_message_type(const std::vector<uint8_t>& buf)
{
  ROS_ERROR_STREAM("Queue commands from byte streams are not implemented");
  return ds_acomms_queue_manager::QueueManagerMessageEnum::INVALID_MESSAGE_TYPE;
}
}

namespace ds_acomms_serialization
{
template <>
void serialize(const sentry_acomms::SentryVehicleState& msg, std::string& buf);

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryVehicleState& msg);

template <>
void serialize(const sentry_acomms::SentryHtp& msg, std::string& buf);

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryHtp& msg);

template <>
void serialize(const ds_control_msgs::ExternalBottomFollowTimedOverride& msg, std::string& buf);

template <>
bool deserialize(const std::string& buf, ds_control_msgs::ExternalBottomFollowTimedOverride& msg);

template <>
void serialize(const sentry_acomms::MissionControllerCommand& msg, std::string& buf);

template <>
bool deserialize(const std::string& buf, sentry_acomms::MissionControllerCommand& msg);

template <>
void serialize(const sentry_acomms::BlueviewCommand& msg, std::string& buf);

template <>
bool deserialize(const std::string& buf, sentry_acomms::BlueviewCommand& msg);

template <>
void serialize(const sentry_acomms::PhinsCommand& msg, std::string& buf);

template <>
bool deserialize(const std::string& buf, sentry_acomms::PhinsCommand& msg);

template <>
void serialize(const sentry_acomms::ResonCommand& msg, std::string& buf);

template <>
bool deserialize(const std::string& buf, sentry_acomms::ResonCommand& msg);

template <>
void serialize(const sentry_acomms::SentryProsilicaCameraStatus& msg, std::string& buf);

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryProsilicaCameraStatus& msg);

template <>
void serialize(const sentry_acomms::SentryPowerSwitches& msg, std::string& buf);

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryPowerSwitches& msg);

template <>
void serialize(const sentry_acomms::SentryCurrentShift& msg, std::string& buf);

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryCurrentShift& msg);

template <>
void serialize(const sentry_acomms::SentryBottomFollowerParameters& msg, std::string& buf);

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryBottomFollowerParameters& msg);

template <>
void serialize(const sentry_acomms::SentryGoalLegState& msg, std::string& buf);

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryGoalLegState& msg);

template <>
void serialize(const sentry_acomms::SentryBlueviewStatus& msg, std::string& buf);

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryBlueviewStatus& msg);

template <>
void serialize(const sentry_acomms::BottomFollowerParameterCommand& msg, std::string& buf);

template <>
bool deserialize(const std::string& buf, sentry_acomms::BottomFollowerParameterCommand& msg);

template <>
void serialize(const sentry_acomms::DepthFollowerParameterCommand& msg, std::string& buf);

template <>
bool deserialize(const std::string& buf, sentry_acomms::DepthFollowerParameterCommand& msg);

template <>
void serialize(const sentry_acomms::SentryControlAndActuatorState& msg, std::string& buf);

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryControlAndActuatorState& msg);

template <>
void serialize(const sentry_acomms::SentryResonStatus& msg, std::string& buf);

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryResonStatus& msg);

template <>
void serialize(const sentry_acomms::SentryScalarScience& msg, std::string& buf);

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryScalarScience& msg);

template <>
void serialize(const sentry_acomms::SentryKongsbergStatus& msg, std::string& buf);

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryKongsbergStatus& msg);

template <>
void serialize(const sentry_acomms::SentryKongsbergStatus2& msg, std::string& buf);

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryKongsbergStatus2& msg);

template <>
void serialize(const sentry_acomms::AcousticJoystickCommand& msg, std::string& buf);

template <>
bool deserialize(const std::string& buf, sentry_acomms::AcousticJoystickCommand& msg);

template <>
void serialize(const sentry_acomms::ThrustersEnabledCommand& msg, std::string& buf);

template <>
bool deserialize(const std::string& buf, sentry_acomms::ThrustersEnabledCommand& msg);

template <>
void serialize(const sentry_acomms::AcousticScheduleCommand& msg, std::string& buf);

template <>
bool deserialize(const std::string& buf, sentry_acomms::AcousticScheduleCommand& msg);

template <>
void serialize(const sentry_acomms::SentryMxEvent& msg, std::string& buf);

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryMxEvent& msg);

template <>
void serialize(const sentry_acomms::SentryMxSharedParam& msg, std::string& buf);

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryMxSharedParam& msg);

template <>
void serialize(const sentry_acomms::SentryMxStatus& msg, std::string& buf);

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryMxStatus& msg);

template <>
void serialize(const sentry_acomms::SentryNavShift& msg, std::string& buf);

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryNavShift& msg);

template <>
void serialize(const sentry_acomms::KongsbergParamCommand& msg, std::string& buf);

template <>
bool deserialize(const std::string& buf, sentry_acomms::KongsbergParamCommand& msg);

template <>
void serialize(const sentry_acomms::KongsbergPingCommand& msg, std::string& buf);

template <>
bool deserialize(const std::string& buf, sentry_acomms::KongsbergPingCommand& msg);

template <>
void serialize(const sentry_acomms::KongsbergKctrlCommand& msg, std::string& buf);

template <>
bool deserialize(const std::string& buf, sentry_acomms::KongsbergKctrlCommand& msg);

template <>
void serialize(const sentry_acomms::SentrySypridStatus& msg, std::string& buf);

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentrySypridStatus& msg);

template <>
void serialize(const sentry_acomms::SypridCommand& msg, std::string& buf);

template <>
bool deserialize(const std::string& buf, sentry_acomms::SypridCommand& msg);

template <>
void serialize(const ds_core_msgs::DsStringStamped& msg, std::string& buf);

template <>
bool deserialize(const std::string& buf, ds_core_msgs::DsStringStamped& msg);

template <>
void serialize(const sentry_acomms::SentryDepthFollowerParameters& msg, std::string& buf);

template <>
bool deserialize(const std::string& buf, sentry_acomms::SentryDepthFollowerParameters& msg);

template <>
void serialize(const sentry_acomms::SetActiveDepthGoalCommand& msg, std::string& buf);

template <>
bool deserialize(const std::string& buf, sentry_acomms::SetActiveDepthGoalCommand& msg);
template <>
void serialize(const sentry_acomms::SentryDvlStatus& msg, std::string& buf);

template<>
bool deserialize(const std::string& buf, sentry_acomms::SentryDvlStatus& msg);
} // namespace



#endif  // SENTRY_ACOMMS_SERIALIZATION_H
