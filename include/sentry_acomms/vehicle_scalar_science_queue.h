/**
* Copyright 2019 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

// Created by llindzey on 31 Jan 2019

#ifndef SENTRY_ACOMMS_VEHICLE_SCALAR_SCIENCE_QUEUE_H
#define SENTRY_ACOMMS_VEHICLE_SCALAR_SCIENCE_QUEUE_H

#include "ds_acomms_queue_manager/simple_single_queue.h"
#include "sentry_acomms/SentryScalarScience.h"
#include <ros/node_handle.h>

namespace ds_hotel_msgs
{
  ROS_DECLARE_MESSAGE(A2D2);
}
namespace ds_sensor_msgs
{
  ROS_DECLARE_MESSAGE(Ctd);
  ROS_DECLARE_MESSAGE(OxygenConcentration);
  ROS_DECLARE_MESSAGE(DepthPressure);
}

namespace sentry_acomms
{
class VehicleScalarScienceQueue : public ds_acomms_queue_manager::SimpleSingleQueue<SentryScalarScience>
{
public:
  explicit VehicleScalarScienceQueue();
  ~VehicleScalarScienceQueue() override;

  void setup(ros::NodeHandle& nh) override;

  void anderaaOptodeCallback(const ds_sensor_msgs::OxygenConcentration& msg);
  void obsCallback(const ds_hotel_msgs::A2D2& msg);
  void orpCallback(const ds_hotel_msgs::A2D2& msg);
  void sbe49Callback(const ds_sensor_msgs::Ctd& msg);
  void paroCallback(const ds_sensor_msgs::DepthPressure& msg);

private:
  void updateQueue();

  ros::Subscriber anderaa_optode_sub_;
  ros::Subscriber obs_sub_;
  ros::Subscriber orp_sub_;
  ros::Subscriber sbe49_sub_;
  ros::Subscriber paro_sub_;
};
}
#endif  // SENTRY_ACOMMS_VEHICLE_SCALAR_SCIENCE_QUEUE_H
