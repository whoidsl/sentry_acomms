/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef SENTRY_ACOMMS_SENTRY_VEHICLE_QUEUE_MANAGER_H
#define SENTRY_ACOMMS_SENTRY_VEHICLE_QUEUE_MANAGER_H

#include "sentry_acomms/SentryAcommQueueEnum.h"
#include "sentry_acomms/SentryQueueManagerCommandEnum.h"

#include <ds_acomms_queue_manager/queue_manager.h>

#include <ds_param/ds_param_conn.h>
#include <ds_core_msgs/VoidCmd.h>

namespace sentry_acomms
{
class SentryVehicleQueueManagerPrivate;

class SentryVehicleQueueManager : public ds_acomms_queue_manager::QueueManager
{
  DS_DECLARE_PRIVATE(SentryVehicleQueueManager);

public:
  enum QueueType
  {
    QUEUE_VEHICLE_STATE = SentryAcommQueueEnum::SENTRY_VEHICLE_STATE,
    QUEUE_HTP = SentryAcommQueueEnum::SENTRY_HTP_SENSORS,
    QUEUE_MC_SHIFT = SentryAcommQueueEnum::SENTRY_MC_SHIFT,
    QUEUE_PWR = SentryAcommQueueEnum::SENTRY_POWER,
    QUEUE_BOTTOM_FOLLOWER = SentryAcommQueueEnum::SENTRY_BOTTOM_FOLLOWER,
    QUEUE_DEPTH_FOLLOWER = SentryAcommQueueEnum::SENTRY_DEPTH_FOLLOWER,
    QUEUE_GOAL_LEG = SentryAcommQueueEnum::SENTRY_GOAL_LEG,
    QUEUE_SCALAR_SCIENCE = SentryAcommQueueEnum::SENTRY_SCALAR_SCIENCE,
    QUEUE_MXSTATUS = SentryAcommQueueEnum::SENTRY_MXSTATUS,
    QUEUE_PROSILICA_CAMERA = SentryAcommQueueEnum::SENTRY_CAMERA,
    QUEUE_BLUEVIEW = SentryAcommQueueEnum::SENTRY_BLUEVIEW,
    QUEUE_CONTROL_AND_ACTUATOR = SentryAcommQueueEnum::SENTRY_CONTROL_AND_ACTUATOR,
    QUEUE_RESON = SentryAcommQueueEnum::SENTRY_RESON_MULTIBEAM,
    QUEUE_KONGSBERG = SentryAcommQueueEnum::SENTRY_KONGSBERG_MULTIBEAM,
    QUEUE_SYPRID = SentryAcommQueueEnum::SENTRY_SYPRID,
    QUEUE_INS = SentryAcommQueueEnum::SENTRY_INS,
    QUEUE_INS_NAV = SentryAcommQueueEnum::SENTRY_INS_NAV,
    QUEUE_VEHICLE_DVL = SentryAcommQueueEnum::SENTRY_VEHICLE_DVL,
    QUEUE_SUPR = SentryAcommQueueEnum::SENTRY_SUPR,
    QUEUE_UDP00 = SentryAcommQueueEnum::SENTRY_UDP_QUEUE_00,
    QUEUE_UDP01 = SentryAcommQueueEnum::SENTRY_UDP_QUEUE_01,
    QUEUE_UDP02 = SentryAcommQueueEnum::SENTRY_UDP_QUEUE_02,
    QUEUE_UDP03 = SentryAcommQueueEnum::SENTRY_UDP_QUEUE_03,
    QUEUE_UDP04 = SentryAcommQueueEnum::SENTRY_UDP_QUEUE_04,
    QUEUE_UDP05 = SentryAcommQueueEnum::SENTRY_UDP_QUEUE_05,
    QUEUE_UDP06 = SentryAcommQueueEnum::SENTRY_UDP_QUEUE_06,
    QUEUE_UDP07 = SentryAcommQueueEnum::SENTRY_UDP_QUEUE_07,
    QUEUE_UDP08 = SentryAcommQueueEnum::SENTRY_UDP_QUEUE_08,
    QUEUE_UDP09 = SentryAcommQueueEnum::SENTRY_UDP_QUEUE_09,
    QUEUE_UDP10 = SentryAcommQueueEnum::SENTRY_UDP_QUEUE_10,
    QUEUE_UDP11 = SentryAcommQueueEnum::SENTRY_UDP_QUEUE_11,
    QUEUE_UDP12 = SentryAcommQueueEnum::SENTRY_UDP_QUEUE_12,
    QUEUE_UDP13 = SentryAcommQueueEnum::SENTRY_UDP_QUEUE_13,
    QUEUE_UDP14 = SentryAcommQueueEnum::SENTRY_UDP_QUEUE_14,
    QUEUE_UDP15 = SentryAcommQueueEnum::SENTRY_UDP_QUEUE_15,
    QUEUE_UDP16 = SentryAcommQueueEnum::SENTRY_UDP_QUEUE_16,
    QUEUE_UDP17 = SentryAcommQueueEnum::SENTRY_UDP_QUEUE_17,
    QUEUE_UDP18 = SentryAcommQueueEnum::SENTRY_UDP_QUEUE_18,
    QUEUE_UDP19 = SentryAcommQueueEnum::SENTRY_UDP_QUEUE_19,
    QUEUE_UDP20 = SentryAcommQueueEnum::SENTRY_UDP_QUEUE_20,
    QUEUE_UDP21 = SentryAcommQueueEnum::SENTRY_UDP_QUEUE_21,
    QUEUE_UDP22 = SentryAcommQueueEnum::SENTRY_UDP_QUEUE_22,
    QUEUE_UDP23 = SentryAcommQueueEnum::SENTRY_UDP_QUEUE_23,
    QUEUE_UDP24 = SentryAcommQueueEnum::SENTRY_UDP_QUEUE_24,
    QUEUE_UDP25 = SentryAcommQueueEnum::SENTRY_UDP_QUEUE_25,
    QUEUE_UDP26 = SentryAcommQueueEnum::SENTRY_UDP_QUEUE_26,
    QUEUE_UDP27 = SentryAcommQueueEnum::SENTRY_UDP_QUEUE_27,
  };

  static const auto UDP_QUEUE_START = QUEUE_UDP00;
  static const auto UDP_QUEUE_END = QUEUE_UDP27 + 1;
  static const auto UDP_PASSTHROUGH_COMMAND_START = SentryQueueManagerCommandEnum::UDP_PASSTHROUGH_00_COMMAND;
  static const auto UDP_PASSTHROUGH_COMMAND_END = SentryQueueManagerCommandEnum::UDP_PASSTHROUGH_27_COMMAND + 1;

  SentryVehicleQueueManager();
  SentryVehicleQueueManager(int argc, char* argv[], std::string& name);
  ~SentryVehicleQueueManager() override;

  DS_DISABLE_COPY(SentryVehicleQueueManager);

protected:
  void setupQueues() override;
  void setupPublishers() override;
  void setupTimers() override;
  void setupServices() override;
  void setupParameters() override;

  bool handleCustomQueueMessage(const ds_acomms_msgs::AcousticModemData& msg, bool as_string) override;
  // 2021-11-18 SS - added the following method for handling additional USBL positioning
  //                 data sent down after SDQ requests
  bool handleQueueRequestAdditionalPayload(std::vector<uint8_t>& buf, bool as_string) override;

  void scheduleMicroModemTimerCallback(const ros::TimerEvent&);
  void scheduleAvtrakTimerCallback(const ros::TimerEvent&);

private:
  std::unique_ptr<SentryVehicleQueueManagerPrivate> d_ptr_;

  // ds_param connection
  ds_param::ParamConnection::Ptr conn_;

  ds_param::EnumParam::Ptr enum_heading_;
  ds_param::EnumParam::Ptr enum_pitchroll_;
  ds_param::EnumParam::Ptr enum_xy_;
  ds_param::EnumParam::Ptr enum_depth_;
  ds_param::EnumParam::Ptr enum_sync_;
};
}
#endif  // SENTRY_ACOMMS_SENTRY_VEHICLE_QUEUE_MANAGER_H
