/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef SENTRY_ACOMMS_VEHICLE_STATE_QUEUE_H
#define SENTRY_ACOMMS_VEHICLE_STATE_QUEUE_H

#include "ds_acomms_queue_manager/simple_single_queue.h"
#include "sentry_acomms/SentryVehicleState.h"

#include <ros/node_handle.h>

namespace ds_nav_msgs
{
ROS_DECLARE_MESSAGE(AggregatedState);
}

namespace ds_core_msgs
{
ROS_DECLARE_MESSAGE(Abort);
}

namespace ds_control_msgs
{
ROS_DECLARE_MESSAGE(BottomFollow1D);
ROS_DECLARE_MESSAGE(GoalLegState);
}

namespace ds_hotel_msgs
{
ROS_DECLARE_MESSAGE(BatMan);
}

namespace sentry_acomms
{
class VehicleStateQueue : public ds_acomms_queue_manager::SimpleSingleQueue<SentryVehicleState>
{
public:
  explicit VehicleStateQueue();
  ~VehicleStateQueue() override;

  void setup(ros::NodeHandle& nh) override;

protected:
  void navStateCallback(const ds_nav_msgs::AggregatedState& msg);
  void legGoalCallback(const ds_control_msgs::GoalLegState& msg);
  void bottomFollowerCallback(const ds_control_msgs::BottomFollow1D& msg);
  void batmanCallback(const ds_hotel_msgs::BatMan& msg);
  void abortCallback(const ds_core_msgs::Abort& msg);

private:
  ros::Subscriber nav_sub_;
  ros::Subscriber bf_goal_sub_;
  ros::Subscriber bat_sub_;
  ros::Subscriber leg_goal_sub_;
  ros::Subscriber abort_sub_;
  uint64_t aggregation_state_;
};
}
#endif  // SENTRY_ACOMMS_VEHICLE_STATE_QUEUE_H
