/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef SENTRY_ACOMMS_SENTRY_MICRO_MODEM_H
#define SENTRY_ACOMMS_SENTRY_MICRO_MODEM_H

#include "ds_acomms_modem/whoi_micromodem.h"

namespace sentry_acomms
{
struct SentryMicromodemPrivate;

class SentryMicromodem : public ds_acomms_modem::WhoiMicromodem
{
  DS_DECLARE_PRIVATE(SentryMicromodem);

public:
  SentryMicromodem();
  SentryMicromodem(int argc, char* argv[], const std::string& name);
  ~SentryMicromodem() override;
  DS_DISABLE_COPY(SentryMicromodem)

  int send(uint16_t remote_address, std::vector<uint8_t> bytes) override;

protected:
  void handleMicromodemPacket(uint16_t src_address, std::vector<uint8_t> bytes) override;

private:
  std::unique_ptr<SentryMicromodemPrivate> d_ptr_;
};
}
#endif  // SENTRY_ACOMMS_SENTRY_MICRO_MODEM_H
