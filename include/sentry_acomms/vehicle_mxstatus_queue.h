/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 10/1/19.
//

#ifndef SENTRY_ACOMMS_VEHICLE_MXSTATUS_QUEUE_H
#define SENTRY_ACOMMS_VEHICLE_MXSTATUS_QUEUE_H

#include "ds_acomms_queue_manager/simple_single_queue.h"
#include "sentry_acomms/SentryMxStatus.h"
#include <ros/node_handle.h>

namespace ds_mx_msgs
{
ROS_DECLARE_MESSAGE(MxMissionStatus);
}

namespace sentry_acomms
{
class VehicleMxStatusQueue : public ds_acomms_queue_manager::SimpleSingleQueue<SentryMxStatus>
{
 public:
  explicit VehicleMxStatusQueue();
  ~VehicleMxStatusQueue() override;

  void setup(ros::NodeHandle& nh) override;

  void mxStatusCallback(const ds_mx_msgs::MxMissionStatus& msg);

 private:
  ros::Subscriber status_sub_;
};
}
#endif //SENTRY_ACOMMS_VEHICLE_MXSTATUS_QUEUE_H
