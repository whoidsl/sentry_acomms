cmake_minimum_required(VERSION 2.8.3)
project(sentry_acomms)

## Compile as C++11, supported in ROS Kinetic and newer
# add_compile_options(-std=c++11)
set (CMAKE_CXX_STANDARD 11)
set (CMAKE_CXX_STANDARD_REQUIRED ON)

list(INSERT CMAKE_MODULE_PATH 0 ${PROJECT_SOURCE_DIR}/cmake)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
  ds_acomms_queue_manager
  ds_acomms_msgs
  ds_acomms_modem
  ds_acomms_serialization
  ds_core_msgs
  ds_hotel_msgs
  ds_control_msgs
  ds_nav_msgs
  ds_nmea_msgs
  ds_kloggerd_msgs
  ds_kctrl_msgs
  ds_mx_msgs
  sentry_msgs
  sail_con
  message_generation
  ds_sensor_msgs
  ds_param
  ds_nmea_msgs
  ds_supr
  sentry_thruster_servo
)

## System dependencies are found with CMake's conventions
find_package(Boost REQUIRED COMPONENTS program_options)

find_package(ClangFormat)
if(CLANG_FORMAT_FOUND)
  add_custom_target(clang-format-sentry-acomms
    COMMENT
    "Run clang-format on all project C++ sources"
    WORKING_DIRECTORY
    ${PROJECT_SOURCE_DIR}
    COMMAND
    find
    include/sentry_acomms
    src/sentry_acomms
    src/test
    src
    -iname '*.h' -o -iname '*.cpp'
    | xargs ${CLANG_FORMAT_EXECUTABLE} -i
    )

endif(CLANG_FORMAT_FOUND)

## Uncomment this if the package has a setup.py. This macro ensures
## modules and global scripts declared therein get installed
## See http://ros.org/doc/api/catkin/html/user_guide/setup_dot_py.html
# catkin_python_setup()

################################################
## Declare ROS messages, services and actions ##
################################################

## To declare and build messages, services or actions from within this
## package, follow these steps:
## * Let MSG_DEP_SET be the set of packages whose message types you use in
##   your messages/services/actions (e.g. std_msgs, actionlib_msgs, ...).
## * In the file package.xml:
##   * add a build_depend tag for "message_generation"
##   * add a build_depend and a run_depend tag for each package in MSG_DEP_SET
##   * If MSG_DEP_SET isn't empty the following dependency has been pulled in
##     but can be declared for certainty nonetheless:
##     * add a run_depend tag for "message_runtime"
## * In this file (CMakeLists.txt):
##   * add "message_generation" and every package in MSG_DEP_SET to
##     find_package(catkin REQUIRED COMPONENTS ...)
##   * add "message_runtime" and every package in MSG_DEP_SET to
##     catkin_package(CATKIN_DEPENDS ...)
##   * uncomment the add_*_files sections below as needed
##     and list every .msg/.srv/.action file to be processed
##   * uncomment the generate_messages entry below
##   * add every package in MSG_DEP_SET to generate_messages(DEPENDENCIES ...)

## Generate messages in the 'msg' folder
add_message_files(DIRECTORY msg
  FILES
  # Custom Sentry Queue Commands
  SentryQueueManagerCommandEnum.msg
  MissionControllerCommand.msg
  BottomFollowerParameterCommand.msg
  DepthFollowerParameterCommand.msg
  SetActiveDepthGoalCommand.msg
  BlueviewCommand.msg
  PhinsCommand.msg
  ResonCommand.msg
  AcousticJoystickCommand.msg
  ThrustersEnabledCommand.msg
  UdpQueueCommand.msg
  AcousticScheduleCommand.msg
  SentryMxEvent.msg
  SentryMxSharedParam.msg
  SentryNavShift.msg
  KongsbergParamCommand.msg
  KongsbergPingCommand.msg
  KongsbergKctrlCommand.msg
  SypridCommand.msg
  NavaggCommand.msg
  SuprCommand.msg

  # Sentry Data Queues
  SentryAcommQueueEnum.msg
  SentryVehicleState.msg
  SentryHtp.msg
  SentryIns.msg
  SentryInsNav.msg
  SentryScalarScience.msg
  SentryProsilicaCameraStatus.msg
  SentryPowerSwitches.msg
  SentryCurrentShift.msg
  SentryBottomFollowerParameters.msg
  SentryDepthFollowerParameters.msg
  SentryGoalLegState.msg
  SentryBlueviewStatus.msg
  SentryResonStatus.msg
  SentryControlAndActuatorState.msg
  SentryMxStatus.msg
  SentryKongsbergStatus.msg
  SentryKongsbergStatus2.msg
  SentrySypridStatus.msg
  SentryDvlStatus.msg
)

## Generate services in the 'srv' folder
# add_service_files(
#   FILES
#   Service1.srv
#   Service2.srv
# )

## Generate actions in the 'action' folder
# add_action_files(
#   FILES
#   Action1.action
#   Action2.action
# )

## Generate added messages and services with any dependencies listed here
generate_messages(
    DEPENDENCIES
    ds_core_msgs
    ds_acomms_msgs
    ds_sensor_msgs
    ds_nmea_msgs
)

################################################
## Declare ROS dynamic reconfigure parameters ##
################################################

## To declare and build dynamic reconfigure parameters within this
## package, follow these steps:
## * In the file package.xml:
##   * add a build_depend and a run_depend tag for "dynamic_reconfigure"
## * In this file (CMakeLists.txt):
##   * add "dynamic_reconfigure" to
##     find_package(catkin REQUIRED COMPONENTS ...)
##   * uncomment the "generate_dynamic_reconfigure_options" section below
##     and list every .cfg file to be processed

## Generate dynamic reconfigure parameters in the 'cfg' folder
# generate_dynamic_reconfigure_options(
#   cfg/DynReconf1.cfg
#   cfg/DynReconf2.cfg
# )

###################################
## catkin specific configuration ##
###################################
## The catkin_package macro generates cmake config files for your package
## Declare things to be passed to dependent projects
## INCLUDE_DIRS: uncomment this if your package contains header files
## LIBRARIES: libraries you create in this project that dependent projects also need
## CATKIN_DEPENDS: catkin_packages dependent projects also need
## DEPENDS: system dependencies of this project that dependent projects also need
catkin_package(
   INCLUDE_DIRS include
   LIBRARIES sentry_acomms
   CATKIN_DEPENDS ds_acomms_msgs
     ds_acomms_serialization
     ds_acomms_modem
     ds_acomms_queue_manager
     sentry_msgs
     ds_core_msgs
     ds_mx_msgs
     ds_control_msgs
     ds_nav_msgs
     ds_kctrl_msgs
     ds_kloggerd_msgs
     sail_con
     ds_sensor_msgs
     ds_supr
#  DEPENDS system_lib
)

###########
## Build ##
###########

## Specify additional locations of header files
## Your package locations should be listed before other locations
include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)

## Declare a C++ library
add_library(${PROJECT_NAME}
  include/${PROJECT_NAME}/serialization.h
  src/${PROJECT_NAME}/serialization.cpp

  include/${PROJECT_NAME}/legacy_micromodem_serialization.h
  src/${PROJECT_NAME}/legacy_micromodem_serialization.cpp

  include/${PROJECT_NAME}/sentry_micromodem.h
  src/${PROJECT_NAME}/sentry_micromodem.cpp

  include/${PROJECT_NAME}/vehicle_state_queue.h
  src/${PROJECT_NAME}/vehicle_state_queue.cpp

  include/${PROJECT_NAME}/vehicle_control_and_actuator_queue.h
  src/${PROJECT_NAME}/vehicle_control_and_actuator_queue.cpp

  include/${PROJECT_NAME}/vehicle_blueview_queue.h
  src/${PROJECT_NAME}/vehicle_blueview_queue.cpp

  include/${PROJECT_NAME}/vehicle_reson_queue.h
  src/${PROJECT_NAME}/vehicle_reson_queue.cpp

  include/${PROJECT_NAME}/vehicle_htp_queue.h
  src/${PROJECT_NAME}/vehicle_htp_queue.cpp

  include/${PROJECT_NAME}/vehicle_scalar_science_queue.h
  src/${PROJECT_NAME}/vehicle_scalar_science_queue.cpp

  include/${PROJECT_NAME}/vehicle_shift_queue.h
  src/${PROJECT_NAME}/vehicle_shift_queue.cpp

  include/${PROJECT_NAME}/vehicle_prosilica_camera_queue.h
  src/${PROJECT_NAME}/vehicle_prosilica_camera_queue.cpp

  include/${PROJECT_NAME}/vehicle_pwr_queue.h
  src/${PROJECT_NAME}/vehicle_pwr_queue.cpp

  include/${PROJECT_NAME}/vehicle_leg_goal_queue.h
  src/${PROJECT_NAME}/vehicle_leg_goal_queue.cpp

  include/${PROJECT_NAME}/vehicle_bottom_follower_queue.h
  src/${PROJECT_NAME}/vehicle_bottom_follower_queue.cpp

  include/${PROJECT_NAME}/vehicle_depth_follower_queue.h
  src/${PROJECT_NAME}/vehicle_depth_follower_queue.cpp

  include/${PROJECT_NAME}/vehicle_mxstatus_queue.h
  src/${PROJECT_NAME}/vehicle_mxstatus_queue.cpp

  include/${PROJECT_NAME}/sentry_vehicle_queue_manager.h
  src/${PROJECT_NAME}/sentry_vehicle_queue_manager.cpp

  include/${PROJECT_NAME}/vehicle_kongsberg_queue.h
  src/${PROJECT_NAME}/vehicle_kongsberg_queue.cpp

  include/${PROJECT_NAME}/vehicle_syprid_queue.h
  src/${PROJECT_NAME}/vehicle_syprid_queue.cpp

  include/${PROJECT_NAME}/vehicle_ins_queue.h
  src/${PROJECT_NAME}/vehicle_ins_queue.cpp

  include/${PROJECT_NAME}/vehicle_ins_nav_queue.h
  src/${PROJECT_NAME}/vehicle_ins_nav_queue.cpp

  include/${PROJECT_NAME}/vehicle_supr_queue.h
  src/${PROJECT_NAME}/vehicle_supr_queue.cpp

  include/${PROJECT_NAME}/vehicle_dvl_queue.h
  src/${PROJECT_NAME}/vehicle_dvl_queue.cpp
)

## Add cmake target dependencies of the library
## as an example, code may need to be generated before libraries
## either from message generation or dynamic reconfigure
add_dependencies(${PROJECT_NAME} ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

target_link_libraries(${PROJECT_NAME}
  ${catkin_LIBRARIES}
)

add_executable(sentry_queue_manager_node
  src/sentry_queue_manager_node.cpp
  )

## Rename C++ executable without prefix
## The above recommended prefix causes long target names, the following renames the
## target back to the shorter version for ease of user use
## e.g. "rosrun someones_pkg node" instead of "rosrun someones_pkg someones_pkg_node"
set_target_properties(sentry_queue_manager_node PROPERTIES OUTPUT_NAME queue_manager PREFIX "")

## Add cmake target dependencies of the executable
## same as for the library above
add_dependencies(sentry_queue_manager_node ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

target_link_libraries(sentry_queue_manager_node
  ${PROJECT_NAME}
  ${catkin_LIBRARIES}
  ${Boost_LIBRARIES}
  )

add_executable(sentry_micromodem_node
  src/sentry_micromodem_node.cpp
  )
set_target_properties(sentry_micromodem_node PROPERTIES OUTPUT_NAME micromodem_node PREFIX "")

## Add cmake target dependencies of the executable
## same as for the library above
add_dependencies(sentry_micromodem_node ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

target_link_libraries(sentry_micromodem_node
  ${PROJECT_NAME}
  ${catkin_LIBRARIES}
  )
## Declare a C++ executable
## With catkin_make all packages are built within a single CMake context
## The recommended prefix ensures that target names across packages don't collide
# add_executable(${PROJECT_NAME}_node src/sentry_acomms_node.cpp)

## Rename C++ executable without prefix
## The above recommended prefix causes long target names, the following renames the
## target back to the shorter version for ease of user use
## e.g. "rosrun someones_pkg node" instead of "rosrun someones_pkg someones_pkg_node"
# set_target_properties(${PROJECT_NAME}_node PROPERTIES OUTPUT_NAME node PREFIX "")

## Add cmake target dependencies of the executable
## same as for the library above
# add_dependencies(${PROJECT_NAME}_node ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

## Specify libraries to link a library or executable target against
# target_link_libraries(${PROJECT_NAME}_node
#   ${catkin_LIBRARIES}
# )

#############
## Install ##
#############

# all install targets should use catkin DESTINATION variables
# See http://ros.org/doc/api/catkin/html/adv_user_guide/variables.html

## Mark executable scripts (Python etc.) for installation
## in contrast to setup.py, you can choose the destination
install(PROGRAMS
  scripts/topside_micromodem_shim.py
  DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

## Mark executables and/or libraries for installation
install(TARGETS ${PROJECT_NAME} sentry_queue_manager_node sentry_micromodem_node
  ARCHIVE DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  LIBRARY DESTINATION ${CATKIN_PACKAGE_LIB_DESTINATION}
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

## Mark cpp header files for installation
install(DIRECTORY include/${PROJECT_NAME}/
  DESTINATION ${CATKIN_PACKAGE_INCLUDE_DESTINATION}
  FILES_MATCHING PATTERN "*.h"
  PATTERN ".svn" EXCLUDE
)

## Mark other files for installation (e.g. launch and bag files, etc.)
# install(FILES
#   # myfile1
#   # myfile2
#   DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}
# )

#############
## Testing ##
#############

if(CATKIN_ENABLE_TESTING)
  catkin_add_gtest(test_vehicle_state_serialization src/test/test_vehicle_state_serialization.cpp)
  target_link_libraries(test_vehicle_state_serialization ${PROJECT_NAME})

  catkin_add_gtest(test_vehicle_htp_serialization src/test/test_vehicle_htp_serialization.cpp)
  target_link_libraries(test_vehicle_htp_serialization ${PROJECT_NAME})

  catkin_add_gtest(test_vehicle_scalar_science_serialization src/test/test_vehicle_scalar_science_serialization.cpp)
  target_link_libraries(test_vehicle_scalar_science_serialization ${PROJECT_NAME})

  catkin_add_gtest(test_vehicle_shift_serialization src/test/test_vehicle_shift_serialization.cpp)
  target_link_libraries(test_vehicle_shift_serialization ${PROJECT_NAME})

  catkin_add_gtest(test_vehicle_bottom_follower_serialization src/test/test_vehicle_bottom_follower_serialization.cpp)
  target_link_libraries(test_vehicle_bottom_follower_serialization ${PROJECT_NAME})

  catkin_add_gtest(test_vehicle_depth_follower_serialization src/test/test_vehicle_depth_follower_serialization.cpp)
  target_link_libraries(test_vehicle_depth_follower_serialization ${PROJECT_NAME})

  catkin_add_gtest(test_wbfp_serialization src/test/test_wbfp_serialization.cpp)
  target_link_libraries(test_wbfp_serialization ${PROJECT_NAME})

  catkin_add_gtest(test_wdfp_serialization src/test/test_wdfp_serialization.cpp)
  target_link_libraries(test_wdfp_serialization ${PROJECT_NAME})

  catkin_add_gtest(test_bf_override_serialization src/test/test_bf_override_serialization.cpp)
  target_link_libraries(test_bf_override_serialization ${PROJECT_NAME})

  catkin_add_gtest(test_vehicle_leg_goal_serialization src/test/test_vehicle_leg_goal_serialization.cpp)
  target_link_libraries(test_vehicle_leg_goal_serialization ${PROJECT_NAME})

  catkin_add_gtest(test_vehicle_power_switches_serialization src/test/test_vehicle_pwr_serialization.cpp)
  target_link_libraries(test_vehicle_power_switches_serialization ${PROJECT_NAME})

  catkin_add_gtest(test_vehicle_prosilica_camera_serialization src/test/test_vehicle_prosilica_camera_serialization.cpp)
  target_link_libraries(test_vehicle_prosilica_camera_serialization ${PROJECT_NAME})

  catkin_add_gtest(test_vehicle_blueview_serialization src/test/test_vehicle_blueview_serialization.cpp)
  target_link_libraries(test_vehicle_blueview_serialization ${PROJECT_NAME})

  catkin_add_gtest(test_vehicle_queue_commands src/test/test_vehicle_queue_commands.cpp)
  target_link_libraries(test_vehicle_queue_commands ${PROJECT_NAME})

  catkin_add_gtest(test_vehicle_control_actuator_serialization src/test/test_vehicle_control_actuator_serialization.cpp)
  target_link_libraries(test_vehicle_control_actuator_serialization ${PROJECT_NAME})

  catkin_add_gtest(test_vehicle_reson_status_serialization src/test/test_vehicle_reson_status_serialization.cpp)
  target_link_libraries(test_vehicle_reson_status_serialization ${PROJECT_NAME})

  catkin_add_gtest(test_ajoy_serialization src/test/test_ajoy_serialization.cpp)
  target_link_libraries(test_ajoy_serialization ${PROJECT_NAME})

  catkin_add_gtest(test_thrusters_enabled_serialization src/test/test_thrusters_enabled_serialization.cpp)
  target_link_libraries(test_thrusters_enabled_serialization ${PROJECT_NAME})

  catkin_add_gtest(test_mxevent_serialization src/test/test_mxevent_serialization.cpp)
  target_link_libraries(test_mxevent_serialization ${PROJECT_NAME})

  catkin_add_gtest(test_mxsharedparam_serialization src/test/test_mxsharedparam_serialization.cpp)
  target_link_libraries(test_mxsharedparam_serialization ${PROJECT_NAME})

  catkin_add_gtest(test_mxstatus_serialization src/test/test_mxstatus_serialization.cpp)
  target_link_libraries(test_mxstatus_serialization ${PROJECT_NAME})

  catkin_add_gtest(test_navshift_serialization src/test/test_sentry_navshift_serialization.cpp)
  target_link_libraries(test_navshift_serialization ${PROJECT_NAME})

  catkin_add_gtest(test_vehicle_kongsberg_serialization src/test/test_vehicle_kongsberg_serialization.cpp)
  target_link_libraries(test_vehicle_kongsberg_serialization ${PROJECT_NAME})

  catkin_add_gtest(test_vehicle_syprid_serialization src/test/test_vehicle_syprid_serialization.cpp)
  target_link_libraries(test_vehicle_syprid_serialization ${PROJECT_NAME})

  catkin_add_gtest(test_vehicle_ins_queue_serialization src/test/test_vehicle_ins_queue_serialization.cpp)
  target_link_libraries(test_vehicle_ins_queue_serialization ${PROJECT_NAME})

  catkin_add_gtest(test_nag_serialization src/test/test_nag_serialization.cpp)
  target_link_libraries(test_nag_serialization ${PROJECT_NAME})

  catkin_add_gtest(test_vehicle_ins_nav_queue_serialization src/test/test_vehicle_ins_nav_queue_serialization.cpp)
  target_link_libraries(test_vehicle_ins_nav_queue_serialization ${PROJECT_NAME})

  catkin_add_gtest(test_vehicle_supr_serialization src/test/test_vehicle_supr_serialization.cpp)
  target_link_libraries(test_vehicle_supr_serialization ${PROJECT_NAME})

  catkin_add_gtest(test_sadg_command_serialization src/test/test_sadg_command_serialization.cpp)
  target_link_libraries(test_sadg_command_serialization ${PROJECT_NAME})

  catkin_add_gtest(test_vehicle_dvl_serialization src/test/test_vehicle_dvl_serialization.cpp)
  target_link_libraries(test_vehicle_dvl_serialization ${PROJECT_NAME})

endif(CATKIN_ENABLE_TESTING)
